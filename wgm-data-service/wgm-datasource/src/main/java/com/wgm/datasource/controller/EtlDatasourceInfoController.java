package com.wgm.datasource.controller;

import java.sql.SQLException;
import java.util.List;

import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.constant.CommonStatus;
import com.wgm.aop.anno.LogAnno;
import com.wgm.datasource.pojo.FieldType;
import com.wgm.datasource.pojo.PageListParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wgm.datasource.entity.EtlDatasourceInfo;
import com.wgm.datasource.service.EtlDatasourceInfoService;

import javax.annotation.PostConstruct;


/**
 * 数据库管理
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-06 14:40:33
 */
@RestController
@RequestMapping("datasource/manage")
@Slf4j
public class EtlDatasourceInfoController {

    @Autowired
    private EtlDatasourceInfoService etlDatasourceInfoService;

    /**
     * TaskOut fiegn 调用
     * 查询所有数据源
     */
    @GetMapping("/getAllDatasource")
    @LogAnno(operationName = "查询所有数据源",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse<String> selectAllDatasource(){
        return CommonResponse.ok(etlDatasourceInfoService.selectAllDatasource());
    }

    /**
     * TaskOut fiegn 调用
     * 根据id查询数据源,并展示该数据库的所有表名
     * @param id 数据源id
     */
    @PostMapping("/getDatasource")
    @LogAnno(operationName = "根据id查询数据源,并展示该数据库的所有表名",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse<String> selectDatasourceById(String id){
        return CommonResponse.ok(etlDatasourceInfoService.selectDatasourceById(id));
    }

    /**
     * TaskOut fiegn 调用
     * 根据数据库源与表名查找所有字段信息
     * @param id 数据源id
     * @param tableName 表名
     * @param asName 别名
     */
    @PostMapping("/getTableField")
    @LogAnno(operationName = "根据数据库源与表名查找所有字段信息",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse<String> selectAllFieldByTableName(@RequestParam("id") String id,
                                                      @RequestParam("tableName") String tableName,
                                                      @RequestParam("asName") String asName){
        return CommonResponse.ok(etlDatasourceInfoService.selectAllFieldByTableName(id, tableName, asName));
    }

    /**
     * 根据数据源id查找数据源信息,根据数据库源与表名查找所有字段信息
     * @param id 数据源id
     * @param tableName 表名
     */
    @PostMapping("/getFieldInOutPut")
    @LogAnno(operationName = "根据数据源id查找数据源信息,根据数据库源与表名查找所有字段信息",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse<String> selectAllField(@RequestParam("id") String id,
                                           @RequestParam("tableName") String tableName) {
        return CommonResponse.ok(etlDatasourceInfoService.selectAllField(id, tableName));
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @LogAnno(operationName = "数据库管理-查询信息",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse info(@PathVariable("id") String id){
		EtlDatasourceInfo dbManage = etlDatasourceInfoService.getById(id);
        return CommonResponse.ok(dbManage);
    }

    /**
     * 数据源删除 逻辑删除
     * 现在配置文件中加入
     delete s,c from stu s,clazz c where s.cid = c.id and s.id = 2
     */
    @DeleteMapping("/delete")
    @LogAnno(operationName = "数据库管理-删除",operationType = CommonConstants.OperationType.DELETE)
    public CommonResponse delete(String id){
        return etlDatasourceInfoService.deleteDatasourceById(id) ? new CommonResponse(CommonStatus.VALID): new CommonResponse(CommonStatus.INVALID);
    }

    /**
     *  获取 对象
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @LogAnno(operationName = "获取数据源信息对象",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse<EtlDatasourceInfo> findById(@PathVariable("id")String id){
        EtlDatasourceInfo dataSource = etlDatasourceInfoService.findById(id);
        return new CommonResponse<EtlDatasourceInfo>(CommonStatus.VALID,dataSource);
    }
    /**
     * 数据源修改功能
     * @param dbManage
     * @param
     * @return
     */
    @PostMapping("/update")
    @LogAnno(operationName = "数据源修改功能",operationType = CommonConstants.OperationType.UPDATE)
    public CommonResponse update(@RequestBody EtlDatasourceInfo dbManage,@RequestParam("flag")String flag){
        log.info("is encryption? ->{}",flag);
        log.info("update message ->{}",dbManage);
        etlDatasourceInfoService.updateEtl(dbManage,flag);
        return new CommonResponse<EtlDatasourceInfo>(CommonStatus.VALID);
    }

    /**
     * 添加数据源
     * @param etlDatasourceInfoEntity
     * @return
     * @throws Exception
     */
    @PostMapping("/save")
    @LogAnno(operationName = "添加数据源",operationType = CommonConstants.OperationType.INSTER)
    public CommonResponse save(@RequestBody EtlDatasourceInfo etlDatasourceInfoEntity) throws SQLException {
        etlDatasourceInfoService.saveInfo(etlDatasourceInfoEntity);
        return new CommonResponse(CommonStatus.VALID);
    }
    /**
     * 测试连接
     * @param etlDatasourceInfoEntity
     * @return
     */
    @PostMapping("/testConnection")
    @LogAnno(operationName = "测试连接",operationType = CommonConstants.OperationType.TEST)
    public CommonResponse testConnection(@RequestBody EtlDatasourceInfo etlDatasourceInfoEntity)  {

        etlDatasourceInfoService.testConnection(etlDatasourceInfoEntity);
        return new CommonResponse(CommonStatus.VALID);
    }
    /**
     * 连接数据库
     */
    @GetMapping("/connection/{id}")
    @LogAnno(operationName = "连接数据库",operationType = CommonConstants.OperationType.TEST)
    public CommonResponse connection(@PathVariable("id")String id){
        return new CommonResponse(CommonStatus.VALID,etlDatasourceInfoService.connection(id));
    }

    /**
     * 初始化连接池
     */
    @PostConstruct
    @LogAnno(operationName = "初始化连接池",operationType = CommonConstants.OperationType.TEST)
    public void initConnactionPool(){
        List<EtlDatasourceInfo> etlDatasourceInfoList = etlDatasourceInfoService.selectAll();
        etlDatasourceInfoService.initConnactionPool(etlDatasourceInfoList);
    }

    /**
     *
     * 列表模糊+分页+DES加密
     * @param etlDatasourceInfoRequest 模糊字段：数据源名称，dbType
     * @param pageSize 每页记录数
     * @param currPage 当前页数
     * @param flag 加密状态判断 如果flag=1则加密 flag=0 不加密（默认）
     * @return 列表数据a
     * @throws Exception
     */
    @GetMapping("/etlDatasourceInfoList")
    @LogAnno(operationName = "列表模糊+分页+DES加密",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse getEtlDatasourceInfoList(PageListParam etlDatasourceInfoRequest,
                                                   @RequestParam(value = "pageSize",defaultValue = "10 ") Integer pageSize,
                                                   @RequestParam(value ="currPage",defaultValue = "1") Integer currPage,
                                                   @RequestParam(value = "flag",defaultValue = "0") String flag) throws Exception {
        return etlDatasourceInfoService.getEtlDatasourceInfoList(etlDatasourceInfoRequest,pageSize,currPage,flag);
    }



    /**
     * 查询表中所有字段,类型
     * @param database
     * @param table
     * @return
     */
    @GetMapping("showFieldAndType")
    @LogAnno(operationName = "查询表中所有字段,类型",operationType = CommonConstants.OperationType.SELECT)
    public List<FieldType> showFieldAndType(@RequestParam("database")String database,
                                               @RequestParam("table")String table,
                                            @RequestParam("id")String id){
        return etlDatasourceInfoService.showFieldAndType(database,table,id);
    }


    /**
     * 获取所有数据源
     * @return
     */
    @GetMapping("/info")
    @LogAnno(operationName = "获取所有数据源",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse dataSourceInfo(){
        List<EtlDatasourceInfo> infoList = etlDatasourceInfoService.list();
        return CommonResponse.ok(infoList);
    }

}
