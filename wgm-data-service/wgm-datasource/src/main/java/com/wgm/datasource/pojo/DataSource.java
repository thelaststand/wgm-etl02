package com.wgm.datasource.pojo;


import lombok.Data;

import java.util.Date;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/6
 */
@Data
public class DataSource {

    /**
     * 数据源id
     */
    private String id;

    /**
     * 数据源类型
     */
    private String type;
    /**
     * 数据库IP
     */
    private String ip;
    /**
     * 端口
     */
    private String port;
    /**
     * 数据库名称
     */
    private String dbName;
    /**
     * 数据源用户名
     */
    private String username;
    /**
     * 数据源密码
     */
    private String password;
    /**
     * 额外配置参数
     */
    private String params;



}
