package com.wgm.datasource.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.common.constant.CommonStatus;
import com.wgm.common.exception.AdException;
import com.wgm.common.exception.DataSourceException;
import com.wgm.common.utils.DesUtils;
import com.wgm.common.utils.PageUtils;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonResponse;
import com.wgm.datasource.base.RedisDataSource;
import com.wgm.datasource.dao.EtlDatasourceInfoDao;
import com.wgm.datasource.dto.TableColumnDTO;
import com.wgm.datasource.pojo.FieldType;
import com.wgm.datasource.pool.JdbcConnectorPool;
import com.wgm.datasource.pool.RedisConnectorPool;
import com.wgm.datasource.service.EtlDatasourceInfoService;
import com.wgm.datasource.vo.EtlDatasourceInfoVo;
import com.wgm.datasource.pojo.DataSource;
import com.wgm.datasource.pojo.PageListParam;
import com.wgm.datasource.base.MysqlDataSource;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Collectors;

import com.wgm.datasource.entity.EtlDatasourceInfo;

import org.springframework.transaction.annotation.Transactional;

import static com.wgm.datasource.pool.JdbcConnectorPool.jdbcPoolMap;


/**
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-06 14:40:33
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class EtlDatasourceInfoServiceImpl extends ServiceImpl<EtlDatasourceInfoDao, EtlDatasourceInfo> implements EtlDatasourceInfoService {

    @Autowired
    EtlDatasourceInfoDao etlDatasourceInfoDao;


    /**
     * 删除 （逻辑）
     * @param id
     * @return
     */
    @Override
    public Boolean deleteDatasourceById(String id){
        EtlDatasourceInfo dbManageEntity = this.baseMapper.selectDatasourceById(id);
        //判断 如果 数据为空 那么数据源不存在
        if(dbManageEntity == null){
            throw new DataSourceException(CommonConstants.DataError.DATA_ERROR);
        }
        //判断状态是否处于 启用状态 0启用 1禁用
        if(dbManageEntity.getState().equals(CommonStatus.IS_USING_ENUM.getCode())){
            //判断是否在工作
            if (CommonConstants.Data.MYSQL.equalsIgnoreCase(dbManageEntity.getType())){
                Connection connection = JdbcConnectorPool.get(id);
                Boolean flag = MysqlDataSource.isNotWorked(connection);
                if (!flag){
                    throw  new DataSourceException(CommonConstants.DataError.DATA_IS_EXECUTING_ERROR);
                }
            }
        }
        int i = this.baseMapper.deleteDatasourceById(id);
        if(i > 0){
            return true;
        }
        return false;
    }

    /**
     * 查询 实体
     * @param id
     * @return
     */
    @Override
    public EtlDatasourceInfo findById(String id) {
        return etlDatasourceInfoDao.findById(id);
    }

    /**
     * 修改
     * @param dbManage
     * @param
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public  void updateEtl(EtlDatasourceInfo dbManage,String flag) {
        if (dbManage==null || dbManage.getId()==null){
            throw new AdException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        DataSource dataSource = new DataSource();
        BeanUtils.copyProperties(dbManage, dataSource);
        //获取 此数据源
        Connection connection = MysqlDataSource.getConnection(dataSource);
        //查看 是否在用
        Boolean worked = MysqlDataSource.isNotWorked(connection);
        if(!worked){
            //正在工作 不能进行 修改
            throw new DataSourceException(CommonConstants.DataError.DATA_IS_EXECUTING_ERROR);
        }
        //进行修改 数据库
        dbManage.setUpdateTime(new Date());
        EtlDatasourceInfo datasourceInfo = null;
        if (CommonConstants.Data.SUCCESS.equals(flag)){
            //把需要加密的 属性 进行加密
            datasourceInfo = this.dbManagePassword(dbManage);
        }
        //不加密
        datasourceInfo=dbManage;
        etlDatasourceInfoDao.updateById(datasourceInfo);
        try  {
            /*
            当数据发生修改后，需要刷新到数据源链接池中，但是不能直接刷入数据源连接池，
             必须把前一个数据源链接关闭才可以刷入数据连接池
             */
            MysqlDataSource.closeConn(connection);
            //连接 数据源 并且 存到 连接池中 把刚修改信息保存到连接池
            DataSource dataSourceUpdate = new DataSource();
            BeanUtils.copyProperties(dataSource, dataSourceUpdate);
            // 从新 存入连接池
            JdbcConnectorPool.put(dataSourceUpdate);
        } catch (Exception e) {
            e.printStackTrace();
            throw new AdException(e.getLocalizedMessage());
        }

    }
    /**
     * 把数据进行 加密
     * @param etlDatasourceInfo
     * @return
     */
    private EtlDatasourceInfo dbManagePassword(EtlDatasourceInfo etlDatasourceInfo){
        if (etlDatasourceInfo==null || etlDatasourceInfo.getIp()==null || etlDatasourceInfo.getDbName()==null
            || etlDatasourceInfo.getType()==null || etlDatasourceInfo.getPassword()==null || etlDatasourceInfo.getPort()==null
            || etlDatasourceInfo.getUsername()==null){
            throw new AdException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //对 该属性 进行 加密
        String ip = DesUtils.getEncryptString(etlDatasourceInfo.getIp());
        etlDatasourceInfo.setIp(ip);
        String username = DesUtils.getEncryptString(etlDatasourceInfo.getUsername());
        etlDatasourceInfo.setUsername(username);
        String dbName = DesUtils.getEncryptString(etlDatasourceInfo.getDbName());
        etlDatasourceInfo.setDbName(dbName);
        String port = DesUtils.getEncryptString(etlDatasourceInfo.getPort());
        etlDatasourceInfo.setPort(port);
        String password = DesUtils.getEncryptString(etlDatasourceInfo.getPassword());
        etlDatasourceInfo.setPassword(password);
        String type = DesUtils.getEncryptString(etlDatasourceInfo.getType());
        etlDatasourceInfo.setType(type);
        return etlDatasourceInfo;
    }

    /**
     * 测试连接
     * @param etlDatasourceInfoEntity
     * @return
     */
    @Override
    public void testConnection(EtlDatasourceInfo etlDatasourceInfoEntity) {
        DataSource dataSource = new DataSource();
        BeanUtils.copyProperties(etlDatasourceInfoEntity,dataSource);
        if (CommonConstants.Data.MYSQL.equalsIgnoreCase(dataSource.getType())){
            Boolean flag = new MysqlDataSource().testConnation(dataSource);
            if (!flag){
                throw new DataSourceException(CommonConstants.DataError.DATA_ERROR);
            }
        }
        if (CommonConstants.Data.REDIS.equalsIgnoreCase(dataSource.getType())){
            Boolean flag = new RedisDataSource().testConnation(dataSource);
            if (!flag){
                throw new DataSourceException(CommonConstants.DataError.DATA_ERROR);
            }
        }
    }
    /**
     * 数据源添加
     * @param datasourceInfo
     */
    @Override
    public void saveInfo(EtlDatasourceInfo datasourceInfo) {
        //拷贝到数据源对象
        DataSource dataSource = new DataSource();
        BeanUtils.copyProperties(datasourceInfo, dataSource);
        //调用测试
        this.testConnection(datasourceInfo);
        //添加
        datasourceInfo.setCreateTime(new Date());
        datasourceInfo.setUpdateTime(new Date());
        datasourceInfo.setTombstone(0);
        save(datasourceInfo);
        //判断该数据源是否启用 如果启用则添加进连接池
        if (0==datasourceInfo.getState()){
            dataSource.setId(datasourceInfo.getId());
            if (CommonConstants.Data.MYSQL.equalsIgnoreCase(dataSource.getType())){
               JdbcConnectorPool.put(dataSource);
            }
            if (CommonConstants.Data.REDIS.equalsIgnoreCase(dataSource.getType())){
               RedisConnectorPool.put(dataSource);
            }
        }

    }

    /**
     * 查询 表字段
     */
    @Override
    public ArrayList<FieldType> showFieldAndType(String database,String table,String id) {
        //创建表字段 类型表
        ArrayList<FieldType> fieldTypes = new ArrayList<>();
        //获取连接
        Connection connection = jdbcPoolMap.get(id);

        Statement statement = null;
        try {
            statement = connection.createStatement();
            //使用数据库
            statement.execute("USE "+database);
            //查询表字段，类型
            List<Map<String, Object>> allFieldAndType = MysqlDataSource.executeQuerySql(connection, "desc "+table);
            /**
             * 存入数据库字段类型
             */
            allFieldAndType.forEach(a -> {
                FieldType fieldType = new FieldType();
                fieldType.setField(a.get("Field").toString());
                //varchar（255）->string
                if (a.get(CommonConstants.Data.TYPE).toString().contains(CommonConstants.Data.VARCHAR)){
                    fieldType.setType("String");
                //int（11）->int
                }else if(a.get(CommonConstants.Data.TYPE).toString().contains(CommonConstants.Data.INT)){
                    fieldType.setType("int");
                }else {
                    fieldType.setType(a.get(CommonConstants.Data.TYPE).toString());
                }
                fieldTypes.add(fieldType);
            });
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return fieldTypes;
    }

    @Override
    public List showDatabase(String database,String id) {

        List databaseList = new ArrayList<>();
        List databaseLikeList = new ArrayList<>();
        //获取连接
        Connection connection = jdbcPoolMap.get(id);

        //查询所有数据库
        List<Map<String, Object>> allDatabase = MysqlDataSource.executeQuerySql(connection, "SHOW DATABASES");
        /**
         * 存入数据库列表
         */
        allDatabase.forEach(a -> {
            databaseList.add(a.get("Database").toString());
        });
        /**
         * 数据库字段查询
         */
        databaseList.forEach(data->{
            if (database != null && database != ""){
                if (data.equals(database)){
                    databaseLikeList.add(data);
                }
            }
        });
        if (databaseLikeList !=null && databaseLikeList.size()>0){
            return databaseLikeList;
        }
        return databaseList;
    }
    /**
     * 查询数据库对应所有表
     * @param database 数据库
     * @return
     */
    @Override
    public List showTables(String database,String table,String id) {
        //获取所有数据库
        List databaseList = this.showDatabase("",id);

        if (! databaseList.contains(database)){
            //没有此数据库
            throw new AdException(CommonConstants.BiMassage.DATABASE_NOT_EXIST);
        }
        List tableList = new ArrayList<>();
        //获取连接
        Connection connection = jdbcPoolMap.get(id);

        //查询库中所有表名
        List<Map<String, Object>> allTable  = MysqlDataSource.executeQuerySql(connection, "SHOW TABLES FROM " + database);
        /**
         * 存入表列表
         */
        allTable.forEach(a -> {
            tableList.add(a.get("Tables_in_"+database).toString());
        });
        List tableLikeList = new ArrayList<>();

        /**
         * 表字段查询
         */
        tableList.forEach(tab ->{
            if (table != ""  && table != null){
                if (tab.equals(table)){
                    tableLikeList.add(tab);
                }
            }
        });
        if (tableLikeList != null && tableLikeList.size()!=0){
            return tableLikeList;
        }
        return tableList;
    }

    /**
     * 根据sql与查询
     * @param database
     * @param table
     * @param sql
     * @param id
     * @return
     */
    @Override
    public Map findData(String database,String table,String sql,String id) {
        //获取所有表
        List tableList = this.showTables(database,"",id);
        if (!tableList.contains(table)){
            //该库中没有该表
            throw new AdException(CommonConstants.BiMassage.TABLE_NOT_EXIST);

        }
        HashMap sqlDatamap = new HashMap<>(16);
        ArrayList<Map<String, Object>> sqlDataList = new ArrayList<>();

        Connection connection = jdbcPoolMap.get(id);

        //sql查询表数据
        List<Map<String, Object>> sqlData  = MysqlDataSource.executeQuerySql(connection, sql+" WHERE tombstone=0");

        sqlData.forEach(sqlDatum ->{
            Map<String, Object> mapData = sqlDatum;
            Map dataMap = new HashMap<>(16);
            mapData.forEach((key,value)->{
                //根据name，password,address脱敏
                if(key.contains(CommonConstants.Data.NAME) || key.contains(CommonConstants.Data.PASSWORD) ||key.contains(CommonConstants.Data.ADDRESS)){
                    value="***";
                }
                dataMap.put(key,value);
            });
            sqlDataList.add(dataMap);
        });

        //查询表字段，类型
        ArrayList<FieldType> fieldTypes = this.showFieldAndType(database, table,id);
        sqlDatamap.put("sqlDataList",sqlDataList);
        sqlDatamap.put("fieldTypes",fieldTypes);
        //执行时间
        sqlDatamap.put("startTime",new Date());

        return sqlDatamap;
    }

    /**
     * 列表模糊+分页
     * @param etlDatasourceInfoRequest
     * @param pageSize
     * @param currPage
     * @param flag
     * @return
     * @throws Exception
     */
    @Override
    public CommonResponse getEtlDatasourceInfoList(PageListParam etlDatasourceInfoRequest, Integer pageSize, Integer currPage, String flag){
        if (pageSize> CommonConstants.Data.MAXTOTAL){
            //每页不能超过50条
            throw new AdException(CommonConstants.ErrorMsg.TOTAL_NO_MORE_THAN);
        }
        //查询列表
        List<EtlDatasourceInfoVo> dbManageList = etlDatasourceInfoDao.getEtlDatasourceInfoList(etlDatasourceInfoRequest.getDbName(),
                etlDatasourceInfoRequest.getType(), etlDatasourceInfoRequest.getDescription(),pageSize,(currPage-1)*pageSize);

        //总条数
        int totalCount = etlDatasourceInfoDao.getCount();
        PageUtils pageUtils = new PageUtils(dbManageList, totalCount, pageSize, currPage);
        //DES加密
        if (!CommonConstants.Data.ZERO.equals(flag)){
            String encryptData = DesUtils.getEncryptString(JSON.toJSONString(pageUtils));
            return  new CommonResponse(CommonStatus.VALID,encryptData);
        }
        //不加密数据
        return  new CommonResponse(CommonStatus.VALID,pageUtils);
    }

    /**
     * 查询 全部
     * @return
     */
    @Override
    public List<EtlDatasourceInfo> selectAll() {
        return this.baseMapper.selectAll();
    }

    /**
     * 初始化连接池
     * @param etlDatasourceInfoList
     */
    @Override
    public void initConnactionPool(List<EtlDatasourceInfo> etlDatasourceInfoList) {

        //获取状态为启动的所有连接
        List<DataSource> collect = etlDatasourceInfoList.stream().filter(datasourceInfo ->
                //过滤未启动的连接
                datasourceInfo.getState() == 0
        ).map((datasourceInfo) -> {
            DataSource dataSource = new DataSource();
            BeanUtils.copyProperties(datasourceInfo, dataSource);
            return dataSource;
        }).collect(Collectors.toList());

        //获取mysql连接
        List<DataSource> mysqlCollect = collect.stream().filter(dataSource -> {
            return CommonConstants.Data.MYSQL.equalsIgnoreCase(dataSource.getType());
        }).collect(Collectors.toList());

        //获取redis连接
        List<DataSource> redisrCollect = collect.stream().filter(dataSource -> {
            return CommonConstants.Data.REDIS.equalsIgnoreCase(dataSource.getType());
        }).collect(Collectors.toList());

        JdbcConnectorPool.initJdbcPoolMap(mysqlCollect);
        RedisConnectorPool.initRedisPoolMap(redisrCollect);

    }


    @Override
    public CommonResponse connection(String id) {
        EtlDatasourceInfo daoById = etlDatasourceInfoDao.findById(id);
        if (daoById==null){
            throw  new DataSourceException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        DataSource dataSource = new DataSource();
        BeanUtils.copyProperties(daoById, dataSource);
        //连接
        JdbcConnectorPool.put(dataSource);
        return null;
    }

    /**
     * 查询所有数据源
     */
    @Override
    public String selectAllDatasource() {
        //查询所有数据源
        List<EtlDatasourceInfo> dataSources = etlDatasourceInfoDao.selectAll();
        //转json
        String dataSourcesString = JSON.toJSONString(dataSources);
        return dataSourcesString;
    }


    /**
     * 根据id查询并连接数据源,并查询该数据库中所有表名
     * @param id id
     * @return 所有表名和id的集合转JSON字符串
     */
    @Override
    public String selectDatasourceById(String id) {
        //根据id查询数据源
        EtlDatasourceInfo dataSourceDTO = etlDatasourceInfoDao.selectDatasourceById(id);
        EtlDatasourceInfo dataSourceEntity = convertDataSourceDtoToEntity(dataSourceDTO);
        //连接数据源
        Connection connection = JdbcConnectorPool.get(dataSourceDTO.getId());
        //定义查询所有表名的sql,并执行
        String sql = "show tables";
        List<Map<String, Object>> maps = MysqlDataSource.executeQuerySql(connection, sql);
        String dataSourceString = JSON.toJSONString(maps);
        return dataSourceString;
    }

    /**
     * @param id 数据源id
     * @param tableName 表名
     * @param asName 别名
     */
    @Override
    public String selectAllFieldByTableName(String id, String tableName, String asName) {
        HashMap<String, String> map = new HashMap<>(16);
        //根据id查询数据源
        EtlDatasourceInfo dataSourceDTO = etlDatasourceInfoDao.selectDatasourceById(id);
        //存入map
        map.put("dbName",dataSourceDTO.getDbName());

        map.put("tableName",tableName);

        List<TableColumnDTO> tableColumnDTOList = etlDatasourceInfoDao.selectAllFieldByTableName(map);

        tableColumnDTOList.stream().peek(tableColumnDTO -> {
            tableColumnDTO.setAsName(asName+"_"+tableColumnDTO.getColumnName());
        }).collect(Collectors.toList());

        return JSON.toJSONString(tableColumnDTOList);
    }

    /**
     * 先根据数据源id查找数据库信息,后根据数据库名与表名查询表字段
     * @param id 数据源id
     * @param tableName 表名
     * */
    @Override
    public String selectAllField(String id, String tableName) {
        Map<String, String> map = new HashMap<>(16);
        //根据id查询数据源
        EtlDatasourceInfo dataSourceDTO = etlDatasourceInfoDao.selectDatasourceById(id);
        //存入map
        map.put("dbName",dataSourceDTO.getDbName());

        map.put("tableName",tableName);

        List<TableColumnDTO> tableColumnDTOList = etlDatasourceInfoDao.selectAllFieldByTableName(map);

        return JSON.toJSONString(tableColumnDTOList);
    }

    /**
     * @param dataSourceDTO DTO
     */
    private EtlDatasourceInfo convertDataSourceDtoToEntity(EtlDatasourceInfo dataSourceDTO) {
        if (dataSourceDTO == null) {
            throw new AdException(CommonConstants.DataError.DATA_SELECT_ERROR);
        }
        EtlDatasourceInfo entity = new EtlDatasourceInfo();
        //数据拷贝
        BeanUtils.copyProperties(dataSourceDTO, entity);
        return entity;
    }
}