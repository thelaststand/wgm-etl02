package com.wgm.datasource.dto;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author songhuan
 * @email 3325074245@qq.com
 * @date 2021/1/13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DataSourceDTO {
    /**
     * id
     */
    @TableId
    private String id;
    /**
     * 数据源描述
     */
    private String description;
    /**
     * 数据源类型ads
     */
    private String type;
    /**
     * 数据库IP
     */
    private String ip;
    /**
     * 端口
     */
    private String port;
    /**
     * 数据库名称
     */
    private String dbName;
    /**
     * 数据源用户名
     */
    private String username;
    /**
     * 数据源密码
     */
    private String password;
    /**
     * 额外配置参数
     */
    private String params;
    /**
     * 状态
     */
    private Integer state;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 最后更新时间
     */
    private Date updateTime;
    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * tombstone 逻辑删除
     */
    @TableLogic(value = "0",delval = "1")
    private Integer tombstone;

}
