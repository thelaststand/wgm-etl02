package com.wgm.datasource.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author pangpang
 * @email 1963148908@qq.com
 * @date 2021/1/12 20:25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FieldType {


    /**
     * 字段
     */
    String field;
    /**
     * 类型
     */
    String type;


}
