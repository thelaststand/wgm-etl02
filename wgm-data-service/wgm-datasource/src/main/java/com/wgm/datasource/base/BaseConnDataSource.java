package com.wgm.datasource.base;

import com.wgm.datasource.pojo.DataSource;

import java.sql.SQLException;

/**
 * @author kangbaby
 */
public interface BaseConnDataSource {
    /**
     * 测试连接
     * @param dataSource
     * @return
     * @throws SQLException
     */
    Boolean testConnation(DataSource dataSource) throws SQLException;
}
