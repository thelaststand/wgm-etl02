package com.wgm.datasource.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.common.constant.CommonResponse;
import com.wgm.datasource.entity.EtlDatasourceInfo;
import com.wgm.datasource.pojo.FieldType;
import com.wgm.datasource.pojo.PageListParam;

import java.util.List;
import java.util.Map;

/**
 * 数据库管理
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-06 14:40:33
 */
public interface EtlDatasourceInfoService extends IService<EtlDatasourceInfo> {

    /**
     * 删除 数据源 实体
     * @param id
     * @return
     */
    Boolean deleteDatasourceById(String id);

    /**
     * 查询 实体
     * @param id
     * @return
     */
    EtlDatasourceInfo findById(String id);

    /**
     * 删除
     * @param datasourceInfo
     * @param flag
     */
    void updateEtl(EtlDatasourceInfo datasourceInfo,String flag);

    /**
     * 添加
     * @param datasourceInfo
     */
    void saveInfo(EtlDatasourceInfo datasourceInfo) ;

    /**
     * 测试连接
     * @param etlDatasourceInfoEntity
     */
    void testConnection(EtlDatasourceInfo etlDatasourceInfoEntity);
    /**
     * 列表模糊+分页
     * @param etlDatasourceInfoRequest
     * @param pageSize
     * @param currPage
     * @param flag
     * @return
     * @throws Exception
     */
    CommonResponse getEtlDatasourceInfoList(PageListParam etlDatasourceInfoRequest, Integer pageSize, Integer currPage, String flag ) throws Exception;
    /**
     * 查询 全部
     * @return
     */
    List<EtlDatasourceInfo> selectAll();

    /**
     * 初始化 连接池
     * @param etlDatasourceInfoList 数据源信息
     */
    void initConnactionPool(List<EtlDatasourceInfo> etlDatasourceInfoList);

    /**
     * 查询 数据源
     * @param database 数据库
     * @param id id
     * @return
     */
    List showDatabase(String database,String id);

    /**
     * 查询 所有的表
     * @param database 数据库
     * @param table 表名
     * @param id id
     * @return
     */
    List showTables(String database,String table,String id);

    /**
     * 查询 数据
     * @param database 数据库
     * @param table 表名
     * @param sql sql
     * @param id 数据源id
     * @return
     */
    Map findData(String database,String table,String sql,String id);

    /**
     * 查询表中所有字段,类型
     * @param database 数据库
     * @param table 表名
     * @param id 数据源id
     * @return
     */
    List<FieldType> showFieldAndType(String database, String table, String id);

    /**
     *  连接数据库
     * @param id
     * @return
     */
    CommonResponse connection(String id);

    /**
     * 查询素有数据源
     * @return
     */
    String selectAllDatasource();

    /**
     * 根据id查询并连接数据源,并查询该数据库中所有表名
     * @param id
     * @return
     */
    String selectDatasourceById(String id);

    /**
     * 先根据数据源id查找数据库信息,后根据数据库名与表名查询表字段 ,拼接字符串
     * @param id 数据源id
     * @param tableName 表名
     * @param asName 别名
     * @return
     */
    String selectAllFieldByTableName(String id, String tableName, String asName);
    /**
     * 先根据数据源id查找数据库信息,后根据数据库名与表名查询表字段
     * @param id 数据源id
     * @param tableName 表名
     * @return
     */
    String selectAllField(String id, String tableName);


}

