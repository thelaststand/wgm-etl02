package com.wgm.datasource.pool;

import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.wgm.common.constant.CommonConstants;
import com.wgm.common.exception.DataSourceException;
import com.wgm.datasource.pojo.DataSource;
import com.wgm.datasource.base.MysqlDataSource;


/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/7
 */
public class JdbcConnectorPool {


    public static  Map<String, Connection> jdbcPoolMap = new ConcurrentHashMap<>(16);

    /**
     * @Description 初始化连接池
     * @Author WangZhuoRan
     * @Date 2021-01-07 10:21
     * @param info 所有数据源信息
     */
    public static void initJdbcPoolMap(List<DataSource> info){
        info.forEach(dataSource -> {
            put(dataSource);
        });

    }

    /**
     * 添加连接
     * @param dataSource 数据源信息
     */
    public static void put(DataSource dataSource){
        //测试连接
        Boolean flag = new MysqlDataSource().testConnation(dataSource);
        if (flag){
            //获取连接
            Connection connection = MysqlDataSource.getConnection(dataSource);
            //判断连接
            if (connection!=null){
                //存入连接池
                jdbcPoolMap.put(dataSource.getId(),connection);
            }
        }
    }

    /**
     * 关闭所有连接
     */
    public static void closeAll(){
        jdbcPoolMap.forEach((key, connection) -> {
           close(key);
        });
    }

    /**
     * 关闭一个连接
     * @param key
     */
    public static void close(String key) {

        synchronized (jdbcPoolMap.get(key)) {
            Connection connection = jdbcPoolMap.get(key);
            MysqlDataSource.closeConn(connection);
        }

    }

    /**
     * 获取一个连接
     * @param key
     */
    public static Connection get(String key) {
        if (!jdbcPoolMap.containsKey(key)){
            throw new DataSourceException(CommonConstants.DataError.CONNETTION_ERROR);
        }
        Connection connection = jdbcPoolMap.get(key);
        return connection;
    }


}
