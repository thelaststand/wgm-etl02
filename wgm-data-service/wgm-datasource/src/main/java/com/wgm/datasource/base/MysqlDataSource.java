package com.wgm.datasource.base;

import com.wgm.common.constant.CommonConstants;
import com.wgm.common.exception.DataSourceException;
import com.wgm.datasource.pojo.DataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/6
 */
public class MysqlDataSource implements BaseConnDataSource{

    /**
     * 获取url
     * @param dataSource
     * @return
     */
    public static String getUrl(DataSource dataSource){
        StringBuilder url = new StringBuilder(CommonConstants.Jdbc.MYSQL_PRE);
        url.append(dataSource.getIp());
        url.append(":");
        url.append(dataSource.getPort());
        url.append("/");
        url.append(dataSource.getDbName());
        url.append("?");
        url.append(dataSource.getParams());
        return url.toString();
    }

    /**
     * 获取连接
     * @param dataSource
     * @return
     */
    public static Connection getConnection(DataSource dataSource){
        String url = getUrl(dataSource);
        System.out.println(url);
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url.toString(), dataSource.getUsername(), dataSource.getPassword());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new DataSourceException(CommonConstants.DataError.DATA_CONNETTION_ERROR);
        }
        return connection;
    }

    /**
     * 测试
     * @param dataSource
     * @return
     */
    @Override
    public  Boolean testConnation(DataSource dataSource) {
        boolean flag=false;
        Connection conn =null;
        String url = getUrl(dataSource);
        try {
            conn = getConnection(dataSource);
            String version = this.selectVersion(conn);
            if (version!=null){
                flag=true;
            }
        } finally {
            closeConn(conn);
        }
        return flag;
    }

    /**
     * 判断是否在工作
     * @param connection
     * @return
     */
    public static Boolean isNotWorked (Connection connection){
        try {
            String query = connection.getClientInfo("isworking");
            if (query==null || CommonConstants.Jdbc.IS_WORKING.equals(query)){
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    /**
     * 改变工作状态
     * @param connection
     * @return
     */
    public static Boolean delWorked (Connection connection){
        try {
            connection.setClientInfo("isworking","0");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    /**
     * 关闭数据流
     * @param ps
     * @param rs
     */
    public static void closeAll(PreparedStatement ps, ResultSet rs){
        try {
            if (ps != null){
                ps.close();
            }
            if (rs != null){
                rs.close();
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw  new DataSourceException(CommonConstants.DataError.DATA_CLOSE_ERROR);
        }
    }
    /**
     * 关闭连接
     * @param conn
     */
    public static void closeConn(Connection conn){
        try {
            if (conn != null){
                conn.close();
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw  new DataSourceException(CommonConstants.DataError.CONNETTION_CLOSE_ERROR);
        }
    }

    /**
     * 关闭数据流ps
     * @param ps
     */
    public static void closePs(PreparedStatement ps){
        try {
            if (ps != null){
                ps.close();
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw  new DataSourceException(CommonConstants.DataError.DATA_CLOSE_ERROR);
        }
    }

    /**
     * 关闭数据流rs
     * @param rs
     */
    public static void closeRs(ResultSet rs){
        try {
            if (rs != null){
                rs.close();
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw  new DataSourceException(CommonConstants.DataError.DATA_CLOSE_ERROR);
        }
    }

    /**
     * 获取属性
     * @param rs
     * @return
     * @throws SQLException
     */
    public static List<String> getField(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        List<String> fieldList = new ArrayList<>();
        int columnCount = metaData.getColumnCount();
        for (int i = 1; i <= columnCount; i++) {
            fieldList.add(metaData.getColumnLabel(i));
        }
        return fieldList;
    }

    /**
     * 查询
     * @param connection
     * @param sql
     * @return
     */
    public static List<Map<String, Object>> executeQuerySql(Connection connection,String sql) {

        List<Map<String,Object>> dataList = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (isNotWorked(connection)){
                connection.setClientInfo("isworking","1");
            }else {
                throw new DataSourceException(CommonConstants.DataError.DATA_IS_EXECUTING_ERROR);
            }
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            List<String> fields = getField(rs);
            while (rs.next()){
                Map<String, Object> data = new HashMap<>(16);
                for (String field : fields) {
                    data.put(field,rs.getObject(field));
                }
                dataList.add(data);
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new DataSourceException(CommonConstants.DataError.DATA_SELECT_ERROR);
        }finally {
            delWorked(connection);
            closeAll(ps,rs);
        }
        return dataList;
    }


    /**
     * 查看版本
     * @param connection
     * @return
     */
    public String selectVersion(Connection connection) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (isNotWorked(connection)){
                connection.setClientInfo("isworking","1");
            }else {
                throw new DataSourceException(CommonConstants.DataError.DATA_IS_EXECUTING_ERROR);
            }
            ps = connection.prepareStatement(CommonConstants.Jdbc.MYSQLBASEQUERY);
            rs = ps.executeQuery();
            if (rs.next()){
                return rs.getString(CommonConstants.Jdbc.SHOWVERSION);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new DataSourceException(CommonConstants.DataError.DATA_SELECT_ERROR);
        }finally {
            delWorked(connection);
            closeAll(ps,rs);
        }
        return null;
    }






    /**
     * 操作
     * @param sql
     * @param valueList
     * @param connection
     * @return
     */
    public int executeUpdateSql(String sql, List<Object> valueList,Connection connection) {
        PreparedStatement ps = null;
        try {
            if (isNotWorked(connection)){
                connection.setClientInfo("isworking","1");
            }else {
                throw new DataSourceException(CommonConstants.DataError.DATA_IS_EXECUTING_ERROR);
            }
            ps =connection.prepareStatement(sql);
            for (int i = 1; i <= valueList.size(); i++) {
                ps.setObject(i , valueList.get(i-1));
            }
            return ps.executeUpdate();
        }catch (SQLException throwables){
            throwables.printStackTrace();
        }finally {
            delWorked(connection);
            closePs(ps);
        }
        return 0;
    }

    /**
     * 获取数量
     * @param dataCountSql
     * @param connection
     * @return
     */
    public long executeCountSql(String dataCountSql,Connection connection) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            if (isNotWorked(connection)){
                connection.setClientInfo("isworking","1");
            }else {
                throw new DataSourceException(CommonConstants.DataError.DATA_IS_EXECUTING_ERROR);
            }
            ps = connection.prepareStatement(dataCountSql);
            rs = ps.executeQuery();
            if (rs.next()){
                return rs.getLong(CommonConstants.Jdbc.SHOWCOUNT);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new DataSourceException(CommonConstants.DataError.DATA_SELECT_ERROR);
        }finally {
            delWorked(connection);
            closeAll(ps,rs);
        }
        return 0;
    }


}
