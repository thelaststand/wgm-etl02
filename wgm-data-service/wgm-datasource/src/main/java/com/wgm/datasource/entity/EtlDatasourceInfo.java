package com.wgm.datasource.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import org.springframework.transaction.annotation.Transactional;

/**
 * 数据库管理
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-06 14:40:33
 */
@Data
@TableName("etl_datasource_info")
public class EtlDatasourceInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 数据源描述
	 */
	private String description;
	/**
	 * 数据源类型ads
	 */
	private String type;
	/**
	 * 数据库IP
	 */
	private String ip;
	/**
	 * 端口
	 */
	private String port;
	/**
	 * 数据库名称
	 */
	private String dbName;
	/**
	 * 数据源用户名
	 */
	private String username;
	/**
	 * 数据源密码
	 */
	private String password;
	/**
	 * 额外配置参数
	 */
	private String params;
	/**
	 * 状态
	 */
	private Integer state;
	/**
	 * 备注
	 */
	private String remarks;
	/**
	 * 最后更新时间
	 */
	private Date updateTime;
	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * tombstone 逻辑删除
	 */
	@TableLogic(value = "0",delval = "1")
	private Integer tombstone;

}
