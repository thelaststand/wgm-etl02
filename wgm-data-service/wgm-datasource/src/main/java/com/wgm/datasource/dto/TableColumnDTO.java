package com.wgm.datasource.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author songhuan
 * @email 3325074245@qq.com
 * @date 2021/1/13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TableColumnDTO {

    /**
     * 字段名
     */
    private String columnName;

    /**
     * 字段类型/长度
     */
    private String columnType;

    /**
     * 非空
     */
    private String isNullable;

    /**
     * 注释
     */
    private String columnComment;

    /**
     * 处理规则
     */
    private String rules;

    /**
     * 表别名
     */
    private String asName;
}
