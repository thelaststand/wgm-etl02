package com.wgm.datasource.pool;

import com.wgm.common.constant.CommonConstants;
import com.wgm.common.exception.DataSourceException;
import com.wgm.datasource.pojo.DataSource;
import com.wgm.datasource.base.RedisDataSource;
import redis.clients.jedis.Jedis;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/7
 */
public class RedisConnectorPool {

    public static Map<String, Jedis> redisPoolMap = new ConcurrentHashMap<>(16);

    /**
     * 初始化所有连接
     *
     * @param dataSources 数据源集合
     */
    public static void initRedisPoolMap(List<DataSource> dataSources) {
        dataSources.forEach(dataSource -> {
            put(dataSource);
        });
    }

    /**
     * 存入连接池
     * @param dataSource
     */
    public static void put(DataSource dataSource){
        //测试连接
        Boolean flag = new RedisDataSource().testConnation(dataSource);
        if (flag){
            //获取连接
            Jedis jedis = RedisDataSource.getJedis(dataSource);
            //判断连接
            if(jedis!=null){
                //存入连接池
                redisPoolMap.put(dataSource.getId(), jedis);
            }
        }

    }

    /**
     * 关闭所有连接
     */
    public static void closeAll(){
        redisPoolMap.forEach((key, connection) -> {
            close(key);
        });
    }

    /**
     * 关闭连接
     * @param key redis数据源id
     */
    public static void close(String key) {
        synchronized (redisPoolMap.get(key)) {
            redisPoolMap.get(key).close();
        }
    }

    /**
     * 获取一个连接
     * @param key
     */
    public static Jedis get(String key) {
        if (!redisPoolMap.containsKey(key)){
            throw new DataSourceException(CommonConstants.DataError.CONNETTION_ERROR);
        }
        Jedis jedis = redisPoolMap.get(key);
        return jedis;
    }


}
