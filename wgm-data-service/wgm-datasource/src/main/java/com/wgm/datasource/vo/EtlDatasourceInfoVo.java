package com.wgm.datasource.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author pangpang
 * @email 1963148908@qq.com
 * @date 2021/1/6 19:50
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EtlDatasourceInfoVo {


    /**
     * 数据源名称
     */
    String dbName;
    /**
     * 数据源描述
     */
    private String description;
    /**
     * 备注/说明
     */
    String remarks;
    /**
     * 数据源类型
     */
    String type;
    /**
     * 状态
     */
    String state;

    /**
     * 数据库IP
     */
    private String ip;

    /**
     * 端口
     */
    private String port;
    /**

    /**
     * 数据源密码
     */
    private String password;
    /**

    /**
     * 数据源用户名
     */
    private String username;
}
