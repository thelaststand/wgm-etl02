package com.wgm.datasource.controller;


import com.wgm.common.constant.CommonConstants;
import com.wgm.aop.anno.LogAnno;
import com.wgm.datasource.service.EtlDatasourceInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
import java.util.Map;

/**
*   class
*   @Author 小脸蛋
*   @CreateDate 2021/1/13
*/

@RestController
@RequestMapping("datasource/bi")
@Slf4j
public class EtlDataSourceBiController {

    @Autowired
    private EtlDatasourceInfoService etlDatasourceInfoService;

    /**
     * 查询所有数据库
     * @return
     */
    @GetMapping("showDatabase")
    @LogAnno(operationName = "查询所有数据库",operationType = CommonConstants.OperationType.SELECT)
    public List showDatabase(@RequestParam(value = "database",defaultValue = "")String database,
                             @RequestParam("id")String id){
        return etlDatasourceInfoService.showDatabase(database,id);
    }

    /**
     * 查询数据库对应所有表
     * @param database 数据库
     * @param table 模糊表
     * @return
     */
    @GetMapping("showTables")
    @LogAnno(operationName = "查询数据库对应所有表",operationType = CommonConstants.OperationType.SELECT)
    public List showTables(@RequestParam("database")String database,
                           @RequestParam(value = "table",defaultValue = "")String table,
                           @RequestParam("id")String id){
        return etlDatasourceInfoService.showTables(database,table,id);
    }

    /**
     * 根据sql与查询
     * @param database
     * @param table
     * @param sql
     * @return
     */
    @GetMapping("findData")
    @LogAnno(operationName = "根据sql与查询",operationType = CommonConstants.OperationType.SELECT)
    public Map findData(@RequestParam("database")String database,
                        @RequestParam("table")String table,
                        @RequestParam("sql")String sql,
                        @RequestParam("id")String id){
        return etlDatasourceInfoService.findData(database,table,sql,id);
    }


}
