package com.wgm.datasource.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
*   class
*   @Author 小脸蛋
*   @CreateDate 2021/1/8
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageListParam implements Serializable {

    /**
     * 数据源描述
     */
    private String description;
    /**
     * 数据源类型ads
     */
    private String type;
    /**
     * 数据库名称
     */
    private String dbName;
}
