package com.wgm.datasource.base;

import com.wgm.common.constant.CommonConstants;
import com.wgm.common.exception.DataSourceException;
import com.wgm.datasource.pojo.DataSource;
import org.springframework.util.StringUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;


/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/9
 */
public class RedisDataSource implements BaseConnDataSource{

    /**
     * 测试连接
     * @param dataSource
     * @return
     */
    @Override
    public Boolean testConnation(DataSource dataSource) {
        Jedis jedis = null;
        try {
            jedis = getJedis(dataSource);
            jedis.ping();
            //数据操作
            jedis.set("redis", "redis");
            String test = jedis.get("redis");
            if (CommonConstants.Data.REDIS.equals(test)) {
                jedis.del("redis");
                return true;
            }
            return false;
        } catch (Exception e) {
            throw new DataSourceException(CommonConstants.DataError.DATA_CONNETTION_ERROR);
        } finally {
            jedis.close();
        }
    }


    /**
     * 获取jedis
     * @param dataSource
     * @return
     */
    public static Jedis getJedis(DataSource dataSource){
            //创建redis连接池配置对象
            JedisPoolConfig jpc = new JedisPoolConfig();
            //最大连接空闲数
            jpc.setMaxIdle(20);
            //最大连接数
            jpc.setMaxTotal(50);
            //创建连接池对象
            JedisPool jp = new JedisPool(jpc, dataSource.getIp(), Integer.parseInt(dataSource.getPort()));
            Jedis jedis = jp.getResource();
            if (!StringUtils.isEmpty(dataSource.getPassword())) {
                jedis.auth(dataSource.getPassword());
            }
            return jedis;
    }


}
