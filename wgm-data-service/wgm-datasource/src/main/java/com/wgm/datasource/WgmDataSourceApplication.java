package com.wgm.datasource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/6
 */
@SpringBootApplication(scanBasePackages = {"com.wgm.datasource","com.wgm.aop"})
@EnableDiscoveryClient
@MapperScan("com.wgm.datasource.dao")
@EnableAspectJAutoProxy
@EnableTransactionManagement
public class WgmDataSourceApplication {
    public static void main(String[] args) {
        SpringApplication.run(WgmDataSourceApplication.class,args);
    }
}

