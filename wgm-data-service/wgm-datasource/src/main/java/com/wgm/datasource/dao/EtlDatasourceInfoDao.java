package com.wgm.datasource.dao;

import com.wgm.datasource.entity.EtlDatasourceInfo;
import com.wgm.datasource.vo.EtlDatasourceInfoVo;
import com.wgm.datasource.dto.TableColumnDTO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 数据库管理
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-06 14:40:33
 */
@Mapper
@Component
public interface EtlDatasourceInfoDao extends BaseMapper<EtlDatasourceInfo> {


    /**
     * 根据id查询
     * @param id id
     * @return
     */
    EtlDatasourceInfo findById(@Param("id") String id);

    /**
     *查询数据源信息
     * @param dbName 数据源名称
     * @param type 类型
     * @param description 描述
     * @param pageSize 每页条数
     * @param currPage 当前页
     * @return
     */
    List<EtlDatasourceInfoVo> getEtlDatasourceInfoList(@Param("dbName") String dbName,
                                                       @Param("type") String type,
                                                       @Param("description") String description,
                                                       @Param("pageSize") Integer pageSize,
                                                       @Param("currPage") Integer currPage);

    /**
     *获取数据源数量
     * @return
     */
    int getCount();

    /**
     * 根据id查找数据源信息
     * @param id
     * @return
     */
    EtlDatasourceInfo selectDatasourceById(@Param("id") String id);

    /**
     * 根据id删除数据源信息
     * @param id
     * @return
     */
    int deleteDatasourceById(String id);

    /**
     * 查询所有数据源
     * @return
     */
    List<EtlDatasourceInfo> selectAll();


    /**
     *查询所有表字段
     * @param map
     * @return
     */
    List<TableColumnDTO> selectAllFieldByTableName(Map<String, String> map);
}
