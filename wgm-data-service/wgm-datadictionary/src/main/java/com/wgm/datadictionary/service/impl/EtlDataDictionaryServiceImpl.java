package com.wgm.datadictionary.service.impl;

import com.alibaba.nacos.common.util.UuidUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonStatus;
import com.wgm.common.exception.BaseException;
import com.wgm.datadictionary.dao.EtlDataDictionaryDao;
import com.wgm.datadictionary.entity.EtlDataDictionaryEntity;
import com.wgm.datadictionary.service.EtlDataDictionaryService;
import com.wgm.datadictionary.vo.EtlDataDictionaryVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


/**
 * 数据字典实现类
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-12 14:50:19
 */
@Service("sysParamService")
@Transactional(rollbackFor = Exception.class)
public class EtlDataDictionaryServiceImpl extends ServiceImpl<EtlDataDictionaryDao, EtlDataDictionaryEntity> implements EtlDataDictionaryService {

    @Autowired
    private EtlDataDictionaryDao etlDataDictionaryDao;


    @Override
    public void add(EtlDataDictionaryVo dataDictionaryVo) {
        EtlDataDictionaryEntity dictionaryEntity = new EtlDataDictionaryEntity();
        BeanUtils.copyProperties(dataDictionaryVo,dictionaryEntity);
        String pid = dictionaryEntity.getPid();
        //父级id为空 默认0
        if (StringUtils.isEmpty(pid)){
            dictionaryEntity.setPid("0");
        }
        //排序为空 默认0
        if (dictionaryEntity.getOrderBy()==null){
            dictionaryEntity.setOrderBy(0);
        }
        dictionaryEntity.setCreateTime(new Date());
        dictionaryEntity.setUpdateTime(new Date());
        //逻辑删除字段 默认0
        dictionaryEntity.setTombstone(0);
        //UUID
        dictionaryEntity.setId(UuidUtils.generateUuid());
        this.baseMapper.add(dictionaryEntity);

    }

    @Override
    public List<EtlDataDictionaryVo> listWithTree() {
        //获取全部数据
        List<EtlDataDictionaryEntity> entityList = this.baseMapper.selectAll();
        //获取一级菜单
        List<EtlDataDictionaryVo> firstlevelMenu = entityList.stream().filter(entity ->
                "0".equals(entity.getPid()) && entity.getTombstone() == 0
        ).map((menu) -> {
            EtlDataDictionaryVo dictionaryVo = new EtlDataDictionaryVo();
            BeanUtils.copyProperties(menu, dictionaryVo);
            //获取子节点
            dictionaryVo.setChildren(getChildren(dictionaryVo, entityList));
            return dictionaryVo;
        }).sorted((menu1,menu2)->{
            //排序
            return (menu1.getOrderBy()!=null?0:menu1.getOrderBy()) - (menu2.getOrderBy()!=null?0:menu2.getOrderBy());
        }).collect(Collectors.toList());

        return firstlevelMenu;
    }

    @Override
    public EtlDataDictionaryVo findById(String id) {
        EtlDataDictionaryEntity dictionaryEntity = this.baseMapper.findById(id);
        EtlDataDictionaryVo dictionaryVo = new EtlDataDictionaryVo();
        BeanUtils.copyProperties(dictionaryEntity,dictionaryVo);
        return dictionaryVo;
    }

    @Override
    public void update(EtlDataDictionaryVo dataDictionaryVo) {
        EtlDataDictionaryEntity dictionaryEntity = new EtlDataDictionaryEntity();
        BeanUtils.copyProperties(dataDictionaryVo,dictionaryEntity);
        this.baseMapper.updateById(dictionaryEntity);
    }

    @Override
    public void del(String id) {
        //根据id查询
        EtlDataDictionaryEntity byId = this.baseMapper.findById(id);

        if (byId==null){
            throw new BaseException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }

        //系统属性不能删除
        if (byId.getIsSystem()==CommonStatus.SYSTEM_PROPERTIES.getCode().intValue()){
            throw new BaseException(CommonConstants.ErrorMsg.SYSTEM_PROPERTIES_DEL_ERROR);
        }

        //查询下级菜单数量
        Integer count = this.baseMapper.findSubMenuCount(byId.getId());

        if (count>0){
            //查询是下级菜单
            List<EtlDataDictionaryEntity> subMenus =  this.baseMapper.findSubMenu(byId.getId());
            if (subMenus!=null&&subMenus.size()>0){
                for (EtlDataDictionaryEntity menu : subMenus) {
                    //递归删除
                    etlDataDictionaryDao.del(menu.getId());
                }
            }
        }
        etlDataDictionaryDao.del(id);
    }

    /**
     * 递归获取子节点
     * @param menu
     * @param list
     * @return
     */
    public List<EtlDataDictionaryVo> getChildren(EtlDataDictionaryVo menu,List<EtlDataDictionaryEntity> list) {

        List<EtlDataDictionaryVo> collect = list.stream().filter(entity ->
                entity.getPid().equals(menu.getId()) && entity.getTombstone() == 0
        ).map((child) -> {
            EtlDataDictionaryVo dictionaryVo = new EtlDataDictionaryVo();
            BeanUtils.copyProperties(child, dictionaryVo);
            //获取子节点
            dictionaryVo.setChildren(getChildren(dictionaryVo, list));
            return dictionaryVo;
        }).sorted((menu1,menu2)->{
            //排序
            return (menu1.getOrderBy()!=null?0:menu1.getOrderBy()) - (menu2.getOrderBy()!=null?0:menu2.getOrderBy());
        }).collect(Collectors.toList());
        return collect;
    }

}