package com.wgm.datadictionary.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 参数字典表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-12 14:50:19
 */
@Data
@TableName("etl_data_dictionary")
public class EtlDataDictionaryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId
	private String id;
	/**
	 * 编码
	 */
	private String code;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 父级id
	 */
	private String pid;
	/**
	 * 1 默认使用  0 不默认使用
	 */
	private Integer defaultStatus;
	/**
	 * 1是系统属性，不可删除，0是自定义属性可以删除
	 */
	private Integer isSystem;
	/**
	 * 排序
	 */
	private Integer orderBy;
	/**
	 * 解释、备注
	 */
	private String text;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改人 （默认为创建人）
	 */
	private String updateBy;
	/**
	 * 修改时间 （默认为创建时间）
	 */
	private Date updateTime;
	/**
	 * 1不可用 0可用
	 */
	@TableLogic(value = "0",delval = "1")
	private Integer tombstone;

}
