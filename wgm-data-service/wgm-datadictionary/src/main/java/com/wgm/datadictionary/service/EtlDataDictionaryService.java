package com.wgm.datadictionary.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.datadictionary.entity.EtlDataDictionaryEntity;
import com.wgm.datadictionary.vo.EtlDataDictionaryVo;


import java.util.List;

/**
 * 参数字典表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-12 14:50:19
 */
public interface EtlDataDictionaryService extends IService<EtlDataDictionaryEntity> {

    /**
     * 数据字典添加
     * @param dataDictionaryVo 请求参数
     * @return
     */
    void add(EtlDataDictionaryVo dataDictionaryVo);

    /**
     * 查询所有
     * @return
     */
    List<EtlDataDictionaryVo> listWithTree();

    /**
     * 根据ID查询
     * @param id 字典id
     * @return
     */
    EtlDataDictionaryVo findById(String id);

    /**
     * 数据字典修改
     * @param dataDictionaryVo 请求参数
     */
    void update(EtlDataDictionaryVo dataDictionaryVo);

    /**
     * 根据id删除
     * @param id 字典id
     */
    void del(String id);
}

