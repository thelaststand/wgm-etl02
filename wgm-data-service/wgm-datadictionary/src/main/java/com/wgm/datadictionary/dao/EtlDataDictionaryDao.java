package com.wgm.datadictionary.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.datadictionary.entity.EtlDataDictionaryEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 参数字典表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-12 14:50:19
 */
@Mapper
public interface EtlDataDictionaryDao extends BaseMapper<EtlDataDictionaryEntity> {

    /**
     *
     * 数据字典添加
     * @param dictionaryEntity 实体类
     */
    void add(@Param("dictionaryEntity") EtlDataDictionaryEntity dictionaryEntity);

    /**
     * 查询所有
     * @return
     */
    List<EtlDataDictionaryEntity> selectAll();

    /**
     * 根据id查询
     * @param id 字典id
     * @return
     */
    EtlDataDictionaryEntity findById(@Param("id") String id);

    /**
     * 查询子菜单
     * @param id
     * @return
     */
    List<EtlDataDictionaryEntity> findSubMenu(@Param("id") String id);

    /**
     * 查询子菜单数量
     * @param id
     * @return
     */
    Integer findSubMenuCount(String id);

    /**
     * 数据字段 删除
     * @param id
     */
    void del(String id);
}
