package com.wgm.datadictionary.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/13
 */
@Data
public class EtlDataDictionaryVo {

    /**
     * 主键id
     */
    @TableId
    private String id;
    /**
     * 编码
     */
    private String code;
    /**
     * 名称
     */
    private String name;
    /**
     * 父级id
     */
    private String pid;
    /**
     * 1 默认使用  0 不默认使用
     */
    private Integer defaultStatus;
    /**
     * 1是系统属性，不可删除，0是自定义属性可以删除
     */
    private Integer isSystem;
    /**
     * 排序
     */
    private Integer orderBy;
    /**
     * 解释、备注
     */
    private String text;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改人 （默认为创建人）
     */
    private String updateBy;
    /**
     * 修改时间 （默认为创建时间）
     */
    private Date updateTime;
    /**
     *子节点
     * JsonInclude(JsonInclude.Include.NON_EMPTY) 不为空才显示
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<EtlDataDictionaryVo> children;
}
