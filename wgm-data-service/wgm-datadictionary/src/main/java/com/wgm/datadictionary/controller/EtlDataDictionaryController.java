package com.wgm.datadictionary.controller;


import com.alibaba.fastjson.JSON;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonResponse;
import com.wgm.datadictionary.service.EtlDataDictionaryService;
import com.wgm.datadictionary.vo.EtlDataDictionaryVo;
import com.wgm.aop.anno.LogAnno;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 参数字典表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-12 14:50:19
 */
@RestController
@RequestMapping("data/dictionary")
@Slf4j
public class EtlDataDictionaryController {
    @Autowired
    private EtlDataDictionaryService sysParamService;

    /**
     * 数据字典添加
     * @param dataDictionaryVo 请求参数体
     * @return
     */
    @PostMapping
    @LogAnno(operationName = "数据字典添加",operationType = CommonConstants.OperationType.INSTER)
    public CommonResponse add(@RequestBody EtlDataDictionaryVo dataDictionaryVo){
        log.info("wgm-datadictionary:add->{}", JSON.toJSONString(dataDictionaryVo));
        sysParamService.add(dataDictionaryVo);
        return CommonResponse.ok();

    }

    /**
     * 数据字典查询
     * @return
     */
    @GetMapping
    @LogAnno(operationName = "数据字典查询",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse list(){
        List<EtlDataDictionaryVo> list = sysParamService.listWithTree();
        return CommonResponse.ok(list);
    }

    /**
     * 根据ID查询
     * @return
     */
    @GetMapping("/{id}")
    @LogAnno(operationName = "根据ID查询",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse findById(@PathVariable("id") String id){
        log.info("wgm-datadictionary:findById->{}", JSON.toJSONString(id));
        EtlDataDictionaryVo dataDictionaryVo = sysParamService.findById(id);
        return CommonResponse.ok(dataDictionaryVo);
    }

    /**
     * 数据字典修改
     * @param dataDictionaryVo 请求参数体
     * @return
     */
    @PutMapping
    @LogAnno(operationName = "数据字典修改",operationType = CommonConstants.OperationType.UPDATE)
    public CommonResponse update(@RequestBody EtlDataDictionaryVo dataDictionaryVo){
        log.info("wgm-datadictionary:update->{}", JSON.toJSONString(dataDictionaryVo));
        sysParamService.update(dataDictionaryVo);
        return CommonResponse.ok();
    }

    /**
     * 数据字典删除
     * @param id 字典id
     * @return
     */
    @DeleteMapping("/{id}")
    @LogAnno(operationName = "数据字典删除",operationType = CommonConstants.OperationType.DELETE)
    public CommonResponse del(@PathVariable("id") String id){
        log.info("wgm-datadictionary:del->{}", JSON.toJSONString(id));
        sysParamService.del(id);
        return CommonResponse.ok();
    }

}
