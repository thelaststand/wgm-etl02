package com.wgm.datadictionary;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/6
 */
@SpringBootApplication(scanBasePackages = {"com.wgm.datadictionary","com.wgm.aop"})
@EnableDiscoveryClient
@MapperScan("com.wgm.datadictionary.dao")
@EnableTransactionManagement
public class  WgmDataDictionaryApplication {
    public static void main(String[] args) {
        SpringApplication.run(WgmDataDictionaryApplication.class,args);
    }
}

