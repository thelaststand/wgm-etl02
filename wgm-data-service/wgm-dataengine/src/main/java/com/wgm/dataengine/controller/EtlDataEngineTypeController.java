package com.wgm.dataengine.controller;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonResponse;
import com.wgm.aop.anno.LogAnno;
import com.wgm.dataengine.service.EtlDataEngineTypeService;
import com.wgm.dataengine.vo.EtlDataEngineTypeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 规则引擎类型表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-12 14:50:19
 */
@RestController
@RequestMapping("dataengine/type")
public class EtlDataEngineTypeController {
    @Autowired
    private EtlDataEngineTypeService etlDataEngineTypeService;

    /**
     * 查询全部数据引擎类型
     * @return
     */
    @GetMapping("list")
    @LogAnno(operationName = "查询全部数据引擎类型",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse list(){
        List<EtlDataEngineTypeVo> typeVos =etlDataEngineTypeService.selectAll();
        return CommonResponse.ok(typeVos);
    }



}
