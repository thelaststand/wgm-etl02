package com.wgm.dataengine.vo;


import lombok.Data;

import java.util.Date;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/12
 */
@Data
public class EtlDataEngineVo {


    private String id;
    /**
     * 规则名称
     */
    private String engineName;
    /**
     * 规则类型id
     */
    private String engineTypeId;
    /**
     * 代码类型
     */
    private String codeType;
    /**
     * 类/函数名称
     */
    private String clName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 规则引擎代码
     */
    private String engineCode;

    /**
     * 规则类型
     */
    private String engineType;



}
