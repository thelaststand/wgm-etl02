package com.wgm.dataengine.controller;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.constant.CommonStatus;
import com.wgm.common.utils.PageUtils;
import com.wgm.dataengine.entity.EtlDataEngineEntity;
import com.wgm.dataengine.service.EtlDataEngineService;
import com.wgm.aop.anno.LogAnno;

import com.wgm.dataengine.vo.EtlDataEngineVo;
import com.wgm.dataengine.vo.EtlDataEngineWriteVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



/**
 * 数据规则引擎表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-12 14:50:19
 */
@RestController
@RequestMapping("dataengine/manage")
public class EtlDataEngineController {
    @Autowired
    private EtlDataEngineService etlDataEngineService;

    /**
     * 数据引擎列表+分页+模糊
     * @param etlDataEngineVo 索引对象
     * @param pageSize 每页条数
     * @param currPage 当前页
     * @return
     */
    @GetMapping("list")
    @LogAnno(operationName = "数据引擎列表+分页+模糊",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse list(EtlDataEngineVo etlDataEngineVo,
                               @RequestParam(value = "pageSize",defaultValue = "10 ") Integer pageSize,
                               @RequestParam(value ="currPage",defaultValue = "1") Integer currPage){
        PageUtils page = etlDataEngineService.selectDataEnginelist(etlDataEngineVo,pageSize,currPage);
        return CommonResponse.ok(page);
    }
    /**
     * 添加规则 引擎
     */
    @PostMapping("/addEngine")
    @LogAnno(operationName = "添加规则 引擎",operationType = CommonConstants.OperationType.INSTER)
    public CommonResponse add(@RequestBody EtlDataEngineEntity etlDataEngineEntity){
       return etlDataEngineService.add(etlDataEngineEntity);
    }
    /**
     * 获取 对象
     */
    @GetMapping("/{id}")
    @LogAnno(operationName = "获取数据字典对象",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse findById(@PathVariable("id")String id){
        EtlDataEngineEntity etlDataEngineEntity = etlDataEngineService.findById(id);
        return new CommonResponse(CommonStatus.VALID,etlDataEngineEntity);
    }
    /**
     * 删除 规则引擎
     */
    @PostMapping("/del/{id}")
    @LogAnno(operationName = "删除 规则引擎",operationType = CommonConstants.OperationType.DELETE)
    public CommonResponse del(@PathVariable("id")String id){
        etlDataEngineService.del(id);
        return CommonResponse.ok(CommonStatus.VALID);
    }

    /**
     * 编写 代码
     * @param etlDataEngineWriteVo
     * @return
     */
    @PostMapping("/write")
    @LogAnno(operationName = "编写 代码",operationType = CommonConstants.OperationType.INSTER)
    public CommonResponse write(@RequestBody EtlDataEngineWriteVo etlDataEngineWriteVo){
        etlDataEngineService.write(etlDataEngineWriteVo);
        return CommonResponse.ok(CommonStatus.VALID);
    }

    /**
     * 修改数据规则引擎
     */

    @PutMapping
    @LogAnno(operationName = "修改数据规则引擎",operationType = CommonConstants.OperationType.UPDATE)
    public CommonResponse edit(@RequestBody EtlDataEngineVo dataEngineVo) {
        etlDataEngineService.updateEtlDataEngine(dataEngineVo);
        return CommonResponse.ok();
    }

}
