package com.wgm.dataengine.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.dataengine.entity.EtlDataEngineTypeEntity;
import com.wgm.dataengine.vo.EtlDataEngineTypeVo;

import java.util.List;

/**
 * 规则引擎类型表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-12 14:50:19
 */
public interface EtlDataEngineTypeService extends IService<EtlDataEngineTypeEntity> {

    /**
     * 查询全部数据引擎类型
     * @return
     */
    List<EtlDataEngineTypeVo> selectAll();

    /**
     * 根据Id查询数据引擎类型
     * @param engineTypeId 数据引擎类型Id
     * @return
     */
    EtlDataEngineTypeEntity findById(String engineTypeId);
}

