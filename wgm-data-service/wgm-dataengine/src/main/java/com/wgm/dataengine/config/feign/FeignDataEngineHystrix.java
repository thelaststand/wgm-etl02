package com.wgm.dataengine.config.feign;

import com.wgm.common.constant.CommonConstants;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
*   class
*   @Author 小脸蛋
*   @CreateDate 2021/1/15
*/
@Component
public class FeignDataEngineHystrix implements FeignDataEngine{
    @Override
    public List showDatabase(String database, String id) {
        List list = new ArrayList();
        list.add(CommonConstants.ErrorMsg.FEIGN_HTSTRIX);
        return list;
    }

    @Override
    public List showTables(String database, String table, String id) {
        List list = new ArrayList();
        list.add(CommonConstants.ErrorMsg.FEIGN_HTSTRIX);
        return list;
    }

    @Override
    public Map findData(String database, String table, String sql, String id) {
        Map map=new HashMap(16);
        map.put("message","hello world is error!!");
        return map;
    }
}
