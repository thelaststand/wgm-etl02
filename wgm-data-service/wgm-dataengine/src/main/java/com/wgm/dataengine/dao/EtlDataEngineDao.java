package com.wgm.dataengine.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.dataengine.entity.EtlDataEngineEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 数据规则引擎表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-12 14:50:19
 */
@Mapper
public interface EtlDataEngineDao extends BaseMapper<EtlDataEngineEntity> {
    /**
     * 类 名称
     * @param name
     * @return
     */
    EtlDataEngineEntity findByClName(String name);

    /**
     * 数据引擎列表+分页+模糊
     * @param clName 类/函数名称
     * @param codeType 代码类型
     * @param engineName 规则名称
     * @param pageSize 每页条数
     * @param currPage 当前页
     * @return
     */
    List<EtlDataEngineEntity> selectDataEnginelist(@Param("clName") String clName,
                                                   @Param("codeType") String codeType,
                                                   @Param("engineName") String engineName,
                                                   @Param("pageSize") Integer pageSize,
                                                   @Param("currPage") Integer currPage);

    /**
     * 列表总条数
     * @param clName
     * @param codeType
     * @param engineName
     * @return
     */
    Integer getCount(@Param("clName") String clName,
                  @Param("codeType") String codeType,
                  @Param("engineName") String engineName);

    /**
     * 查找该名称条数
     * @param engineName 规则名称
     * @return
     */
    Integer findByNameCount(@Param("engineName") String engineName);

    /**
     * 修改规则引擎
     * @param engineEntity
     * @return
     */
    boolean updateEtlDataEngine(@Param("engineEntity") EtlDataEngineEntity engineEntity);
}
