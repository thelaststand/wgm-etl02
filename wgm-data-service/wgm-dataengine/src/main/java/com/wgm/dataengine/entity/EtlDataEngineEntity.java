package com.wgm.dataengine.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 数据规则引擎表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-12 14:50:19
 */
@Data
@TableName("etl_data_engine")
public class EtlDataEngineEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 规则名称
	 */
	private String engineName;
	/**
	 * 规则类型id
	 */
	private String engineTypeId;
	/**
	 * 代码类型
	 */
	private String codeType;
	/**
	 * 类/函数名称
	 */
	private String clName;
	/**
	 * 乐观锁
	 */
	private Integer revision;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新人
	 */
	private String updateBy;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 规则引擎代码
	 */
	private String engineCode;
	/**
	 * 
	 */
	private Integer tombstone;

}
