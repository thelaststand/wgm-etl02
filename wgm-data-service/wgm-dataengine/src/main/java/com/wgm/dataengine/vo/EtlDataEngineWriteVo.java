package com.wgm.dataengine.vo;

import lombok.Data;

/**
*   class
*   @Author 小脸蛋
*   @CreateDate 2021/1/14
*/
@Data
public class EtlDataEngineWriteVo {
    /**
     * id
     */
    private String id;
    /**
     * 添加的信息
     */
    private String message;
}
