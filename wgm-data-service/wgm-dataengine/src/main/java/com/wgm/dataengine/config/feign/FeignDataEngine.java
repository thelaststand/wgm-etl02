package com.wgm.dataengine.config.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author pangpang
 * @email 1963148908@qq.com
 * @date 2021/1/12 19:42
 */
@FeignClient(name = "wgm-elt-datasource",fallback = FeignDataEngineHystrix.class )
@Component
public interface FeignDataEngine {


    /**
     * 查询所有数据库
     * @param database 数据库
     * @param id 数据源id
     * @return
     */
    @GetMapping("datasource/bi/showDatabase")
    public List showDatabase(@RequestParam(value = "database",defaultValue = "")String database,
                             @RequestParam("id")String id);

    /**
     * 查询数据库对应所有表
     * @param database 数据库
     * @param table 表
     * @param id 数据源id
     * @return
     */
    @GetMapping("datasource/bi/showTables")
    public List showTables(@RequestParam("database")String database,
                           @RequestParam(value = "table",defaultValue = "")String table,
                           @RequestParam("id")String id);

    /**
     * 根据sql与查询
     * @param database 数据库
     * @param table 表
     * @param sql sql
     * @param id 数据源id
     * @return
     */
    @GetMapping("datasource/bi/findData")
    public Map findData(@RequestParam("database")String database,
                        @RequestParam("table")String table,
                        @RequestParam("sql")String sql,
                        @RequestParam("id")String id);

}
