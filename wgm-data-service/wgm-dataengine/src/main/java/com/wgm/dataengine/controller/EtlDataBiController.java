package com.wgm.dataengine.controller;

import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonResponse;
import com.wgm.dataengine.service.EtlDataEngineService;
import com.wgm.aop.anno.LogAnno;
import com.wgm.dataengine.vo.EtlDataBiVo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * @author pangpang
 * @email 1963148908@qq.com
 * @date 2021/1/13 9:48
 */
@RestController
@RequestMapping("dataengine/bi")
@Slf4j
public class EtlDataBiController {

    @Autowired
    private EtlDataEngineService etlDataEngineService;

    /**
     * 查询所有数据库
     * @param database
     * @param id
     * @return
     */
    @GetMapping("showDatabase")
    @LogAnno(operationName = "查询所有数据库",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse showDatabase(@RequestParam(value = "database",defaultValue = "")String database,
                                       @RequestParam("id")String id){
        List list = etlDataEngineService.showDatabase(database, id);
        return CommonResponse.ok(list);
    }

    /**
     * 查询数据库对应所有表
     * @param etlDataBiVo
     * @param table
     * @return
     */
    @PostMapping("showTables")
    @LogAnno(operationName = "查询数据库对应所有表",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse showTables(@RequestBody EtlDataBiVo etlDataBiVo,
                                     @RequestParam(value = "table",defaultValue = "")String table
                                     ){
        List list = etlDataEngineService.showTables(etlDataBiVo, table);
        return CommonResponse.ok(list);
    }

    /**
     * 根据sql与查询
     * @param etlDataBiVo :数据库,表,sql语句
     * @return
     */
    @PostMapping("findData")
    @LogAnno(operationName = "根据sql与查询",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse findData(@RequestBody EtlDataBiVo etlDataBiVo){
        Map data = etlDataEngineService.findData(etlDataBiVo);
        return CommonResponse.ok(data);
    }

}
