package com.wgm.dataengine;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/6
 */
@SpringBootApplication(scanBasePackages = {"com.wgm.dataengine","com.wgm.aop"})
@EnableDiscoveryClient
@MapperScan("com.wgm.dataengine.dao")
@EnableFeignClients(basePackages = "com.wgm.dataengine.config.feign")
@EnableTransactionManagement
public class WgmDataEngineApplication {
    public static void main(String[] args) {
        SpringApplication.run(WgmDataEngineApplication.class,args);
    }
}
