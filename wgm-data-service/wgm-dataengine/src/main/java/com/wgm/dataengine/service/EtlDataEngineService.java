package com.wgm.dataengine.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.utils.PageUtils;
import com.wgm.dataengine.entity.EtlDataEngineEntity;
import com.wgm.dataengine.vo.EtlDataBiVo;

import com.wgm.dataengine.vo.EtlDataEngineVo;
import com.wgm.dataengine.vo.EtlDataEngineWriteVo;


import java.util.List;
import java.util.Map;

/**
 * 数据规则引擎表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-12 14:50:19
 */
public interface EtlDataEngineService extends IService<EtlDataEngineEntity> {
    /**
     * 添加
     * @param etlDataEngineEntity
     * @return
     */
    CommonResponse add(EtlDataEngineEntity etlDataEngineEntity);

    /**
     * 查找对象
     * @param id
     * @return
     */
    EtlDataEngineEntity findById(String id);
    /**
     * 数据引擎列表+分页+模糊
     * @param etlDataEngineVo 索引对象
     * @param pageSize 每页条数
     * @param currPage 当前页
     * @return
     */
    PageUtils selectDataEnginelist(EtlDataEngineVo etlDataEngineVo, Integer pageSize, Integer currPage);

    /**
     * 查看该mysql中的 所有 数据库
     * @param database
     * @param id
     * @return
     */
    List showDatabase(String database,String id);

    /**
     * 查看该数据库中 所有的 表
     * @param etlDataBiVo
     * @param table
     * @return
     */
    List showTables(EtlDataBiVo etlDataBiVo, String table);

    /**
     * 查看数据
     * @param etlDataBiVo
     * @return
     */
    Map findData(EtlDataBiVo etlDataBiVo);

    /**
     * 删除
     * @param id
     */
    void del(String id);

    /**
     * 编写 数据引擎
     * @param etlDataEngineWriteVo
     */
    void write(EtlDataEngineWriteVo etlDataEngineWriteVo);

    /**
     * 修改规则引擎
     * @param dataEngineVo 请求体
     */
    void updateEtlDataEngine(EtlDataEngineVo dataEngineVo);
}

