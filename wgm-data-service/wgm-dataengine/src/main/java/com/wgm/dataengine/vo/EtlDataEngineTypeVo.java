package com.wgm.dataengine.vo;

import lombok.Data;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/12
 */
@Data
public class EtlDataEngineTypeVo {
    private String id;
    /**
     * 规则类型
     */
    private String engineType;
}
