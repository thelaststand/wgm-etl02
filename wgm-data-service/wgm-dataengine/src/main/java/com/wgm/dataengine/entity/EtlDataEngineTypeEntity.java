package com.wgm.dataengine.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 规则引擎类型表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-12 14:50:19
 */
@Data
@TableName("etl_data_engine_type")
public class EtlDataEngineTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 规则类型
	 */
	private String engineType;
	/**
	 * 
	 */
	private Integer tombstone;

}
