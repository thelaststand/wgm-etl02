package com.wgm.dataengine.vo;

import lombok.Data;

/**
 * @author pangpang
 * @email 1963148908@qq.com
 * @date 2021/1/13 14:33
 */
@Data
public class EtlDataBiVo {

    /**
     * 数据库
     */
    String database;
    /**
     * 表
     */
    String table;
    /**
     * sql语句
     */
    String sql;
    /**
     * 数据源id
     */
    String id;

}
