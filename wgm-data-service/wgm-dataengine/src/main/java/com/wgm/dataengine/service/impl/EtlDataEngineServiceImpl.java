package com.wgm.dataengine.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.common.constant.CommonStatus;
import com.wgm.common.exception.EngineException;
import com.wgm.dataengine.config.feign.FeignDataEngine;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.utils.DateUtils;
import com.wgm.dataengine.dao.EtlDataEngineDao;
import com.wgm.dataengine.entity.EtlDataEngineEntity;
import com.wgm.dataengine.pojo.CreadWriteFile;
import com.wgm.dataengine.entity.EtlDataEngineTypeEntity;
import com.wgm.dataengine.service.EtlDataEngineService;
import com.wgm.dataengine.vo.EtlDataBiVo;
import com.wgm.dataengine.vo.EtlDataEngineWriteVo;

import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import com.wgm.dataengine.service.EtlDataEngineTypeService;
import com.wgm.dataengine.vo.EtlDataEngineVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;


import java.io.IOException;
import java.util.Date;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import com.wgm.common.utils.PageUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * 数据规则引擎实现类
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-12 14:50:19
 */
@Service("etlDataEngineService")
@Transactional(rollbackFor = Exception.class)
public class EtlDataEngineServiceImpl extends ServiceImpl<EtlDataEngineDao, EtlDataEngineEntity> implements EtlDataEngineService {

    @Autowired
    EtlDataEngineDao etlDataEngineDao;

    private static final String ISMYSQL="mysql";
    @Autowired
    private EtlDataEngineTypeService typeService;

    @Autowired
    FeignDataEngine feignDataSource;
    /** 创建一个 公平锁 */
    private ReentrantLock lock = new ReentrantLock(true);

    @Override
    public PageUtils selectDataEnginelist(EtlDataEngineVo etlDataEngineVo, Integer pageSize, Integer currPage) {

       List<EtlDataEngineEntity> entityList = this.baseMapper.selectDataEnginelist(etlDataEngineVo.getClName(),
                etlDataEngineVo.getCodeType(),
                etlDataEngineVo.getEngineName(),
                pageSize,
                (currPage-1)*pageSize);

        List<EtlDataEngineVo> collect = entityList.stream().map(entity -> {
            EtlDataEngineVo dataEngineVo = new EtlDataEngineVo();
            BeanUtils.copyProperties(entity, dataEngineVo);
            EtlDataEngineTypeEntity typeEntity = typeService.findById(entity.getEngineTypeId());
            dataEngineVo.setEngineType(typeEntity.getEngineType());
            return dataEngineVo;
        }).collect(Collectors.toList());

        Integer totalCount = this.baseMapper.getCount(etlDataEngineVo.getClName(),
                etlDataEngineVo.getCodeType(),
                etlDataEngineVo.getEngineName());
        return new PageUtils(collect,totalCount,pageSize,currPage);
    }

    @Override
    public void updateEtlDataEngine(EtlDataEngineVo dataEngineVo) {
        //获取修改前对象
        EtlDataEngineEntity engineEntity = this.findById(dataEngineVo.getId());

        //判断是否存在
        if (engineEntity==null){
            throw new EngineException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }

        //比较类/函数名称是否修改
        if (!engineEntity.getClName().equals(dataEngineVo.getClName())){
            throw new EngineException(CommonConstants.ErrorMsg.CLASS_FUNCTION_NAME_NOT_EDITABLE);
        }
        //规则引擎名称唯一
        Integer count = this.baseMapper.findByNameCount(dataEngineVo.getEngineName());
        if (count>0){
            throw new EngineException(CommonConstants.ErrorMsg.RULE_ENGINE_NAME_REPEAT);
        }
        BeanUtils.copyProperties(dataEngineVo,engineEntity);
        engineEntity.setUpdateTime(DateUtils.getNowDate());

        if (!this.baseMapper.updateEtlDataEngine(engineEntity)){
            throw new EngineException(CommonStatus.INVALID.getDesc());
        }


    }

    @Override
    public List showDatabase(String database,String id) {
        return feignDataSource.showDatabase(database,id);
    }

    @Override
    public List showTables(EtlDataBiVo etlDataBiVo, String table) {
        return feignDataSource.showTables(etlDataBiVo.getDatabase(),table,etlDataBiVo.getId());
    }

    @Override
    public Map findData(EtlDataBiVo etlDataBiVo) {
        return feignDataSource.findData(etlDataBiVo.getDatabase(),etlDataBiVo.getTable(),
                                        etlDataBiVo.getSql(),etlDataBiVo.getId());
    }

    /**
     * 添加 规则引擎
     * @param etlDataEngineEntity
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommonResponse add(EtlDataEngineEntity etlDataEngineEntity) {

        if (etlDataEngineEntity==null){
            throw new EngineException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //生成一个 随机的 名称
        String replace = "Etl"+UUID.randomUUID().toString().replace("-", "");
        //添加时注意数据规则引擎名称唯一
        EtlDataEngineEntity engine=etlDataEngineDao.findByClName(etlDataEngineEntity.getEngineName());
        if (engine!=null){
            //该名称引擎 已存在
            return new CommonResponse(CommonStatus.FILE_NOT_ENGINENAME_EXISTES);
        }
        etlDataEngineEntity.setCreateTime(new Date());
        etlDataEngineEntity.setUpdateTime(new Date());
        etlDataEngineEntity.setTombstone(0);
        //判断 类型
        if (ISMYSQL.equals(etlDataEngineEntity.getCodeType())){
        }else {
            //java类型   添加数据 并且 创建一个java文件
            try {
                String creatRun = CreadWriteFile.createRun(replace);
                if (creatRun==null){
                    //类/函数名称 已存在
                    return new CommonResponse(CommonStatus.FILE_NOT_EXISTES);
                }
                etlDataEngineEntity.setClName(replace);
                etlDataEngineEntity.setEngineCode(creatRun);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //添加到 数据库
        etlDataEngineDao.insert(etlDataEngineEntity);
        return new CommonResponse(CommonStatus.VALID);
    }

    /**
     * 查找 规则 引擎
     * @param id
     * @return
     */
    @Override
    public EtlDataEngineEntity findById(String id) {
        return etlDataEngineDao.selectById(id);
    }

    /**
     * 删除
     * @param id
     */
    @Override
    @Transactional(rollbackFor = EngineException.class)
    public void del(String id) {
        if (StringUtils.isEmpty(id)){
            throw new  EngineException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //先查找 对象
        EtlDataEngineEntity etlDataEngineEntity = etlDataEngineDao.selectById(id);
        if (etlDataEngineEntity==null){
            throw new  EngineException(CommonConstants.ErrorMsg.TARGET_DOESNOTEXIST);
        }

        //修改 状态 进行逻辑删除
        etlDataEngineEntity.setTombstone(1);
        //删除java 文件
        CreadWriteFile.delFile(etlDataEngineEntity.getClName());
        etlDataEngineEntity.setUpdateTime(new Date());
        etlDataEngineDao.updateById(etlDataEngineEntity);
    }

    /**
     * 编写 内容
     * @param etlDataEngineWriteVo
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void write(EtlDataEngineWriteVo etlDataEngineWriteVo) {

        if (etlDataEngineWriteVo==null){
            throw new EngineException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        EtlDataEngineEntity dataEngineEntity = etlDataEngineDao.selectById(etlDataEngineWriteVo.getId());
        dataEngineEntity.setUpdateTime(new Date());
        //进行 编写
        try {
            String writeFile = CreadWriteFile.writeFile(dataEngineEntity.getClName(), etlDataEngineWriteVo.getMessage());
            dataEngineEntity.setEngineCode(writeFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        lock.lock();
        try {
            etlDataEngineDao.updateById(dataEngineEntity);
        }finally {
            lock.unlock();
        }
    }

}