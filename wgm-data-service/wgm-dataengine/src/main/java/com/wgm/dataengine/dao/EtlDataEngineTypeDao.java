package com.wgm.dataengine.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.dataengine.entity.EtlDataEngineTypeEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 规则引擎类型表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-12 14:50:19
 */
@Mapper
public interface EtlDataEngineTypeDao extends BaseMapper<EtlDataEngineTypeEntity> {

    /**
     * 查询全部数据规则类型
     * @return
     */
    List<EtlDataEngineTypeEntity> selectAll();

    /**
     * 根据id查询
     * @param engineTypeId
     * @return
     */
    EtlDataEngineTypeEntity findById(@Param("engineTypeId") String engineTypeId);
}
