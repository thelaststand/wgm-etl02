package com.wgm.dataengine.pojo;
import org.apache.commons.lang3.StringUtils;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.*;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;

/**

*   class
*   @Author 小脸蛋
*   @CreateDate 2021/1/12
*/
public class CreadWriteFile {

    private static String url = Thread.currentThread().getContextClassLoader().getResource("com/wgm/dataengine/pojo/generatejava").toString();
    private static String filenameTemp;
    private static String pathUrl= url.replace("file:/", "");
    //写入内容：
    /**
     * 向文件中写入内容
     * @param fileName 文件路径与名称
     * @param newstr  写入的内容
     * @return
     * @throws IOException
     */
    public static String writeFile(String fileName,String newstr) throws IOException{
        if (fileName==null){
            return null;
        }
        filenameTemp = pathUrl+"/"+fileName+".java";
        //新写入的行，换行
        String filein="";
        if (StringUtils.isNotEmpty(newstr)){
            filein =newstr;
        }
        String temp  = "";
        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        FileOutputStream fos  = null;
        PrintWriter pw = null;
        try {
            //文件路径(包括文件名称)
            File file = new File(filenameTemp);
            //将文件读入输入流
            fis = new FileInputStream(file);
            isr = new InputStreamReader(fis);
            br = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();
            buffer.append(filein);
            fos = new FileOutputStream(file);
            pw = new PrintWriter(fos);
            pw.write(buffer.toString().toCharArray());
            pw.flush();
            //编译 运行
            run(fileName);
            System.out.println("Insert data successfully.");
        } catch (Exception e) {
            // : handle exception
            e.printStackTrace();
        }finally {
            //不要忘记关闭
            if (pw != null) {
                pw.close();
            }
            if (fos != null) {
                fos.close();
            }
            if (br != null) {
                br.close();
            }
            if (isr != null) {
                isr.close();
            }
            if (fis != null) {
                fis.close();
            }
        }
        return filein;
    }

    /**
     * 删除文件
     * @param fileName  文件名称
     * @return
     */
    public static boolean delFile(String fileName){
        boolean bool = false;
        filenameTemp = pathUrl+"/"+fileName+".java";
        String clazz=pathUrl+"/"+fileName+".class";
        File file  = new File(filenameTemp);
        File file1 = new File(clazz);
        try {
            if(file.exists() && file1.exists()){
                file.delete();
                file1.delete();
                bool = true;
                System.out.println("success delete file,the file is " + filenameTemp);
            }
        } catch (Exception e) {
            // : handle exception
        }
        return bool;
    }

    /**
     *  创建  编译  运行
     * @param fileName
     */
    public static String createRun(String fileName){
        String code = "import java.util.Map;\n" +
                "\n" +
                "public class "+fileName+"{\n" +
                "\n" +
                "  public void ruleEngine(Map<String,Object> map){\n" +
                "      map.forEach((k,v)->{\n" +
                "          System.out.println(\"k:\"+k+\"\\n v:\"+v);\n" +
                "      });\n" +
                "  }\n" +
                "}";
        OutputStream os = null ;
        try {
            File file = new File(pathUrl+"/"+fileName+".java");
            if (!file.getParentFile().exists()) {
                file.getParentFile();
            }
            if (file.exists()){
                return null;
            }
            file.createNewFile();
            os = new FileOutputStream(file);
            os.write(code.getBytes(), 0, code.length());
            //编译 运行
            run(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if (os!=null){
                    os.flush();
                    os.close();
                };
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return code;
    }
    /**
     * 编译 运行
     */
    public static void run(String fileName){
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        //直接从文件中获取
        //将Java代码以字符串的形式，先写入文件，然后再通过编译器读取出来，达到动态的效果。
        int result = compiler.run(null, null, null, pathUrl+"/"+fileName+".java");
        System.out.println(result == 0 ? "ok" : "error");
        //运行
        Class clazz = null;
        try {
            URL[] urls = new URL[]{new URL(url+"/")};
            URLClassLoader loader = new URLClassLoader(urls);
            clazz = loader.loadClass(fileName);
            Method m = clazz.getDeclaredMethod("ruleEngine", Map.class);
            HashMap<String, Object> map = new HashMap<>(16);
            map.put("a",1);
            map.put("b",2);
            //通过Object把数组转化为参数
            m.invoke(clazz.newInstance(),map);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static void main(String[] args) throws Exception{
    }
}
