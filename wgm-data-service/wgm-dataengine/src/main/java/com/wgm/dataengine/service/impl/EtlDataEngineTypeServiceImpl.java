package com.wgm.dataengine.service.impl;

import com.wgm.dataengine.dao.EtlDataEngineTypeDao;
import com.wgm.dataengine.entity.EtlDataEngineTypeEntity;
import com.wgm.dataengine.service.EtlDataEngineTypeService;
import com.wgm.dataengine.vo.EtlDataEngineTypeVo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;


/**
 * 数据规则引擎类型实现类
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-12 14:50:19
 */
@Service("etlDataEngineTypeService")
@Transactional(rollbackFor = Exception.class)
public class EtlDataEngineTypeServiceImpl extends ServiceImpl<EtlDataEngineTypeDao, EtlDataEngineTypeEntity> implements EtlDataEngineTypeService {


    @Override
    public List<EtlDataEngineTypeVo> selectAll() {
        List<EtlDataEngineTypeEntity> typeEntities = this.baseMapper.selectAll();
        List<EtlDataEngineTypeVo> collect = typeEntities.stream().map((typeEntity) -> {
            EtlDataEngineTypeVo typeVo = new EtlDataEngineTypeVo();
            typeVo.setId(typeEntity.getId());
            typeVo.setEngineType(typeEntity.getEngineType());
            return typeVo;
        }).collect(Collectors.toList());
        return collect;
    }

    @Override
    public EtlDataEngineTypeEntity findById(String engineTypeId) {
        return this.baseMapper.findById(engineTypeId);
    }

}