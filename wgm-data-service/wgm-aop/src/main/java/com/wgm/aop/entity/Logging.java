package com.wgm.aop.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/14
 */
@Data
@TableName("etl_log")
public class Logging implements Serializable {

    @TableId
    private String id;

    /**
     * 操作name
     */
    private String operationName;

    /**
     * 执行方法名
     */
    private String methodName;

    /**
     * 类名
     */
    private String className;

    /**
     * 操作类型
     */
    private String operationType;

    /**
     * 异常信息
     */
    private String exceptionDetail;

    /**
     * 参数
     */
    private String params;

    /**
     * 操作人姓名
     */
    private String adminName;

    /**
     * 操作日期
     */
    private Date createDate;

    /**
     * 执行时间
     */
    private float executionTime;

}
