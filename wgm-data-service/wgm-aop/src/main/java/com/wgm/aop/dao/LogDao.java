package com.wgm.aop.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.aop.entity.Logging;

/**
 * @author kangbaby
 */
public interface LogDao extends BaseMapper<Logging> {

}
