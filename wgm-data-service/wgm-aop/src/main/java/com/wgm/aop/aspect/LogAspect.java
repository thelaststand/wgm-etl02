package com.wgm.aop.aspect;

import com.alibaba.nacos.common.util.UuidUtils;
import com.wgm.aop.dao.LogDao;
import com.wgm.common.constant.CommonResponse;
import com.wgm.aop.entity.Logging;
import com.wgm.aop.anno.LogAnno;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;


/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/14
 */
@Component
@Aspect
@MapperScan("com.wgm.aop.dao")
@Transactional(rollbackFor = Exception.class)
public class LogAspect {
   //    注入日志操作service

    @Autowired
    private LogDao logDao;

    private static final Logger logger = (Logger) LoggerFactory.getLogger(LogAspect.class);

    // 配置织入点

    @Pointcut("within(com.wgm.*.controller.*) && @annotation(com.wgm.aop.anno.LogAnno)")
    public void controllerAspect() {
    }



    /**
     * 配置controller环绕通知,使用在方法aspect()上注册的切入点
     * 环绕通知：环绕目标方式执行
     * @param pjp
     * @return
     * @throws Throwable
     */
    @Around("controllerAspect()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {

        //开始时间
        long startTime = System.currentTimeMillis();
        Logging log = new Logging();
        Object result = null;

        //获取操作用户
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String userName = (String) request.getSession().getAttribute("username");
        System.out.println("获取session内容" + request.getSession().getAttribute("username"));

        if (userName == null) {
            log.setAdminName("非登录用户");
        } else {
            //操作用户
            log.setAdminName(userName);
        }

        // 执行目标方法
        try {
            // 前置通知
            String methodName1 = pjp.getSignature().getName();
            System.out.println("前置通知=>" + "方法名：" + methodName1 + "");
            Object[] params = pjp.getArgs();

            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < params.length - 1; i++) {
                stringBuilder.append(params[i] + ",");
                System.out.println("参数为：" + params[i]);
            }
            System.out.println("stringBuilder" + stringBuilder);
            MethodSignature methodName = (MethodSignature) pjp.getSignature();
            Method method = methodName.getMethod();
            String name = method.getAnnotation(LogAnno.class).operationName();
            String type = method.getAnnotation(LogAnno.class).operationType();
            System.out.println(name + "========================");
            System.out.println(type + "========================");
            String className = pjp.getTarget().getClass().getName();
            //去掉横线 32位
            log.setId(UuidUtils.generateUuid());
            log.setMethodName(methodName1);
            log.setClassName(className);
            log.setParams(stringBuilder.toString());
            log.setOperationName(name);
            log.setOperationType(type);
            result = pjp.proceed();
            // 返回通知

            log.setExceptionDetail("无异常");
        } catch (Throwable e) {
            System.out.println("异常通知" + e);
            log.setExceptionDetail(e.toString());
            if (!StringUtils.isEmpty(e.getMessage())){
                result= CommonResponse.error(e.getMessage());
            }
            result = CommonResponse.error(e.toString());
        }
        //后置通知
        System.out.println("后置通知");
        //结束时间
        long endTime = System.currentTimeMillis();
        float excTime = (float) (endTime - startTime) / 1000;
        System.out.println("执行时间:" + excTime + "s");
        System.out.println(log);
        log.setCreateDate(new Date());
        logDao.insert(log);

        return result;
    }

}
  
