package com.wgm.aop.anno;
import java.lang.annotation.*;

/**
 * Demo class
 *自定义注解 日志记录
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/14
 */
/**
 * @author kangbaby
 */
@Target({ElementType.METHOD,ElementType.PACKAGE})
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface LogAnno {
    //    要执行的操作类型 add
    String operationType() default "";

    //    要执行的具体操作比如：添加用户
    String operationName() default "";

}
