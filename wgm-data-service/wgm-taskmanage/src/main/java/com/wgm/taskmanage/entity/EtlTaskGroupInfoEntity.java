package com.wgm.taskmanage.entity;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import lombok.Data;

/**
 * 任务处理分组表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Data
@TableName("etl_task_group_info")
public class EtlTaskGroupInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 节点ID
	 */
	private String nodeId;
	/**
	 * 节点名称
	 */
	private String nodeName;
	/**
	 * 任务ID
	 */
	private String taskId;
	/**
	 * 选择节点ID
	 */
	private String selectNodeId;
	/**
	 * 选择表名称
	 */
	private String selectTableName;
	/**
	 * 选择表字段
	 */
	private String selectTableField;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新人
	 */
	private String updateBy;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * tombstone 逻辑删除
	 */
	@TableLogic(value = "0",delval = "1")
	private Integer tombstone;
	public EtlTaskGroupInfoEntity(JSONObject node, Date now) {
		this.id= UUID.randomUUID().toString();
		this.nodeId=node.getString("id");
		JSONObject nodeData = node.getJSONObject("data");
		this.taskId=nodeData.getString("taskId");
		this.selectNodeId=nodeData.getString("selectNodeId");
		this.selectTableName=nodeData.getString("selectTableName");
		this.selectTableField=nodeData.getString("selectTableField");
		this.createTime=this.updateTime=now;
	}
}
