package com.wgm.taskmanage.dao;

import com.wgm.taskmanage.entity.EtlTaskInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.taskmanage.vo.TaskManageInfoVo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.apache.ibatis.annotations.Param;


import java.util.List;
/**
 * 任务信息表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Mapper
@Component
public interface EtlTaskInfoDao extends BaseMapper<EtlTaskInfoEntity> {
    /**
     * 根据id删除任务信息
     * @param id 任务信息id
     */
    void deleteInfoById(@Param("id") String id);

    /**
     *根据id查询任务信息
     * @param id 任务信息id
     * @return
     */
    EtlTaskInfoEntity selectEtlInfoById(@Param("id") String id);


    /**
     * 任务列表 分页+模糊
     * @param name 任务名称
     * @param processStatus 流程状态
     * @param weight 权重
     * @param pageSize 每页条数
     * @param currPage 当前页
     * @return
     */
    List<TaskManageInfoVo> getTaskManageList(@Param("name") String name,
                                             @Param("processStatus") Integer processStatus,
                                             @Param("weight") Integer weight,
                                             @Param("pageSize") Integer pageSize,
                                             @Param("currPage") Integer currPage);

    /**
     * 连接池中的个数
     * @return
     */
    int getcount();
}
