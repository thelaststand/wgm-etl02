package com.wgm.taskmanage.service.impl;

import com.alibaba.fastjson.JSON;

import com.wgm.common.exception.AdException;
import com.wgm.taskmanage.config.feign.FeignTaskManage;
import com.wgm.common.constant.CommonConstants;
import com.wgm.datasource.base.MysqlDataSource;
import com.wgm.datasource.entity.EtlDatasourceInfo;
import com.wgm.datasource.pojo.DataSource;
import com.wgm.taskmanage.pojo.FieldRequest;
import com.wgm.taskmanage.vo.DataSourceVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.wgm.taskmanage.dao.EtlTaskInputInfoDao;
import com.wgm.taskmanage.entity.EtlTaskInputInfoEntity;
import com.wgm.taskmanage.service.EtlTaskInputInfoService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;


/**
 * @author kangbaby
 */
@Service("etlTaskInputInfoService")
@Transactional(rollbackFor = Exception.class)
public class EtlTaskInputInfoServiceImpl extends ServiceImpl<EtlTaskInputInfoDao, EtlTaskInputInfoEntity> implements EtlTaskInputInfoService {

    @Autowired
    private EtlTaskInputInfoDao etlTaskInputInfoDao;

    @Autowired
    FeignTaskManage feignTaskManage;


    @Override
    public List<EtlTaskInputInfoEntity> findTaskInputInfoList(EtlTaskInputInfoEntity etlTaskInputInfo) {
        return etlTaskInputInfoDao.findTaskInputInfoList(etlTaskInputInfo);
    }

    @Override
    public List<Map<String, Object>> dataTableList(String taskId) {
        //拷贝到数据源对象
        DataSource dataSource = new DataSource();

        Object data = feignTaskManage.findById(taskId).getData();

        Object json = JSON.toJSON(data);

        EtlDatasourceInfo datasourceInfo = JSON.parseObject(json.toString(), EtlDatasourceInfo.class);

        BeanUtils.copyProperties(datasourceInfo, dataSource);

        BeanUtils.copyProperties(datasourceInfo, dataSource);
        if(StringUtils.isEmpty(taskId)){
            throw new AdException(CommonConstants.DataError.DATA_ID_NULL);
        }

//        获取连接
        Connection connection = MysqlDataSource.getConnection(dataSource);

//        查询数据源的所有表的SQL语句
        String sql = "show tables";
//        查询
        List<Map<String, Object>> maps = MysqlDataSource.executeQuerySql(connection, sql);

//        关闭连接
        MysqlDataSource.closeConn(connection);

        return maps;
    }

    @Override
    public List<Map<String, Object>> dataFieldList(FieldRequest request) {
        //拷贝到数据源对象
        DataSource dataSource = new DataSource();
        Object obj = feignTaskManage.findById(request.getId()).getData();
        EtlDatasourceInfo data = JSON.parseObject(JSON.toJSON(obj).toString(), EtlDatasourceInfo.class);
        BeanUtils.copyProperties(data, dataSource);

        if(StringUtils.isEmpty(request.getId())){
            throw new AdException(CommonConstants.DataError.DATA_ID_NULL);
        }
        if(StringUtils.isEmpty(request.getDbName()) || !data.getDbName().equals(request.getDbName())){
            throw new AdException(CommonConstants.DataError.DATABASE_NULL);
        }
        if(StringUtils.isEmpty(request.getTableName())){
            throw new AdException(CommonConstants.DataError.TABLE_NAME_NULL);
        }

//        获取连接
        Connection connection = MysqlDataSource.getConnection(dataSource);

//        查询表的所有字段的SQL语句
        String sql = "SELECT COLUMN_NAME fieldName,DATA_TYPE dataType " +
                          " FROM information_schema.columns WHERE table_name = '"+
                              request.getTableName() +"' AND table_schema = '"+request.getDbName()+"'";
//        查询
        List<Map<String, Object>> maps = MysqlDataSource.executeQuerySql(connection, sql);

//        关闭连接
        MysqlDataSource.closeConn(connection);

        return maps;
    }

    @Override
    public List<DataSourceVo> dataList() {

        List<DataSourceVo> list = new ArrayList<>();

        Connection conn;
        Statement stat;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            // 2. 获得连接
            String url = "jdbc:mysql://localhost:3306/wgm_etl?useUnicode=true&serverTimezone=Asia/Shanghai";
            conn = DriverManager.getConnection(url, "root", "root");
            System.out.println(conn);

            stat = conn.createStatement();
            ResultSet resultSet = stat.executeQuery("SHOW DATABASES");
            while (resultSet.next()) {
                String dbName = resultSet.getString("Database");
                DataSourceVo dataSourceVo = new DataSourceVo();
                dataSourceVo.setDbName(dbName);
                list.add(dataSourceVo);
                System.out.println(dbName);
            }
            conn.close();
            stat.close();
            resultSet.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return list;
    }



}