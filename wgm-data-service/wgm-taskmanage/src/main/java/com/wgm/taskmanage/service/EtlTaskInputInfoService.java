package com.wgm.taskmanage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.taskmanage.entity.EtlTaskInputInfoEntity;
import com.wgm.taskmanage.pojo.FieldRequest;
import com.wgm.taskmanage.vo.DataSourceVo;

import java.util.List;
import java.util.Map;

/**
 * 任务数据输入表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
public interface EtlTaskInputInfoService extends IService<EtlTaskInputInfoEntity> {

    /**
     * 查询数据输入表信息
     * @param etlTaskInputInfo
     * @return
     */
    List<EtlTaskInputInfoEntity> findTaskInputInfoList(EtlTaskInputInfoEntity etlTaskInputInfo);

    /**
     * 查询数据库的所有表名称
     * @param taskId 数据源的id
     * @return
     */
    List<Map<String, Object>> dataTableList(String taskId);

    /**
     * 通过数据库名、表名查询所有的字段名
     * @param request
     * @return
     */
    List<Map<String, Object>> dataFieldList(FieldRequest request);

    /**
     * 查询所有的数据源
     * @return
     */
    List<DataSourceVo> dataList();
}

