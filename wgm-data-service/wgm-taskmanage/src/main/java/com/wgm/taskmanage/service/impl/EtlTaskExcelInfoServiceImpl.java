package com.wgm.taskmanage.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.wgm.taskmanage.dao.EtlTaskExcelInfoDao;
import com.wgm.taskmanage.entity.EtlTaskExcelInfoEntity;
import com.wgm.taskmanage.service.EtlTaskExcelInfoService;
import org.springframework.transaction.annotation.Transactional;


/**
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/8
 */
@Service("etlTaskExcelInfoService")
@Transactional(rollbackFor = Exception.class)
public class EtlTaskExcelInfoServiceImpl extends ServiceImpl<EtlTaskExcelInfoDao, EtlTaskExcelInfoEntity> implements EtlTaskExcelInfoService {


}