package com.wgm.taskmanage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wgm.taskmanage.service.EtlTaskLogicAnnalInfoService;


/**
 * 任务处理过程表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@RestController
@RequestMapping("taskmanage/etltasklogicannalinfo")
public class EtlTaskLogicAnnalInfoController {
    @Autowired
    private EtlTaskLogicAnnalInfoService etlTaskLogicAnnalInfoService;

}
