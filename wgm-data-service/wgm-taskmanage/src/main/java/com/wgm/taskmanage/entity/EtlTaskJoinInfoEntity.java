package com.wgm.taskmanage.entity;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import lombok.Data;

/**
 * 数据关联表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Data
@TableName("etl_task_join_info")
public class EtlTaskJoinInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 节点ID
	 */
	private String nodeId;
	/**
	 * 节点名称
	 */
	private String nodeName;
	/**
	 * 任务ID
	 */
	private String taskId;
	/**
	 * 左节点ID
	 */
	private String leftNodeId;
	/**
	 * 左表ID
	 */
	private String leftId;
	/**
	 * 关联方式
	 */
	private String joinType;
	/**
	 * 右表节点ID
	 */
	private String rightNodeId;
	/**
	 * 右表ID
	 */
	private String rightId;
	/**
	 * 左表字段
	 */
	private String leftJoinField;
	/**
	 * 右表字段
	 */
	private String rightJoinField;
	/**
	 * 乐观锁
	 */
	private Integer revision;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新人
	 */
	private String updateBy;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * tombstone 逻辑删除
	 */
	@TableLogic(value = "0",delval = "1")
	private Integer tombstone;
	public EtlTaskJoinInfoEntity(JSONObject node, Date now, String leftId, String rightId) {
		this.id= UUID.randomUUID().toString();
		this.nodeId=node.getString("id") ;
		JSONObject nodeData = node.getJSONObject("data");
		JSONArray fromNodeData = nodeData.getJSONArray("fromNodeData");
		JSONObject left = fromNodeData.getJSONObject(0);
		JSONObject right = fromNodeData.getJSONObject(1);
		this.taskId=nodeData.getString("taskId");
		this.leftId=leftId;
		this.leftNodeId=left.getString("id");
		this.rightId=rightId;
		this.rightNodeId=right.getString("id");
		this.joinType=nodeData.getString("joinType");
		this.leftJoinField=nodeData.getString("leftJoinField");
		this.rightJoinField=nodeData.getString("rightJoinField");
		this.createTime=this.updateTime=now;
	}
}
