package com.wgm.taskmanage.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author pangpang
 * @email 1963148908@qq.com
 * @date 2021/1/8 19:59
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskManageInfoVo {

    /**
     * id
     */
    private String id;
    /**
     * 任务名称
     */
    private String name;
    /**
     * 任务权重级别 0紧急任务1高2中3低
     */
    private Integer weight;
    /**
     * 任务执行状态 -1待执行 0可执行 1正在执行 2执行完成
     */
    private Integer processStatus;
    /**
     * 任务完成度
     */
    private BigDecimal proportion;
    /**
     * 总处理条数
     */
    private Integer total;
    /**
     * 已处理条数
     */
    private Integer processTotal;
    /**
     * 错误条数
	 */
    private Integer errorTotal;



}
