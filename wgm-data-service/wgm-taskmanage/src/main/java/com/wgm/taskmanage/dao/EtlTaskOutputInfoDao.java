package com.wgm.taskmanage.dao;

import com.wgm.taskmanage.entity.EtlTaskOutputInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 任务结果输出表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:53
 */
@Mapper
@Component
public interface EtlTaskOutputInfoDao extends BaseMapper<EtlTaskOutputInfoEntity> {
    /**
     * 根据id删除任务处理排序
     * @param id
     */
    void deleteEtlOutputInfoById(@Param("id") String id);


    /**
     * 根据id查询这张表的对象
     * @param taskId
     * @return
     */
    /**
     * 根据id查询任务处理排序
     * @param taskId
     * @return
     */
    List<EtlTaskOutputInfoEntity> findTaskId(String taskId);
}
