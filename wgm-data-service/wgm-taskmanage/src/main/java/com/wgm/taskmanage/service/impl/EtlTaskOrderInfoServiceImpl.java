package com.wgm.taskmanage.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.wgm.taskmanage.dao.EtlTaskOrderInfoDao;
import com.wgm.taskmanage.entity.EtlTaskOrderInfoEntity;
import com.wgm.taskmanage.service.EtlTaskOrderInfoService;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author kangbaby
 */
@Service("etlTaskOrderInfoService")
@Transactional(rollbackFor = Exception.class)
public class EtlTaskOrderInfoServiceImpl extends ServiceImpl<EtlTaskOrderInfoDao, EtlTaskOrderInfoEntity> implements EtlTaskOrderInfoService {


}