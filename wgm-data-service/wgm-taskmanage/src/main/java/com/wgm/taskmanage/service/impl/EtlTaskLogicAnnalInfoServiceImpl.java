package com.wgm.taskmanage.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wgm.common.utils.DateUtils;
import com.wgm.taskmanage.dao.*;
import com.wgm.taskmanage.entity.*;
import com.wgm.taskmanage.service.EtlTaskLineInfoService;
import com.wgm.taskmanage.service.EtlTaskLogicAnnalInfoService;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 任务处理过程service实现类
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Service("etlTaskLogicAnnalInfoService")
@Transactional(rollbackFor = Exception.class)
public class EtlTaskLogicAnnalInfoServiceImpl extends ServiceImpl<EtlTaskLogicAnnalInfoDao, EtlTaskLogicAnnalInfoEntity> implements EtlTaskLogicAnnalInfoService {
    @Resource
    private EtlTaskInfoDao taskInfoService;
    @Resource
    private EtlTaskLineInfoService taskLineInfoService;
    @Resource
    private EtlTaskInputInfoDao taskInputInfoService;
    @Resource
    private EtlTaskJoinInfoDao taskJoinInfoService;
    @Resource
    private EtlTaskGroupInfoDao taskGroupInfoService;
    @Resource
    private EtlTaskOrderInfoDao taskOrderInfoService;
    @Resource
    private EtlTaskLogicAnnalInfoDao taskLogicInfoService;
    @Resource
    private EtlTaskOutputInfoDao taskOutputInfoService;

    private String taskName;

    private String taskId;

    private EtlTaskInfoEntity etlTaskInfo;

    private static List<EtlTaskInputInfoEntity> taskInputInfos =new ArrayList<>();

    private static List<EtlTaskJoinInfoEntity> taskJoinInfos =new ArrayList<>();

    private static List<EtlTaskGroupInfoEntity> taskGroupInfos =new ArrayList<>();

    private static List<EtlTaskOrderInfoEntity> taskOrderInfos =new ArrayList<>();

    private static List<EtlTaskLineInfo> taskLineInfos =new ArrayList<>();

    private static List<EtlTaskLogicAnnalInfoEntity> taskLogicInfos=new ArrayList<>();

    private static List<EtlTaskOutputInfoEntity> taskOutputInfos=new ArrayList<>();

    @Override
    public  void parseJson(String taskProcessJson) {
        //将json字符串转json对象
        JSONObject taskProcessJsonObject = JSONObject.parseObject(taskProcessJson);
        JSONObject taskProcessBean = taskProcessJsonObject.getJSONObject("taskProcessBean");
        JSONObject processJson = taskProcessJsonObject.getJSONObject("processJson");

        this.taskId=taskProcessBean.getString("taskId");
        this.taskName = taskProcessBean.getString("taskName");
        this.etlTaskInfo=taskInfoService.selectById(taskId);

        this.parseLineList(processJson.getJSONArray("lineList"));
        this.parseNodeList(processJson,false);
        this.etlTaskInfo.setProcessJson(taskProcessJson);
        taskInfoService.updateById(this.etlTaskInfo);
    }
    HashMap<String, Object> columnMap = new HashMap<>(16);
    public void parseNodeList(JSONObject processJson,Boolean flag){
        List<JSONObject> nodeList=processJson.getJSONArray("nodeList").toJavaList(JSONObject.class);
        AtomicInteger index=new AtomicInteger(0);
        columnMap.put("task_id",this.taskId);
        AtomicReference<Boolean> tableFlag = new AtomicReference<>(true);
        AtomicReference<Boolean> logicFlag= new AtomicReference<>(true);
        AtomicReference<Boolean> outputFlag= new AtomicReference<>(true);
        AtomicReference<Boolean> joinFlag = new AtomicReference<>(true);
        AtomicReference<Boolean> groupFlag= new AtomicReference<>(true);
        AtomicReference<Boolean> orderFlag= new AtomicReference<>(true);
        nodeList.forEach(node->{
            String type = node.getString("type");
            switch (type){
                case "table":{
                    taskInputInfoManipulation(node,tableFlag);
                }break;
                case "join":{
                    taskJoinInfoManipulation(node,index,flag,joinFlag);
                }break;
                case "group":{
                    taskGroupInfoManipulation(node,flag,groupFlag);
                }break;
                case "order":{
                    taskOrderInfoManipulation(node,flag,orderFlag);
                }break;
                case "logic":{
                    taskLogicInfoManipulation(node,flag,logicFlag);
                }break;
                case "output":{
                    taskOutputInfoManipulation(node,flag,outputFlag);
                }break;
                case "start":
                case "end":
                default:break;
            }
        });
    }

    public void taskInputInfoManipulation(JSONObject node,AtomicReference<Boolean> tableFlag){
        EtlTaskInputInfoEntity taskInputInfo =new EtlTaskInputInfoEntity(node, DateUtils.getNowDate());
        taskInputInfos.add(taskInputInfo);
        if(tableFlag.get()){
            taskInputInfoService.deleteByMap(columnMap);
            tableFlag.set(false);
        }
        taskInputInfoService.insert(taskInputInfo);
    }

    public void taskJoinInfoManipulation(JSONObject node,AtomicInteger index,Boolean flag,AtomicReference<Boolean> joinFlag){
        EtlTaskJoinInfoEntity taskJoinInfo = new EtlTaskJoinInfoEntity(node, DateUtils.getNowDate(), taskInputInfos.get(index.get()).getId(), taskInputInfos.get(index.get()+1).getId());
        index.incrementAndGet();
        if(flag){
            taskJoinInfos.add(taskJoinInfo);
        }else{
            if(joinFlag.get()){
                taskJoinInfoService.deleteByMap(columnMap);
                joinFlag.set(false);
            }
            taskJoinInfoService.insert(taskJoinInfo);
        }
    }

    public void taskGroupInfoManipulation(JSONObject node,Boolean flag,AtomicReference<Boolean> groupFlag){
        EtlTaskGroupInfoEntity taskGroupInfo = new EtlTaskGroupInfoEntity(node,  DateUtils.getNowDate());
        if(flag){
            taskGroupInfos.add(taskGroupInfo);
        }else{
            if(groupFlag.get()){
                taskGroupInfoService.deleteByMap(columnMap);
                groupFlag.set(false);
            }
            taskGroupInfoService.insert(taskGroupInfo);
        }
    }

    public void taskOrderInfoManipulation(JSONObject node,Boolean flag,AtomicReference<Boolean> orderFlag){
        EtlTaskOrderInfoEntity taskOrderInfo = new EtlTaskOrderInfoEntity(node,  DateUtils.getNowDate());
        if(flag){
            taskOrderInfos.add(taskOrderInfo);
        }else {
            if(orderFlag.get()){
                taskGroupInfoService.deleteByMap(columnMap);
                orderFlag.set(false);
            }
            taskOrderInfoService.insert(taskOrderInfo);
        }
    }

    public void taskLogicInfoManipulation(JSONObject node,Boolean flag,AtomicReference<Boolean> logicFlag){
        EtlTaskLogicAnnalInfoEntity taskLogicInfo = new EtlTaskLogicAnnalInfoEntity(node,  DateUtils.getNowDate());
        if(flag){
            taskLogicInfos.add(taskLogicInfo);
        }else{
            if(logicFlag.get()){
                taskLogicInfoService.deleteByMap(columnMap);
                logicFlag.set(false);
            }
            taskLogicInfoService.insert(taskLogicInfo);
        }
    }

    public void taskOutputInfoManipulation(JSONObject node,Boolean flag,AtomicReference<Boolean> outputFlag){
        EtlTaskOutputInfoEntity taskOutputInfo = new EtlTaskOutputInfoEntity(node,  DateUtils.getNowDate());
        if(flag){
            taskOutputInfos.add(taskOutputInfo);
        }else{
            if(outputFlag.get()){
                taskOutputInfoService.deleteByMap(columnMap);
                outputFlag.set(false);
            }
            taskOutputInfoService.insert(taskOutputInfo);
        }
    }



    public void parseLineList(JSONArray lineList){
        HashMap<String, Object> columnMap = new HashMap<>(1);
        columnMap.put("task_id",this.taskId);
        taskLineInfoService.removeByMap(columnMap);
        for (int i = 0; i < lineList.size(); i++) {
            EtlTaskLineInfo taskLine =lineList.getJSONObject(i).toJavaObject(EtlTaskLineInfo.class);
            taskLine.setId(UUID.randomUUID().toString());
            taskLine.setTaskId(this.taskId);
            taskLineInfos.add(taskLine);
            taskLineInfoService.save(taskLine);
        }
    }

    public void clearAllList(){
        taskInputInfos.clear();
        taskJoinInfos.clear();
        taskGroupInfos.clear();
        taskOrderInfos.clear();
        taskLineInfos.clear();
        taskLogicInfos.clear();
        taskOutputInfos.clear();
    }

    private StringBuffer taskSql=new StringBuffer("select ");

    @Override
    public String getTask(String taskId){
        clearAllList();
        EtlTaskInfoEntity etlTaskInfoEntity = taskInfoService.selectById(taskId);
        String processJson = etlTaskInfoEntity.getProcessJson();
        if(processJson==null){
            return null;
        }
        JSONObject processJsonObject = JSONObject.parseObject(processJson);
        parseNodeList(processJsonObject.getJSONObject("processJson"),true);
        taskInputInfos.forEach(taskInput->{
            String[] tableFields = taskInput.getTableField().split(",");
            String[] tableAsField = taskInput.getTableAsField().split(",");
            String tableAsName =taskInput.getTableAsName();
            String tableName = tableAsName!=null?tableAsName:taskInput.getDatabaseId()+"."+taskInput.getTableName();
            for(int i=0;i<tableFields.length;i++){
                taskSql.append(tableName).append(".").append(tableFields[i]).append(" as ").append(tableAsField[i]).append(",");
            }
        });
        taskSql.deleteCharAt(taskSql.length()-1);
        appendJoin();
        appendGroup();
        appendOrder();
        return taskSql.toString();
    }

    public void appendJoin(){
        if(taskJoinInfos.size()>0){
            for (int i = 0; i < taskJoinInfos.size(); i++) {
                EtlTaskInputInfoEntity left = taskInputInfos.get(i);
                EtlTaskInputInfoEntity right = taskInputInfos.get(i+1);
                taskSql.append(" from ").append(left.getDatabaseId()).append(".").append(left.getTableName());
                if(left.getTableAsName()!=null){
                    taskSql.append(" as ").append(left.getTableAsName());
                }
                switch (taskJoinInfos.get(i).getJoinType()){
                    case "1":
                    case "left":taskSql.append(" left join ");break;
                    case "2":
                    case "right":taskSql.append(" right join ");break;
                    case "3":
                    case "inner":taskSql.append(" inner join ");break;
                    default:break;
                }
                taskSql.append(right.getDatabaseId()).append(".").append(right.getTableName());
                if(right.getTableAsName()!=null){
                    taskSql.append(" as ").append(right.getTableAsName());
                }
                taskSql.append(" where ");
                String leftTableName=left.getTableAsName()==null?left.getTableName():left.getTableAsName();
                String rightTableName=right.getTableName()==null?right.getTableName():right.getTableAsName();
                taskSql.append(leftTableName).append(".").append(taskJoinInfos.get(i).getLeftJoinField()).append("=").append(rightTableName).append(".").append(taskJoinInfos.get(i).getRightJoinField());
            }
        }else{
            EtlTaskInputInfoEntity input = taskInputInfos.get(0);
            taskSql.append(" from ").append(input.getDatabaseId()).append(".").append(input.getTableName());
        }
    }

    public void appendGroup(){
        if(taskGroupInfos.size()>0){
            for (int i = 0; i < taskGroupInfos.size(); i++) {
                if(i==0){
                    taskSql.append(" group by ");
                }
                int finalI = i;
                taskInputInfos.forEach(taskInput->{
                    if(taskInput.getNodeId().equals(taskGroupInfos.get(finalI).getSelectNodeId())){
                        String tableName=taskInput.getTableAsName()==null?taskInput.getDatabaseId()+"."+ taskInput.getTableName():taskInput.getTableAsName();

                        taskSql.append(tableName).append(".").append(taskGroupInfos.get(finalI).getSelectTableField()).append(",");
                    }
                });
            }
            taskSql.deleteCharAt(taskSql.length()-1);
        }

    }

    public void appendOrder(){
        if(taskOrderInfos.size()>0){
            for (int i = 0; i < taskOrderInfos.size(); i++) {
                if(i==0){
                    taskSql.append(" order by ");
                }
                int finalI = i;
                taskInputInfos.forEach(taskInput->{
                    if(taskInput.getNodeId().equals(taskOrderInfos.get(finalI).getSelectNodeId())){
                        String tableName=taskInput.getTableAsName()==null?taskInput.getDatabaseId()+"."+ taskInput.getTableName():taskInput.getTableAsName();
                        taskSql.append(tableName).append(".").append(taskOrderInfos.get(finalI).getSelectTableField());
                    }
                });
                if(taskOrderInfos.get(i).getOrderSort().equals(1)){
                    taskSql.append(" asc,");
                }else{
                    taskSql.append(" desc,");
                }
            }
            taskSql.deleteCharAt(taskSql.length()-1);
        }
    }
}