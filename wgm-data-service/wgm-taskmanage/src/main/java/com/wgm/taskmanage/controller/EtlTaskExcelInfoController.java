package com.wgm.taskmanage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wgm.taskmanage.service.EtlTaskExcelInfoService;



/**
 * 数据引擎excel表头模板表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@RestController
@RequestMapping("taskmanage/etltaskexcelinfo")
public class EtlTaskExcelInfoController {
    @Autowired
    private EtlTaskExcelInfoService etlTaskExcelInfoService;



}
