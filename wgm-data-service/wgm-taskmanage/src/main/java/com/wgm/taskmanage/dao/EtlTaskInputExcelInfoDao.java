package com.wgm.taskmanage.dao;

import com.wgm.taskmanage.entity.EtlTaskInputExcelInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 任务数据输入excel表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Mapper
public interface EtlTaskInputExcelInfoDao extends BaseMapper<EtlTaskInputExcelInfoEntity> {

    /**
     *根据id删除Excel信息
     * @param id
     */
    void deleteEtlExcelInfoById(String id);
}
