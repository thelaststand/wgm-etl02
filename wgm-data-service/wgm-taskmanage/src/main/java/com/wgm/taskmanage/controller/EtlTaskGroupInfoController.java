package com.wgm.taskmanage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wgm.taskmanage.service.EtlTaskGroupInfoService;


/**
 * 任务处理分组表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@RestController
@RequestMapping("taskmanage/etltaskgroupinfo")
public class EtlTaskGroupInfoController {
    @Autowired
    private EtlTaskGroupInfoService etlTaskGroupInfoService;
}
