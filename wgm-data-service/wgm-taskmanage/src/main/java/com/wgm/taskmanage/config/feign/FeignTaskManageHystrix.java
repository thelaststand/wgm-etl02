package com.wgm.taskmanage.config.feign;

import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonResponse;
import org.springframework.stereotype.Component;

/**
*   class
*   @Author 小脸蛋
*   @CreateDate 2021/1/15
*/
@Component
public class FeignTaskManageHystrix implements FeignTaskManage {
    @Override
    public CommonResponse findById(String id) {
        return CommonResponse.error(CommonConstants.ErrorMsg.FEIGN_HTSTRIX);
    }

    @Override
    public CommonResponse<String> selectAllDatasource() {
        return CommonResponse.error(CommonConstants.ErrorMsg.FEIGN_HTSTRIX);
    }

    @Override
    public CommonResponse<String> selectDatasourceById(String id) {
        return CommonResponse.error(CommonConstants.ErrorMsg.FEIGN_HTSTRIX);
    }

    @Override
    public CommonResponse<String> selectAllFieldByTableName(String id, String tableName, String asName) {
        return CommonResponse.error(CommonConstants.ErrorMsg.FEIGN_HTSTRIX);
    }

    @Override
    public CommonResponse<String> selectAllField(String id, String tableName) {
        return CommonResponse.error(CommonConstants.ErrorMsg.FEIGN_HTSTRIX);
    }
}
