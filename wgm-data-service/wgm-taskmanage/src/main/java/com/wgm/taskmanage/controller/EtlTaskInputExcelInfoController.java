package com.wgm.taskmanage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wgm.taskmanage.service.EtlTaskInputExcelInfoService;


/**
 * 任务数据输入excel表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@RestController
@RequestMapping("taskmanage/etltaskinputexcelinfo")
public class EtlTaskInputExcelInfoController {
    @Autowired
    private EtlTaskInputExcelInfoService etlTaskInputExcelInfoService;



}
