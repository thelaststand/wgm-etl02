package com.wgm.taskmanage.dao;

import com.wgm.taskmanage.entity.EtlTaskGroupInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 任务处理分组表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Mapper
public interface EtlTaskGroupInfoDao extends BaseMapper<EtlTaskGroupInfoEntity> {

    /**
     * 删除
     * @param id
     */
    void deleteGroupInfoById(@Param("id") String id);

    /**
     * 查询
     * @param taskId
     * @return
     */
    List<EtlTaskGroupInfoEntity> findTaskId(String taskId);
}
