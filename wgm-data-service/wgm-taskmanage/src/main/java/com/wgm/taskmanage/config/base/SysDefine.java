package com.wgm.taskmanage.config.base;

import java.util.HashMap;
import java.util.Map;
/**
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
public class SysDefine {

    /**
     * 菜单主节点
     */
    public static final String MODULESBASE = "0";

    /**
     * 通用分割符号
     */
    public static final String SPLITSIGN = ",";

    /**
     * 特殊分隔符号
     */
    public static final String SPECIALSPLITSIGN = "[<|SPLIT|>]";

    /**
     * 非逻辑处理填充符
     */
    public static final String NOSELECTLOGIC = "noSelectLogic";

    /**
     * 系统参数：真
     */
    public static final int TRUE = 1;

    /**
     * 系统参数：假
     */
    public static final int FALSE = 0;

    /**
     * 分页条数
     */
    public static final int PAGESIZE = 20000;

    public interface TaskInfoStatus{
        public static final String  NEWTASK = "-1";
        public static final String  CANTASK = "0";
    }

    public interface DatabaseMysqlConnection{
        public Map<String,String> ERRORMAP = new HashMap<String,String>(){{
            put("01S00","数据库连接异常缺少[serverTimezone]配置项！");
            put("28000","数据库连接密码错误！");
            put("42000","查询无此数据库！");
        }};
    }

    public static String getMysqlError(String sqlStatus){
        String errorValue = DatabaseMysqlConnection.ERRORMAP.get(sqlStatus);
        return errorValue == null ? "数据库连接异常！" : errorValue;
    }
}
