package com.wgm.taskmanage.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

/**
 * (EtlTaskLineInfo)表实体类
 *
 * @author makejava
 * @since 2021-01-09 10:27:59
 */
@SuppressWarnings("serial")
public class EtlTaskLineInfo extends Model<EtlTaskLineInfo> {

    /**
     * id
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**任务id
     *
     */
    private String taskId;
    /**连线起点
     *
     */
    @JSONField(name = "from")
    private String lineFrom;
    /**连线终点
     *
     */
    @JSONField(name = "to")
    private String lineTo;
    /**
     *
     */
    private Long executeOrder;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getLineFrom() {
        return lineFrom;
    }

    public void setLineFrom(String lineFrom) {
        this.lineFrom = lineFrom;
    }

    public String getLineTo() {
        return lineTo;
    }

    public void setLineTo(String lineTo) {
        this.lineTo = lineTo;
    }

    public Long getExecuteOrder() {
        return executeOrder;
    }

    public void setExecuteOrder(Long executeOrder) {
        this.executeOrder = executeOrder;
    }

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}