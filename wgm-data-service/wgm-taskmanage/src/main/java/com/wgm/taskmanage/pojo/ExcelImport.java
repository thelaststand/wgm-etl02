package com.wgm.taskmanage.pojo;

import com.wgm.common.constant.CommonConstants;
import com.wgm.common.exception.TaskException;
import com.wgm.taskmanage.entity.EtlTaskInfoEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
*   class
*   @Author 小脸蛋
*   @CreateDate 2021/1/10
*/
@Slf4j
public class ExcelImport {

    public static List show(MultipartFile fileName) {
        ArrayList<EtlTaskInfoEntity> ts = new ArrayList<>();
        try {
            //读文件
            InputStream inputStream = fileName.getInputStream();
            //xssfWorkbook对应于你读到的这个表格
            HSSFWorkbook xssfWorkbook = new HSSFWorkbook(inputStream);
            //一个表格可能有好多sheet，通过getSheetAt(0),拿到第一个sheet
            HSSFSheet sheet = xssfWorkbook.getSheetAt(0);
            if (sheet != null) {
                //遍历行，i = 1，从第二行开始，第一行是表头跳过。
                for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
                    //row是一行数据，row.getCell(i),代表拿到这一行，第i列数据
                    Row row = sheet.getRow(i);
                    if (row == null) {
                        continue;
                    }
                    Cell cell = null;
                    Double a = null;
                    EtlTaskInfoEntity entity = new EtlTaskInfoEntity();
                    cell = row.getCell(0);
                    entity.setName(cell == null ? "" : cell.toString());
                    cell = row.getCell(1);
                    entity.setWeight(cell == null ? null :Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(2);
                    entity.setProcessStatus(cell == null ? null : Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(3);
                    entity.setStatus(cell == null ? null : Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(4);
                    entity.setTotal(cell == null ? null :  Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(5);
                    entity.setTotal(cell == null ? null : Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(6);
                    entity.setProcessTotal(cell == null ? null :  Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(7);
                    entity.setProportion(cell == null ? null : new BigDecimal(cell.toString()));
                    cell = row.getCell(8);
                    entity.setErrorTotal(cell == null ? null : Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(8);
                    entity.setExecuteTotal(cell == null ? null : Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(9);
                    entity.setCompleteExecuteTotal(cell == null ? null : Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(10);
                    entity.setSuccessInsertTotal(cell == null ? null : Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(11);
                    entity.setSuccessUpdateTotal(cell == null ? null : Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(12);
                    entity.setSuccessDeleteTotal(cell == null ? null : Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(13);
                    entity.setErrorAddTotal(cell == null ? null : Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(14);
                    entity.setErrorLogicTotal(cell == null ? null : Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(15);
                    entity.setCompleteTime(cell == null ? null : Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(16);
                    entity.setGrandTotalTime(cell == null ? null : Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(17);
                    entity.setProcessJson(cell == null ? null : cell.toString());
                    cell = row.getCell(18);
                    entity.setRevision(cell == null ? null : Double.valueOf(cell.toString()).intValue());
                    cell = row.getCell(19);
                    entity.setCreateBy(cell == null ? null : String.valueOf(Double.valueOf(cell.toString()).intValue()));
                    cell = row.getCell(21);
                    entity.setUpdateBy(cell == null ? null : String.valueOf(Double.valueOf(cell.toString()).intValue()));
                    entity.setCreateTime(new Date());
                    entity.setUpdateTime(new Date());
                    ts.add(entity);
                }
            }
            return ts;
        } catch (Exception e) {
            log.info("error,message->{}", e.getLocalizedMessage());
            throw new TaskException(CommonConstants.ErrorMsg.FILE_TEMPLATE_ERROR);
        }
    }
}
