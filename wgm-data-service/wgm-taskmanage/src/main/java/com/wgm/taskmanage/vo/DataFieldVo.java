package com.wgm.taskmanage.vo;

import lombok.Data;

/**
 * @author kangbaby
 */
@Data
public class DataFieldVo {

    private String fieldName;

    private String dataType;
}
