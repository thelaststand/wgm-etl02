package com.wgm.taskmanage.exception;


/**
 * @author kangbaby
 */
public class TaskJsonToBeanException extends RuntimeException {

    public TaskJsonToBeanException(String message) {
        super(message);
    }
}
