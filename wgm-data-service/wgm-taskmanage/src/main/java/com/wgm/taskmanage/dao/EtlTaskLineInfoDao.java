package com.wgm.taskmanage.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.taskmanage.entity.EtlTaskLineInfo;

/**
 * (EtlTaskLineInfo)表数据库访问层
 *
 * @author makejava
 * @since 2021-01-09 10:27:59
 */
public interface EtlTaskLineInfoDao extends BaseMapper<EtlTaskLineInfo> {

}