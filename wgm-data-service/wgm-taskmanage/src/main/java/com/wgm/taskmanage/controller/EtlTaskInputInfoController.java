package com.wgm.taskmanage.controller;

import java.util.List;
import java.util.Map;

import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonResponse;
import com.wgm.aop.anno.LogAnno;
import com.wgm.taskmanage.pojo.FieldRequest;
import com.wgm.taskmanage.vo.DataSourceVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wgm.taskmanage.entity.EtlTaskInputInfoEntity;
import com.wgm.taskmanage.service.EtlTaskInputInfoService;


/**
 * 任务数据输入表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@RestController
@RequestMapping("taskmanage/etltaskinputinfo")
@Slf4j
public class EtlTaskInputInfoController {

    @Autowired
    private EtlTaskInputInfoService etlTaskInputInfoService;

    /**
     * 查询数据输入表信息
     */
    @GetMapping("/list")
    @LogAnno(operationName = "查询数据输入表信息",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse findTaskInputInfoList(EtlTaskInputInfoEntity etlTaskInputInfo){
        List<EtlTaskInputInfoEntity> data =  etlTaskInputInfoService.findTaskInputInfoList(etlTaskInputInfo);
        return  CommonResponse.ok(data);
    }

    /**
     * 查询所有的数据源
     */
    @GetMapping("/dataList")
    @LogAnno(operationName = "查询所有的数据源",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse dataList(){
        List<DataSourceVo> data =  etlTaskInputInfoService.dataList();
        return CommonResponse.ok(data);
    }

    /**
     * 查询数据库的所有表名称
     * @param taskId 数据源的id
     * @return
     */
    @GetMapping("/dataTableList/{taskId}")
    @LogAnno(operationName = "查询数据库的所有表名称",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse dataTableList(@PathVariable("taskId") String taskId){
        List<Map<String, Object>> data =  etlTaskInputInfoService.dataTableList(taskId);
        return CommonResponse.ok(data);
    }

    /**
     * 通过数据库名、表名查询所有的字段名
     * @param request
     * @return
     */
    @PostMapping("/fieldList")
    @LogAnno(operationName = "通过数据库名、表名查询所有的字段名",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse fieldList(@RequestBody FieldRequest request){
        List<Map<String, Object>> data = etlTaskInputInfoService.dataFieldList(request);
        return CommonResponse.ok(data);
    }



}
