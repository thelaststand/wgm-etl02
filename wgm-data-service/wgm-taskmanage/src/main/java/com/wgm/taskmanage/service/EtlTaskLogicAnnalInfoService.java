package com.wgm.taskmanage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.taskmanage.entity.EtlTaskLogicAnnalInfoEntity;

/**
 * 任务处理过程表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
public interface EtlTaskLogicAnnalInfoService extends IService<EtlTaskLogicAnnalInfoEntity> {

    /**
     * 解析json
     * @param taskProcessJson
     */
    void parseJson(String taskProcessJson);

    /**
     * 获取任务处理过程
     * @param taskId
     * @return
     */
    String getTask(String taskId);
}

