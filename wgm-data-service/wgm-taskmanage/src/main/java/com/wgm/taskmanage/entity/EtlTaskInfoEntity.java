package com.wgm.taskmanage.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 任务信息表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Data
@TableName("etl_task_info")
public class EtlTaskInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 任务名称
	 */
	private String name;
	/**
	 * 任务权重级别 0紧急任务1高2中3低
	 */
	private Integer weight;
	/**
	 * 任务执行状态 -1待执行 0可执行 1正在执行 2执行完成
	 */
	private Integer processStatus;
	/**
	 * 任务状态 1无错误0有错误
	 */
	private Integer status;
	/**
	 * 总处理条数
	 */
	private Integer total;
	/**
	 * 已处理条数
	 */
	private Integer processTotal;
	/**
	 * 任务完成度
	 */
	private BigDecimal proportion;
	/**
	 * 错误条数
	 */
	private Integer errorTotal;
	/**
	 * 总需执行次数
	 */
	private Integer executeTotal;
	/**
	 * 已完成执行次数
	 */
	private Integer completeExecuteTotal;
	/**
	 * 成功插入条数
	 */
	private Integer successInsertTotal;
	/**
	 * 成功修改条数
	 */
	private Integer successUpdateTotal;
	/**
	 * 成功删除条数
	 */
	private Integer successDeleteTotal;
	/**
	 * 插入失败条数
	 */
	private Integer errorAddTotal;
	/**
	 * 逻辑失败条数
	 */
	private Integer errorLogicTotal;
	/**
	 * 预计完成时间 精确到秒
	 */
	private Integer completeTime;
	/**
	 * 累计处理时间 精确到秒
	 */
	private Integer grandTotalTime;
	/**
	 * 前端json对象
	 */
	private String processJson;
	/**
	 * 乐观锁
	 */
	private Integer revision;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新人
	 */
	private String updateBy;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * tombstone 逻辑删除
	 */
	@TableLogic(value = "0",delval = "1")
	private Integer tombstone;

}
