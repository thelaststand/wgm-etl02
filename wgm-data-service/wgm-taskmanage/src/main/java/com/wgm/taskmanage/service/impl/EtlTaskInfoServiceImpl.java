package com.wgm.taskmanage.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.nacos.client.utils.StringUtils;
import com.wgm.common.constant.CommonStatus;
import com.wgm.common.exception.AdException;
import com.wgm.taskmanage.config.feign.FeignTaskManage;
import com.wgm.taskmanage.config.task.ExecuteSql;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.exception.TaskException;
import com.wgm.datasource.pojo.DataSource;
import com.wgm.taskmanage.dao.*;
import com.wgm.taskmanage.entity.*;

import com.wgm.taskmanage.pojo.ExcelImport;
import org.springframework.beans.factory.annotation.Autowired;
import com.wgm.taskmanage.vo.TaskManageInfoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.*;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.common.utils.PageUtils;

import com.wgm.taskmanage.service.EtlTaskInfoService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

/**
 * 任务信息service实现类
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Service("etlTaskInfoService")
@Transactional(rollbackFor = Exception.class)
public class EtlTaskInfoServiceImpl extends ServiceImpl<EtlTaskInfoDao, EtlTaskInfoEntity> implements EtlTaskInfoService {

    @Autowired
    EtlTaskGroupInfoDao etlTaskGroupInfoDao;
    @Autowired
    EtlTaskInputExcelInfoDao etlTaskInputExcelInfoDao;
    @Autowired
    EtlTaskInputInfoDao etlTaskInputInfoDao;
    @Autowired
    EtlTaskJoinInfoDao etlTaskJoinInfoDao;
    @Autowired
    EtlTaskLogicAnnalInfoDao etlTaskLogicAnnalInfoDao;
    @Autowired
    EtlTaskLogicFieldInfoDao etlTaskLogicFieldInfoDao;
    @Autowired
    EtlTaskOrderInfoDao etlTaskOrderInfoDao;
    @Autowired
    EtlTaskOutputInfoDao etlTaskOutputInfoDao;
    @Autowired
    EtlTaskInfoDao etlTaskInfoDao;
    @Autowired
    FeignTaskManage feignTaskManage;

    private static final String SUFFIX=".xls";



    @Override
    public void add(EtlTaskInfoEntity etlTaskInfoEntity) {

        //请求值不能为空且任务名称不能为空
        if (etlTaskInfoEntity==null||etlTaskInfoEntity.getName()==null){
            throw new TaskException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //查询表中有没有这个任务名
        List<EtlTaskInfoEntity> etlTaskInfoEntities = this.baseMapper.selectList(null);

        if (etlTaskInfoEntities!=null){
            boolean flag = false;
            for (EtlTaskInfoEntity taskInfoEntity : etlTaskInfoEntities) {
                //判断任务名
                if (etlTaskInfoEntity.getName().equals(taskInfoEntity.getName())){
                    flag = true;
                    break;
                }
            }
            //判断flag
            if (flag){
                //存在抛出异常   任务名已存在数据库
                throw new TaskException(CommonConstants.TaskError.TASK_NAME_EXISTS);
            }
        }
        //判断任务权重是否为空
        if (etlTaskInfoEntity.getWeight()==null){
            throw new TaskException(CommonConstants.TaskError.TASK_WEIGHT_NULL);
        }else {
            //添加
            etlTaskInfoEntity.setProcessStatus(CommonStatus.PROCESS_STATUS_NO.getCode());
            etlTaskInfoEntity.setCreateTime(new Date());
            etlTaskInfoEntity.setUpdateTime(new Date());
            this.baseMapper.insert(etlTaskInfoEntity);
        }

    }

    @Override
    public PageUtils getTaskManageList(TaskManageInfoVo taskManageInfoVo, Integer pageSize, Integer currPage) {

        //超出100条
        if (pageSize> CommonConstants.Data.HUNDRED){
            //默认100条
            pageSize=CommonConstants.Data.HUNDRED;
        }
        //查询列表
        List<TaskManageInfoVo> taskManageList = etlTaskInfoDao.getTaskManageList(taskManageInfoVo.getName(),
                                                                                 taskManageInfoVo.getProcessStatus(),
                                                                                 taskManageInfoVo.getWeight(),
                                                                                 pageSize,
                                                                        (currPage-1)*pageSize);
        /**
         * 遍历TaskManageList列表
         */
        taskManageList.forEach(manageInfoVo->{
            if (manageInfoVo.getTotal()!=null && manageInfoVo.getErrorTotal()!=null && manageInfoVo.getTotal()!=null){

                //计算 任务完成度 =（已处理条数-错误条数）/总处理条数
                BigDecimal bigDecimal = new BigDecimal((manageInfoVo.getProcessTotal() - manageInfoVo.getErrorTotal()) /
                                                           (double)manageInfoVo.getTotal());
                //保留两位小数
                BigDecimal  twobigDecimal = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);

                manageInfoVo.setProportion(twobigDecimal);
                EtlTaskInfoEntity etlTaskInfoEntity = new EtlTaskInfoEntity();
                BeanUtils.copyProperties(manageInfoVo,etlTaskInfoEntity);

                //提交任务完成度
                this.baseMapper.updateById(etlTaskInfoEntity);
            }
        });
        //查询总记录数
        int totalCount = etlTaskInfoDao.getcount();

        PageUtils pageUtils = new PageUtils(taskManageList, totalCount, pageSize, currPage);

        return pageUtils;
    }

    /**
     * 逻辑删除
     * @param id
     * @return
     */
    @Override
    public boolean deleteEtlInfoById(String id) {
        //判断如果传过来的Id 是否为空
        if(StringUtils.isBlank(id)){
            throw  new AdException(CommonConstants.TaskError.TASK_DOES_NOT_EXSIT_ERROR);
        }
        //获取到这条数据
        EtlTaskInfoEntity entity = this.baseMapper.selectEtlInfoById(id);
        //判断 逻辑删除字段 字段 是否为展示 逻辑删除 1不显示 0显示
        if(entity.getTombstone() == 1){
            throw  new AdException(CommonConstants.TaskError.TASK_DOES_NOT_EXSIT_ERROR);
        }
        //判断任务执行状态 -1待执行 0可执行 1正在执行 2执行完成
        if(entity.getProcessStatus() != -1){
            throw  new AdException(CommonConstants.TaskError.TASK_IS_EXECUTING_ERROR);
        }
        this.baseMapper.deleteInfoById(id);
        etlTaskGroupInfoDao.deleteGroupInfoById(id);
        etlTaskInputExcelInfoDao.deleteEtlExcelInfoById(id);
        etlTaskInputInfoDao.deleteEtlInputInfoById(id);
        etlTaskJoinInfoDao.deleteEtlJoinInfoById(id);
        etlTaskLogicAnnalInfoDao.deleteEtlLogicAnnalInfoById(id);
        etlTaskLogicFieldInfoDao.deleteEtlLogicFieldInfoById(id);
        etlTaskOrderInfoDao.deleteEtlOrderInfoById(id);
        etlTaskOutputInfoDao.deleteEtlOutputInfoById(id);
        return true;
    }

    /**
     * 任务 设计
     * @param etlTaskInfoEntity
     */
    @Override
    public void designing(EtlTaskInfoEntity etlTaskInfoEntity) {

        //解析 json
        String processJson = etlTaskInfoEntity.getProcessJson();
        this.show(processJson);
    }

    /**
     * 解析 json
     * @param json
     */
    public void show(String json){

        JSONObject jsonObject = JSON.parseObject(json);

        Set<Map.Entry<String, Object>> entries = jsonObject.entrySet();

        entries.forEach((key) ->{
            Object list = jsonObject.get(key.getKey());
            JSONArray objects = JSON.parseArray(list.toString());
            System.out.println(objects);
        });

    }

    /**
     * 下载 excel 模板
     * @param response
     */
    @Override
    public void downloadTemplate(HttpServletResponse response) {

        String path = "excel.xls";

        File imageFile = new File(path);
        if (!imageFile.exists()) {
            return;
        }
        //下载的文件携带这个名称
        response.setHeader("Content-Disposition", "attachment;filename=" + path);
        //文件下载类型--二进制文件
        response.setContentType("application/octet-stream");
        try {
            FileInputStream fis = new FileInputStream(path);
            byte[] content = new byte[fis.available()];
            fis.read(content);
            fis.close();
            ServletOutputStream sos = response.getOutputStream();
            sos.write(content);
            sos.flush();
            sos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * excel 添加数据
     * @param file
     */
    @Override
    public void excelAdd(MultipartFile file) {
        //参数为空
        if (file.isEmpty()){
            throw new TaskException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        String originalFilename = file.getOriginalFilename();
        String last = originalFilename.substring(originalFilename.lastIndexOf("."));
        //判断 是否是 xls文件
        if (!SUFFIX.equals(last)){
            throw new TaskException(CommonConstants.ErrorMsg.FILE_LAST_ERROR);
        }
        //传入 文件 绝对路径
        List show = ExcelImport.show(file);
        if (show.size()>=0 && show!=null){
            show.forEach(etlTaskInfo->{
                etlTaskInfoDao.insert((EtlTaskInfoEntity) etlTaskInfo);
            });
        }
    }

    /**
     * 任务 执行
     * @param taskId
     */
    @Override
    public void execute(String taskId) {
        /**
         * 通过  任务 id 拿所有的任务
         */
        //任务处理分组表
        List<EtlTaskGroupInfoEntity> groupInfoDaoTaskId = etlTaskGroupInfoDao.findTaskId(taskId);
        //任务数据输入表
        List<EtlTaskInputInfoEntity> inputInfoDaoTaskId = etlTaskInputInfoDao.findTaskId(taskId);
        //数据关联表
        List<EtlTaskJoinInfoEntity> joinInfoDaoTaskId = etlTaskJoinInfoDao.findTaskId(taskId);
        //任务处理过程表
        List<EtlTaskLogicAnnalInfoEntity> logicAnnalInfoDaoTaskId = etlTaskLogicAnnalInfoDao.findTaskId(taskId);
        //任务处理排序表
        List<EtlTaskOrderInfoEntity> orderInfoDaoTaskId = etlTaskOrderInfoDao.findTaskId(taskId);
        //任务结果输出表
        List<EtlTaskOutputInfoEntity> outputInfoDaoTaskId = etlTaskOutputInfoDao.findTaskId(taskId);

        //操作  单表
        ArrayList<Map<String, StringBuilder>> arrayList = ExecuteSql.assembleSql(inputInfoDaoTaskId);
        //获取 数据库 信息

        DataSource dataSource = new DataSource();

        arrayList.forEach(info->{
            info.forEach((inputId,msg)->{
                System.out.println("准备执行的sql:"+msg);
                CommonResponse manageById = feignTaskManage.findById(inputId);
                BeanUtils.copyProperties(manageById.getData(),dataSource);
                //执行 sql
                ExecuteSql.select(dataSource, msg.toString());
            });
        });
    }

}