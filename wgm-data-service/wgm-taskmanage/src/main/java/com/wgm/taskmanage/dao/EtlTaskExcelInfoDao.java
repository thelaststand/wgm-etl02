package com.wgm.taskmanage.dao;

import com.wgm.taskmanage.entity.EtlTaskExcelInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据引擎excel表头模板表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Mapper
public interface EtlTaskExcelInfoDao extends BaseMapper<EtlTaskExcelInfoEntity> {


}
