package com.wgm.taskmanage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.common.utils.PageUtils;
import com.wgm.taskmanage.entity.EtlTaskInfoEntity;
import com.wgm.taskmanage.vo.TaskManageInfoVo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 任务信息表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
public interface EtlTaskInfoService extends IService<EtlTaskInfoEntity> {
    /**
     * 列表模糊+分页
     * @param taskManageInfoVo 模糊字段 任务名称,任务权重级别，任务执行状态
     * @param pageSize 每页记录数
     * @param currPage 当前页数
     * @return 返回列表数据
     */
    PageUtils getTaskManageList(TaskManageInfoVo taskManageInfoVo, Integer pageSize, Integer currPage);

    /**
     * 添加
     * @param etlTaskInfoEntity
     * @return
     */
    void add(EtlTaskInfoEntity etlTaskInfoEntity);

    /**
     *设计
     * @param etlTaskInfoEntity
     * @return
     */
    void designing(EtlTaskInfoEntity etlTaskInfoEntity);

    /**
     * 执行
     * @param taskId
     * @return
     */
    void execute(String taskId);

    /**
     *  添加 excel导入
     * @param file
     * @return
     */
    void excelAdd(MultipartFile file);

    /**
     * 逻辑删除
     * @param id
     * @return
     */
    boolean deleteEtlInfoById(String id);

    /**
     * 下载 excel 模板
     * @param response
     */
    void downloadTemplate(HttpServletResponse response);
}

