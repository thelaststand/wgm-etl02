package com.wgm.taskmanage.config.task;

import com.wgm.taskmanage.config.feign.FeignTaskManage;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.exception.TaskException;
import com.wgm.datasource.base.MysqlDataSource;
import com.wgm.datasource.pojo.DataSource;
import com.wgm.taskmanage.entity.EtlTaskInputInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
*   class
*   @Author 小脸蛋
*   @CreateDate 2021/1/12
*/
public class ExecuteSql {

    @Autowired
    private static FeignTaskManage feignTaskManage;

    /**
     * 执行 查找 语句
     */
    public static void select(DataSource dataSource, String sql){
        //连接驱动
        Connection connection = MysqlDataSource.getConnection(dataSource);
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()){
                String s = resultSet.toString();
                System.out.println(s);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                statement.close();
                connection.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        DataSource dataSource = new DataSource();
        dataSource.setIp("127.0.0.1");
        dataSource.setUsername("root");
        dataSource.setPassword("root");
        dataSource.setDbName("wgm_etl");
        dataSource.setPort("3306");
        dataSource.setParams("useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=Asia/Shanghai");
        select(dataSource,"USE wgm_etl ;  DESC etl_data_engine" );
    }

    /**
     * 单表  拼接  sql
     * @param inputInfoDaoTaskId
     * @return
     */
    public static ArrayList<Map<String, StringBuilder>> assembleSql (List<EtlTaskInputInfoEntity> inputInfoDaoTaskId){
        ArrayList<Map<String, StringBuilder>> arrayList = new ArrayList<>();
        Map<String, StringBuilder> map = new HashMap<>(16);

        //非 空 判断
        if (inputInfoDaoTaskId.size()==0 || inputInfoDaoTaskId==null){
            throw new TaskException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        EtlTaskInputInfoEntity input = inputInfoDaoTaskId.get(0);

        StringBuilder stringBuilder = new StringBuilder("select ");
        //表名称
        String tableName = input.getTableName();
        //表别名称
        String tableAsName = input.getTableAsName();
        //获取 字段
        String tableField = input.getTableField();
        //获取 字段别名
        String tableAsField = input.getTableAsField();

        String[] tableFields = tableField.split(",");
        String[] tableAsFields = tableAsField.split(",");
        //循环 拼接 字段 和 字段别名
        for (int i = 0; i < tableFields.length; i++) {
            stringBuilder.append(tableFields[i]+" "+tableAsFields[i]);
            if (i!=tableFields.length-1){
                stringBuilder.append(",");
            }
        }
        stringBuilder.append(" from ");
        //拼接 表
        stringBuilder.append(tableName+" "+tableAsName);
        map.put(input.getDatabaseId(),stringBuilder);
        arrayList.add(map);
        return arrayList;
    }
}
