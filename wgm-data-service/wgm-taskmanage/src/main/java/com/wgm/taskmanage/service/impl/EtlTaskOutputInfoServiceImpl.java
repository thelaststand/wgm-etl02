package com.wgm.taskmanage.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.wgm.taskmanage.dao.EtlTaskOutputInfoDao;
import com.wgm.taskmanage.entity.EtlTaskOutputInfoEntity;
import com.wgm.taskmanage.service.EtlTaskOutputInfoService;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author kangbaby
 */
@Service("etlTaskOutputInfoService")
@Transactional(rollbackFor = Exception.class)
public class EtlTaskOutputInfoServiceImpl extends ServiceImpl<EtlTaskOutputInfoDao, EtlTaskOutputInfoEntity> implements EtlTaskOutputInfoService {


}