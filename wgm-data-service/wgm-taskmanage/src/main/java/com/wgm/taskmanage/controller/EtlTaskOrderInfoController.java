package com.wgm.taskmanage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wgm.taskmanage.service.EtlTaskOrderInfoService;


/**
 * 任务处理排序表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@RestController
@RequestMapping("taskmanage/etltaskorderinfo")
public class EtlTaskOrderInfoController {
    @Autowired
    private EtlTaskOrderInfoService etlTaskOrderInfoService;



}
