package com.wgm.taskmanage.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 任务处理过程表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Data
@TableName("etl_task_logic_field_info")
public class EtlTaskLogicFieldInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 节点ID
	 */
	private String nodeId;
	/**
	 * 节点名称
	 */
	private String nodeName;
	/**
	 * 任务ID
	 */
	private String taskId;
	/**
	 * 选择节点Id
	 */
	private String selectNodeId;
	/**
	 * 选择表名称
	 */
	private String selectNodeTable;
	/**
	 * 选择字段名称
	 */
	private String selectNodeField;
	/**
	 * 记录级别处理方案
	 */
	private String annalEngine;
	/**
	 * 字段级别处理方案
	 */
	private String fieldEngine;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新人
	 */
	private String updateBy;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * tombstone 逻辑删除
	 */
	@TableLogic(value = "0",delval = "1")
	private Integer tombstone;
}
