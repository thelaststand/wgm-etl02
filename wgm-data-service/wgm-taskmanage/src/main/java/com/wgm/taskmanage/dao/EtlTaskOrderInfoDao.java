package com.wgm.taskmanage.dao;

import com.wgm.taskmanage.entity.EtlTaskOrderInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 任务处理排序表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Mapper
public interface EtlTaskOrderInfoDao extends BaseMapper<EtlTaskOrderInfoEntity> {

    /**
     *根据id删除任务处理排序
     * @param id
     */
    void deleteEtlOrderInfoById(@Param("id") String id);

    /**
     * 根据id查询任务处理排序
     * @param taskId
     * @return
     */
    List<EtlTaskOrderInfoEntity> findTaskId(String taskId);
}
