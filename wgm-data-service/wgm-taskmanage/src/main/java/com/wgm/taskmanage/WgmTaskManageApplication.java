package com.wgm.taskmanage;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/8
 */
@SpringBootApplication(scanBasePackages = {"com.wgm.taskmanage","com.wgm.aop"})
@MapperScan("com.wgm.taskmanage.dao")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.wgm.taskmanage.config.feign")
@EnableTransactionManagement
public class WgmTaskManageApplication {
    public static void main(String[] args) {
        SpringApplication.run(WgmTaskManageApplication.class,args);
    }
}
