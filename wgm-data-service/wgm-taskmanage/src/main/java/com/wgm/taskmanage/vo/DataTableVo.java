package com.wgm.taskmanage.vo;

import lombok.Data;

/**
 * 表名
 * @author 小脸蛋
 */
@Data
public class DataTableVo {

    private String tableName;
}
