package com.wgm.taskmanage.entity;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import lombok.Data;

/**
 * 任务结果输出表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:53
 */
@Data
@TableName("etl_task_output_info")
public class EtlTaskOutputInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 节点ID
	 */
	private String nodeId;
	/**
	 * 节点名称
	 */
	private String nodeName;
	/**
	 * 任务ID
	 */
	private String taskId;
	/**
	 * 
	 */
	private String databaseId;
	/**
	 * 输入字段
	 */
	private String inputField;
	/**
	 * 输出表
	 */
	private String outputTable;
	/**
	 * 输入表字段
	 */
	private String outputTableField;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新人
	 */
	private String updateBy;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * tombstone 逻辑删除
	 */
	@TableLogic(value = "0",delval = "1")
	private Integer tombstone;

	public EtlTaskOutputInfoEntity(JSONObject node, Date now) {
		this.id= UUID.randomUUID().toString();
		this.nodeId=node.getString("id");
		JSONObject nodeData = node.getJSONObject("data");
		this.taskId= nodeData.getString("taskId");
		this.inputField=nodeData.getString("fromField");
		this.outputTable=nodeData.getString("tableName");
		this.outputTableField=nodeData.getString("toField");
	}
}
