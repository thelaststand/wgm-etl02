package com.wgm.taskmanage.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 数据引擎excel表头模板表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Data
@TableName("etl_task_excel_info")
public class EtlTaskExcelInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 模块名称
	 */
	private String name;
	/**
	 * 表头json
	 */
	private String headList;
	/**
	 * 映射json
	 */
	private String filedList;
	/**
	 * 类型json
	 */
	private String typeList;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改人
	 */
	private String updateBy;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * tombstone 逻辑删除
	 */
	@TableLogic(value = "0",delval = "1")
	private Integer tombstone;

}
