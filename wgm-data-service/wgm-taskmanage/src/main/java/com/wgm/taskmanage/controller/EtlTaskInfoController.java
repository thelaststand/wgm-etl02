package com.wgm.taskmanage.controller;

import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.constant.CommonStatus;
import com.wgm.common.utils.PageUtils;
import com.wgm.aop.anno.LogAnno;

import com.wgm.taskmanage.service.EtlTaskLogicAnnalInfoService;
import com.wgm.taskmanage.vo.TaskManageInfoVo;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wgm.taskmanage.entity.EtlTaskInfoEntity;
import com.wgm.taskmanage.service.EtlTaskInfoService;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;


/**
 * 任务信息表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@RestController
@RequestMapping("taskmanage/etltaskinfo")
@Slf4j
public class EtlTaskInfoController {

    @Autowired
    private EtlTaskInfoService etlTaskInfoService;
    @Autowired
    private EtlTaskLogicAnnalInfoService etlTaskLogicAnnalInfoService;

    /**
     * 列表模糊+分页
     * @param taskManageInfoVo 模糊字段 任务名称,任务权重级别，任务执行状态
     * @param pageSize 每页记录数
     * @param currPage 当前页数
     * @return 返回列表数据
     */
    @GetMapping("taskManageList")
    @LogAnno(operationName = "列表模糊+分页",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse getTaskManageList(TaskManageInfoVo taskManageInfoVo,
                                            @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize,
                                            @RequestParam(value ="currPage",defaultValue = "1") Integer currPage) {
        PageUtils pageUtils = etlTaskInfoService.getTaskManageList(taskManageInfoVo,pageSize,currPage);
        return new CommonResponse(CommonStatus.VALID,pageUtils);
    }

    /**
     * 添加
     * @param etlTaskInfoEntity
     * @return
     */
    @PostMapping
    @LogAnno(operationName = "添加",operationType = CommonConstants.OperationType.INSTER)
    public CommonResponse add(@RequestBody EtlTaskInfoEntity etlTaskInfoEntity){
        log.info("EtlTaskInfoController-post{}",etlTaskInfoEntity);
        etlTaskInfoService.add(etlTaskInfoEntity);
        return new CommonResponse(CommonStatus.VALID);
    }

    /**
     * 逻辑删除
     * @param id
     * @return
     */
    @PostMapping("/delete")
    @LogAnno(operationName = "逻辑删除",operationType = CommonConstants.OperationType.DELETE)
    public CommonResponse delete(String id){
        return etlTaskInfoService.deleteEtlInfoById(id) ? new CommonResponse(CommonStatus.VALID): new CommonResponse(CommonStatus.INVALID);
    }

    /**
     * 下载 excel 模板
     * @param response
     */
    @GetMapping("/download")
    @LogAnno(operationName = "下载 excel 模板",operationType = CommonConstants.OperationType.INSTER)
    public void  downloadTemplate(HttpServletResponse response){
        etlTaskInfoService.downloadTemplate(response);
    }
    /**
     *  添加 excel导入
     * @param file
     * @return
     */
    @PostMapping("/excel")
    @LogAnno(operationName = "添加 excel导入",operationType = CommonConstants.OperationType.INSTER)
    public CommonResponse excelAdd(@RequestParam("file") MultipartFile file){
        log.info("file->{}",file.getOriginalFilename());
        etlTaskInfoService.excelAdd(file);
        return new CommonResponse(CommonStatus.VALID);
    }

    /**
     *设计
     * @param etlTaskInfoEntity
     * @return
     */
    @PostMapping("/designing")
    @LogAnno(operationName = "设计",operationType = CommonConstants.OperationType.INSTER)
    public CommonResponse designing(@RequestBody EtlTaskInfoEntity etlTaskInfoEntity){
        etlTaskInfoService.designing(etlTaskInfoEntity);
        return  new CommonResponse(CommonStatus.VALID);
    }

    /**
     * 执行
     * @param taskId
     * @return
     */
    @PostMapping("/execute/{id}")
    @LogAnno(operationName = "执行",operationType = CommonConstants.OperationType.INSTER)
    public CommonResponse execute(@PathVariable("id") String taskId){
        etlTaskInfoService.execute(taskId);
        return new CommonResponse(CommonStatus.VALID);
    }

    /**
     * 添加任务
     * @param taskProcessJson
     * @return
     */
    @PostMapping("/saveTask")
    @LogAnno(operationName = "添加任务",operationType = CommonConstants.OperationType.INSTER)
    public CommonResponse saveTaskProcess(@RequestBody String taskProcessJson){
        etlTaskLogicAnnalInfoService.parseJson(taskProcessJson);
        return CommonResponse.ok(CommonStatus.VALID);
    }

    /**
     * 执行任务
     * @param taskId
     * @return
     */
    @GetMapping("/executeTask")
    @LogAnno(operationName = "添加任务",operationType = CommonConstants.OperationType.INSTER)
    public CommonResponse executeTask(@RequestParam(value = "taskId") String taskId){
        String sql = etlTaskLogicAnnalInfoService.getTask(taskId);
        System.out.println(sql);
        return CommonResponse.ok(CommonStatus.VALID);
    }


}
