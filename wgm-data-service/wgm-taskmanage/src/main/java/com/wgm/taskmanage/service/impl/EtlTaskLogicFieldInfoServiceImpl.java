package com.wgm.taskmanage.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.wgm.taskmanage.dao.EtlTaskLogicFieldInfoDao;
import com.wgm.taskmanage.entity.EtlTaskLogicFieldInfoEntity;
import com.wgm.taskmanage.service.EtlTaskLogicFieldInfoService;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author kangbaby
 */
@Service("etlTaskLogicFieldInfoService")
@Transactional(rollbackFor = Exception.class)
public class EtlTaskLogicFieldInfoServiceImpl extends ServiceImpl<EtlTaskLogicFieldInfoDao, EtlTaskLogicFieldInfoEntity> implements EtlTaskLogicFieldInfoService {


}