package com.wgm.taskmanage.dao;

import com.wgm.taskmanage.entity.EtlTaskJoinInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 数据关联表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Mapper
@Component
public interface EtlTaskJoinInfoDao extends BaseMapper<EtlTaskJoinInfoEntity> {

    /**
     *删除数据关联
     * @param id
     */
    void deleteEtlJoinInfoById(@Param("id") String id);

    /**
     * 根据id查数据关联表
     * @param taskId
     * @return
     */
    List<EtlTaskJoinInfoEntity> findTaskId(String taskId);
}
