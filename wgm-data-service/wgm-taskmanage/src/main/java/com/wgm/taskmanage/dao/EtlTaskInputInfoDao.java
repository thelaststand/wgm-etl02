package com.wgm.taskmanage.dao;

import com.wgm.taskmanage.entity.EtlTaskInputInfoEntity;
import com.wgm.taskmanage.pojo.FieldRequest;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.taskmanage.vo.DataFieldVo;
import com.wgm.taskmanage.vo.DataSourceVo;
import com.wgm.taskmanage.vo.DataTableVo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 任务数据输入表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Mapper
@Component
public interface EtlTaskInputInfoDao extends BaseMapper<EtlTaskInputInfoEntity> {

    /**
     * 根据id删除任务数据输入
     * @param id
     */
    void deleteEtlInputInfoById(@Param("id") String id);

    /**
     * 查询任务数据输入
     * @param etlTaskInputInfo
     * @return
     */
    List<EtlTaskInputInfoEntity> findTaskInputInfoList(EtlTaskInputInfoEntity etlTaskInputInfo);

    /**
     *根据id查询任务数据输入
     * @param taskId
     * @return
     */
    List<EtlTaskInputInfoEntity> findTaskId(String taskId);

    /**
     *查询数据库所有表名
     * @param taskId
     * @return
     */
    List<DataTableVo> dataTableList(String taskId);

    /**
     *查询表所有字段
     * @param request
     * @return
     */
    List<DataFieldVo> dataFieldList(FieldRequest request);

    /**
     *查询所有数据库
     * @return
     */
    List<DataSourceVo> dataList();

}
