package com.wgm.taskmanage.controller;

import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonResponse;
import com.wgm.taskmanage.config.feign.FeignTaskManage;
import com.wgm.aop.anno.LogAnno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 任务结果输出表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:53
 */
@RestController
@RequestMapping("taskmanage/etltaskoutputinfo")
public class EtlTaskOutputInfoController {

    @Autowired
    private FeignTaskManage feignTaskManage;

    /**
     * 数据输出表节点通用:查询所有数据源
     */
    @GetMapping("/getAllDatasource")
    @LogAnno(operationName = "数据输出表节点通用:查询所有数据源",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse<String> selectAllDatasource(){
        return feignTaskManage.selectAllDatasource();
    }
    /**
     * @param id 数据源id
     */
    @PostMapping("/getDatasource")
    @LogAnno(operationName = "数据输出表节点通用:根据id查询数据源,并展示该数据库的所有表名",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse<String> selectDatasourceById(@RequestParam("id") String id){
        return CommonResponse.ok(feignTaskManage.selectDatasourceById(id));
    }

    /**
     * @param id 数据源id
     * @param tableName 表名
     * @param asName 别名
     * */
    @PostMapping("/getTableField")
    @LogAnno(operationName = "表功能节点:根据数据源id查找数据源信息,根据数据库源与表名查找所有字段信息",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse<String> selectAllFieldByTableName(String id,String tableName,String asName){
        return CommonResponse.ok(feignTaskManage.selectAllFieldByTableName(id, tableName, asName));
    }

    /**
     * 数据输出:根据数据源id查找数据源信息,根据数据源id与表名查找所有字段信息
     * @param id 数据源id
     * @param tableName 表名
     */
    @PostMapping("/getFieldInOutPut")
    @LogAnno(operationName = "数据输出:根据数据源id查找数据源信息,根据数据源id与表名查找所有字段信息",operationType = CommonConstants.OperationType.SELECT)
    public CommonResponse<String> selectAllField(String id,String tableName){
        return CommonResponse.ok(feignTaskManage.selectAllField(id, tableName));
    }

}
