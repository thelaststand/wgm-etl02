package com.wgm.taskmanage.entity;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 任务数据输入表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("etl_task_input_info")
public class EtlTaskInputInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 节点ID
	 */
	private String nodeId;
	/**
	 * 节点名称
	 */
	private String nodeName;
	/**
	 * 任务ID
	 */
	private String taskId;
	/**
	 * 数据库ID
	 */
	private String databaseId;
	/**
	 * 表名称
	 */
	private String tableName;
	/**
	 * 表别名
	 */
	private String tableAsName;
	/**
	 * 表字段
	 */
	private String tableField;
	/**
	 * 表字段别名
	 */
	private String tableAsField;
	/**
	 * 字段规则
	 */
	private String fieldAsEngineId;
	/**
	 * 乐观锁
	 */
	private Integer revision;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新人
	 */
	private String updateBy;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * tombstone 逻辑删除
	 */
	@TableLogic(value = "0",delval = "1")
	private Integer tombstone;

	public EtlTaskInputInfoEntity(JSONObject node, Date now){
		this.nodeId=node.getString("id");
		JSONObject nodeData = node.getJSONObject("data");
		this.id= UUID.randomUUID().toString();
		this.taskId=nodeData.getString("taskId");
		this.databaseId=nodeData.getString("databaseName");
		this.tableName=nodeData.getString("tableName");
		this.tableAsName=nodeData.getString("tableAsName");
		this.tableField=nodeData.getString("tableField");
		this.tableAsField=nodeData.getString("tableAsField");
		this.createTime=this.updateTime=now;
	}

}
