package com.wgm.taskmanage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.taskmanage.entity.EtlTaskOutputInfoEntity;

/**
 * 任务结果输出表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:53
 */
public interface EtlTaskOutputInfoService extends IService<EtlTaskOutputInfoEntity> {


}

