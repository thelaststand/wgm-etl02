package com.wgm.taskmanage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.taskmanage.dao.EtlTaskLineInfoDao;
import com.wgm.taskmanage.entity.EtlTaskLineInfo;
import com.wgm.taskmanage.service.EtlTaskLineInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * (EtlTaskLineInfo)表服务实现类
 *
 * @author makejava
 * @since 2021-01-09 10:27:59
 */
@Service("etlTaskLineInfoService")
@Transactional(rollbackFor = Exception.class)
public class EtlTaskLineInfoServiceImpl extends ServiceImpl<EtlTaskLineInfoDao, EtlTaskLineInfo> implements EtlTaskLineInfoService {

}