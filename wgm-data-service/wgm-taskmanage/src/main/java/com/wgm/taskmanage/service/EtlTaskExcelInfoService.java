package com.wgm.taskmanage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.taskmanage.entity.EtlTaskExcelInfoEntity;

/**
 * 数据引擎excel表头模板表
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
public interface EtlTaskExcelInfoService extends IService<EtlTaskExcelInfoEntity> {

}

