package com.wgm.taskmanage.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author kangbaby
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FieldRequest {
    /**
     * 数据源id
     */
    private String id;
    /**
     * 数据库名称
     */
    private String dbName;
    /**
     * 表名
     */
    private String tableName;
}
