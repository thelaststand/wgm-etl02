package com.wgm.taskmanage.dao;

import com.wgm.taskmanage.entity.EtlTaskLogicFieldInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 任务处理过程表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Mapper
public interface EtlTaskLogicFieldInfoDao extends BaseMapper<EtlTaskLogicFieldInfoEntity> {

    /**
     * 根据id删除任务处理过程
     * @param id
     */
    void deleteEtlLogicFieldInfoById(@Param("id") String id);
}
