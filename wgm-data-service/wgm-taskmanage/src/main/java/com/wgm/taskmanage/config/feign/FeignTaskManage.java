package com.wgm.taskmanage.config.feign;
import com.wgm.common.constant.CommonResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * interface
 *
 * @Author 小脸蛋
 * @CreateDate 2021/1/12
 */
@Component
@FeignClient(name = "wgm-elt-datasource",fallback = FeignTaskManageHystrix.class )
public interface FeignTaskManage {

    /**
     *  获取 对象
     * @param id
     * @return
     */
    @GetMapping("/datasource/manage/{id}")
    public CommonResponse findById(@PathVariable("id")String id);

    /**
     * 数据输出表节点通用:查询所有数据源
     * @return
     */
    @GetMapping("/datasource/manage/getAllDatasource")
    CommonResponse<String> selectAllDatasource();

    /**
     * 数据输出表结点:根据id查询数据源,并展示该数据库的所有表名
     * @param id 数据源id
     * @return
     */
    @PostMapping("/datasource/manage/getDatasource")
    CommonResponse<String> selectDatasourceById(@RequestParam("id")  String id);

    /**
     * 表功能节点:先根据数据源id查找数据源信息,后根据数据库源与表名查找所有字段信息
     * @param id 数据源id
     * @param tableName 表名
     * @param asName 别名
     * @return
     */
    @PostMapping("/datasource/manage/getTableField")
    CommonResponse<String> selectAllFieldByTableName(@RequestParam("id") String id,
                                                      @RequestParam("tableName") String tableName,
                                                      @RequestParam("asName") String asName);

    /**
     * 数据输出功能:先根据数据源id查找数据源信息,后根据数据库源与表名查找所有字段信息
     * @param id 数据源id
     * @param tableName 表名
     * @return
     */
    @PostMapping("/datasource/manage/getFieldInOutPut")
    CommonResponse<String> selectAllField(@RequestParam("id") String id,
                                         @RequestParam("tableName") String tableName);
}

