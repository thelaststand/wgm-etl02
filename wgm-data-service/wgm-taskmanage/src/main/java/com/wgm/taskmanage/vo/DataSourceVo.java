package com.wgm.taskmanage.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author kangbaby
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataSourceVo {

    /**
     * 数据库名
     */
    private String dbName;
}
