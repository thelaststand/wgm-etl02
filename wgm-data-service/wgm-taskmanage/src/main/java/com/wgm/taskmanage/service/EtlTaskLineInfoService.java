package com.wgm.taskmanage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.taskmanage.entity.EtlTaskLineInfo;


/**
 * @author kangbaby
 */
public interface EtlTaskLineInfoService extends IService<EtlTaskLineInfo> {

}