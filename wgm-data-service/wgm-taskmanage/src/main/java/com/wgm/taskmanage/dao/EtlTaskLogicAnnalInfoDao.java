package com.wgm.taskmanage.dao;

import com.wgm.taskmanage.entity.EtlTaskLogicAnnalInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 任务处理过程表
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Mapper
public interface EtlTaskLogicAnnalInfoDao extends BaseMapper<EtlTaskLogicAnnalInfoEntity> {

    /**
     * 根据id删除任务处理过程
     * @param id
     */
    void deleteEtlLogicAnnalInfoById(@Param("id") String id);

    /**
     * 根据id查询任务处理过程
     * @param taskId
     * @return
     */
    List<EtlTaskLogicAnnalInfoEntity> findTaskId(String taskId);
}
