package com.wgm.taskmanage.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.wgm.taskmanage.dao.EtlTaskGroupInfoDao;
import com.wgm.taskmanage.entity.EtlTaskGroupInfoEntity;
import com.wgm.taskmanage.service.EtlTaskGroupInfoService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 任务处理分组service实现类
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-08 19:23:54
 */
@Service("etlTaskGroupInfoService")
@Transactional(rollbackFor = Exception.class)
public class EtlTaskGroupInfoServiceImpl extends ServiceImpl<EtlTaskGroupInfoDao, EtlTaskGroupInfoEntity> implements EtlTaskGroupInfoService {


}