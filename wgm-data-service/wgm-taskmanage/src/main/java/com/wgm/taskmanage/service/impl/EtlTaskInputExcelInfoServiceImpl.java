package com.wgm.taskmanage.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.wgm.taskmanage.dao.EtlTaskInputExcelInfoDao;
import com.wgm.taskmanage.entity.EtlTaskInputExcelInfoEntity;
import com.wgm.taskmanage.service.EtlTaskInputExcelInfoService;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author kangbaby
 */
@Service("etlTaskInputExcelInfoService")
@Transactional(rollbackFor = Exception.class)
public class EtlTaskInputExcelInfoServiceImpl extends ServiceImpl<EtlTaskInputExcelInfoDao, EtlTaskInputExcelInfoEntity> implements EtlTaskInputExcelInfoService {

}