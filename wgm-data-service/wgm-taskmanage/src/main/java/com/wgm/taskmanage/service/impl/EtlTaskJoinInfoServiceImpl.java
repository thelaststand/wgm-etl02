package com.wgm.taskmanage.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.wgm.taskmanage.dao.EtlTaskJoinInfoDao;
import com.wgm.taskmanage.entity.EtlTaskJoinInfoEntity;
import com.wgm.taskmanage.service.EtlTaskJoinInfoService;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author kangbaby
 */
@Service("etlTaskJoinInfoService")
@Transactional(rollbackFor = Exception.class)
public class EtlTaskJoinInfoServiceImpl extends ServiceImpl<EtlTaskJoinInfoDao, EtlTaskJoinInfoEntity> implements EtlTaskJoinInfoService {


}