package com.ruoyi.web.controller.dbmanage;

import com.ruoyi.web.service.dbmanage.DbManageService;
import com.wgm.common.constant.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
*   class
*   @Author 小脸蛋
*   @CreateDate 2021/1/6
*/
@RestController
public class DbManageContoller{

    @Autowired
    DbManageService dbManageService;


    @GetMapping("datasource/manage/{id}")
    public CommonResponse findById(Long id) {
        return dbManageService.findById(id);
    }
}
