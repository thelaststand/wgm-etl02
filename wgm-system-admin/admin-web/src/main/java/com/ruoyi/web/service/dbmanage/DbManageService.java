package com.ruoyi.web.service.dbmanage;

import com.wgm.common.constant.CommonResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * interface
 *
 * @Author 小脸蛋
 * @CreateDate 2021/1/6
 */
@FeignClient("wgm-elt-datasource")
public interface DbManageService {

    /**
     *  获取 对象
     * @param id
     * @return
     */
    @GetMapping("datasource/manage/{id}")
    public CommonResponse findById(@PathVariable("id")Long id);

}
