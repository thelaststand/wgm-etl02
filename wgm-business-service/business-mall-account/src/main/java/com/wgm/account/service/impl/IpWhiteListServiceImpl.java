package com.wgm.account.service.impl;

import com.wgm.account.entity.BusinessCompanyInfoEntity;
import com.wgm.account.entity.MallUserEntity;
import com.wgm.account.service.BusinessCompanyInfoService;
import com.wgm.account.service.MallUserService;
import com.wgm.account.vo.CompanyInfoVo;
import com.wgm.account.vo.IpWhiteListVo;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.exception.AdException;
import com.wgm.common.utils.PageUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.wgm.account.dao.IpWhiteListDao;
import com.wgm.account.entity.IpWhiteListEntity;
import com.wgm.account.service.IpWhiteListService;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 *
 * @author song-huan
 * @email 3325074245@qq.com
 * @date 2021-01-22 10:40:42
 * 后台登录
 */
@Service("ipWhiteListService")
public class IpWhiteListServiceImpl extends ServiceImpl<IpWhiteListDao, IpWhiteListEntity> implements IpWhiteListService {
    @Autowired
    private BusinessCompanyInfoService businessCompanyInfoService;
    @Autowired
    private MallUserService mallUserService;

    /**
     *  Ip白名单列表查询
     * @param pageSize 每页条数
     * @param curPage 当前页
     * @return
     */
    @Override
    public PageUtils getIpList(Integer pageSize, Integer curPage) {
        //1、查询所有可能展示的Ip白名单
        List<IpWhiteListEntity> ipWhiteListEntities = this.baseMapper.selectByPage((curPage-1) * pageSize,pageSize);
        //2、根据白名单信息 获取公司信息
        List<IpWhiteListVo> collect = ipWhiteListEntities.stream().map(item -> {
            //3.获取到当前公司的信息
            BusinessCompanyInfoEntity businessCompanyInfoEntity = businessCompanyInfoService.findById(item.getCompanyId());
            //4.根据当前公司信息获取到用户信息
            MallUserEntity mallUserEntity = mallUserService.findById(businessCompanyInfoEntity.getUserId());
            IpWhiteListVo ipWhiteListVo = new IpWhiteListVo();
            BeanUtils.copyProperties(item, ipWhiteListVo);
            ipWhiteListVo.setCompanyName(businessCompanyInfoEntity.getEnterpriseName());
            ipWhiteListVo.setUserName(mallUserEntity.getName());
            return ipWhiteListVo;
        }).collect(Collectors.toList());
        //3、获取总条数
        Integer count = this.baseMapper.getCount();
        //4、封装分页信息
        return new PageUtils(collect,count,pageSize,curPage);
    }

    /**
     *  添加Ip白名单
     * @param ipWhiteList
     * @return
     */
    @Override
    public Boolean saveIpList(IpWhiteListVo ipWhiteList, HttpSession session) {
        //1.根据创建人的姓名查找到 创建人的信息
        BusinessCompanyInfoEntity businessCompanyInfoEntity = businessCompanyInfoService.findByCompanyName(ipWhiteList.getCompanyName());
        if(businessCompanyInfoEntity == null){
            throw new AdException(CommonConstants.IpWhiteListError.THE_COMPANY_IS_NOT_CERTIFIED);
        }
        //获取当前登录人的名字
        String login = (String) session.getAttribute("adminUser");

        IpWhiteListEntity ipWhiteListEntity = new IpWhiteListEntity();
        ipWhiteListEntity.setCreatBy(login);
        ipWhiteListEntity.setIpSite(ipWhiteList.getIpSite());
        ipWhiteListEntity.setCompanyId(businessCompanyInfoEntity.getId());
        ipWhiteListEntity.setShowStatus(0);
        ipWhiteListEntity.setCreatTime(new Date());
        ipWhiteListEntity.setUpdateTime(new Date());
        ipWhiteListEntity.setRevision(0);
        ipWhiteListEntity.setStatus(0);
        int i = baseMapper.saveIpList(ipWhiteListEntity);
        if(i > 0){

            return true;
        }
        return false;
    }

    /**
     *  后台删除Ip白名单（逻辑删除）
     * @param id
     * @return
     */
    @Override
    public Boolean deleteIpList(Integer id) {
        IpWhiteListEntity entity = baseMapper.selectByIpListId(id);
        if(entity == null){
            throw new AdException(CommonConstants.IpWhiteListError.THE_DATA_DOES_NOT_EXIST);
        }
        int i = baseMapper.deleteIpList(id);
        if(i>0){
            return true;

        }
        return false;
    }



    /**
     *  前台Ip白名单列表
     * @return
     */
    @Override
    public List<IpWhiteListEntity> getIpWebList() {
        return baseMapper.getIpWebList();
    }

    /**
     *  前台删除Ip白名单列表
     * @param id
     * @return
     */
    @Override
    public Boolean deleteIpWebList(Integer id) {
        IpWhiteListEntity entity = baseMapper.selectByIpListId(id);
        if(entity == null){
            throw new AdException(CommonConstants.IpWhiteListError.THE_DATA_DOES_NOT_EXIST);
        }
        int i = baseMapper.deleteIpWebList(id);
        if(i>0){
            return true;
        }
        return false;
    }

    /**
     *  前台添加
     * @param ipSite
     * @return
     */
    @Override
    public Boolean saveIpWebList(String ipSite) {
        System.out.println(ipSite);
        String[] split = ipSite.split(",");
        int i = 0;
        for (String s : split) {
            IpWhiteListEntity entity = new IpWhiteListEntity();
            entity.setIpSite(s);
            entity.setShowStatus(0);
            entity.setRevision(0);
            entity.setUpdateTime(new Date());
            entity.setCreatTime(new Date());
            entity.setStatus(0);
            entity.setCompanyId("");
            i = baseMapper.saveIpWebList(entity);
        }
        if(i>0){
            return true;
        }
        return false;
    }

    /**
     *  前台修改Ip地址
     * @param ipWhiteListEntity
     * @return
     */
    @Override
    public Boolean updateIpWebList(IpWhiteListEntity ipWhiteListEntity) {
        IpWhiteListEntity entity = baseMapper.selectById(ipWhiteListEntity.getId());
        if(entity == null ){
            throw new AdException(CommonConstants.IpWhiteListError.THE_DATA_DOES_NOT_EXIST);
        }
        int i = baseMapper.updateIpWebList(ipWhiteListEntity);
        if(i>0){
            return true;
        }
        return false;
    }

    /**
     *  修改Ip白名单
     * @param ipWhiteListVo
     * @param session
     * @return
     */
    @Override
    public Boolean updateIpList(IpWhiteListVo ipWhiteListVo, HttpSession session) {

        return null;
    }


}