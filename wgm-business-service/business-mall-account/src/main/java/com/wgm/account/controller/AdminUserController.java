package com.wgm.account.controller;


import com.wgm.account.vo.AdminUserVo;
import com.wgm.account.vo.MallUserVo;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.constant.CommonStatus;
import com.wgm.common.valid.AddGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wgm.account.service.AdminUserService;



/**
 * 
 *
 * @author song-huan
 * @email 3325074245@qq.com
 * @date 2021-01-22 10:40:42
 * 后台登录
 */
@RestController
@RequestMapping("account/adminuser")
public class AdminUserController {
    @Autowired
    private AdminUserService adminUserService;


    /**
     *  登录
     * @param adminUser
     * @return
     */
    @PostMapping("/login")
    public CommonResponse login(@RequestBody AdminUserVo adminUser){

        return adminUserService.login(adminUser) ?
                new CommonResponse(CommonStatus.VALID) :
                new CommonResponse(CommonStatus.INVALID);
    }

    /**
     *  register 注册
     * @param adminUser
     * @return Boolean
     */
    @PostMapping("/register")
    public CommonResponse register(@Validated(AddGroup.class) @RequestBody AdminUserVo adminUser){
        return adminUserService.register(adminUser) ?
                new CommonResponse(CommonStatus.VALID) :
                new CommonResponse(CommonStatus.INVALID);
    }
}
