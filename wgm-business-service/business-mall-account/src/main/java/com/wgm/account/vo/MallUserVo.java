package com.wgm.account.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.wgm.common.valid.AddGroup;
import com.wgm.common.valid.UpdateGroup;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

/**
 * @author songhuan
 * @email 3325074245@qq.com
 * @date 2021/01/20
 */
@Data
public class MallUserVo {
    /**
     * 编号
     */
    @TableId
    private String id;
    /**
     * 姓名
     */
    @NotEmpty(message = "用户名不能为空",groups = {AddGroup.class})
    @Length(max = 20,message = "用户名不能大于20个字符",groups = {AddGroup.class})
    private String name;
    /**
     * 手机号
     */
    @NotEmpty(message = "手机号不能为空",groups = {AddGroup.class, UpdateGroup.class})
    @Length(max = 11,min = 11,message = "手机号必须为11位",groups = {AddGroup.class, UpdateGroup.class})
    private String phone;
    /**
     * 密码
     */
    @NotEmpty(message = "密码不能为空",groups = {AddGroup.class, UpdateGroup.class})
    @Length(min = 8,max = 20,message = "密码长度必须大于8位且小于20位",groups = {AddGroup.class, UpdateGroup.class})
    private String password;
    /**
     * 头像
     */
    private String imgUrl;
    /**
     * 职位编号
     */
    private String postId;
    /**
     * 乐观锁
     */
    private Integer revision;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 更新人
     */
    private String updateBy;
    /**
     * 纳税人识别号
     */
    private String taxpayerIdentificationNumbe;
    /**
     * 公司
     */
    private String companyInformationId;
    /**
     * 0展示 1不展示(逻辑删除)
     */
    private Integer showStatus;
    /**
     * 临时字段 验证码
     */
    @TableField(exist=false)
    private String code;
    /**
     * 临时字段 二次密码
     */
    @TableField(exist=false)
    private String passwordTwo;
}
