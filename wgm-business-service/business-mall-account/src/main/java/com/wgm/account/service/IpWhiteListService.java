package com.wgm.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.account.entity.IpWhiteListEntity;
import com.wgm.account.vo.IpWhiteListVo;
import com.wgm.common.utils.PageUtils;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;


/**
 * ip白名单
 *
 * @author song-huan
 * @email 3325074245@qq.com
 * @date 2021-01-22 18:59:00
 */
public interface IpWhiteListService extends IService<IpWhiteListEntity> {
    /**
     *  IP白名单列表
     * @param pageSize 每页条数
     * @param curPage 当前页
     * @return
     */
    PageUtils getIpList(Integer pageSize, Integer curPage);

    /**
     *  添加Ip白名单
     * @param ipWhiteList
     * @param session
     * @return
     */
    Boolean saveIpList(IpWhiteListVo ipWhiteList,HttpSession session);

    /**
     *  后台删除Ip白名单
     * @param id
     * @return
     */
    Boolean deleteIpList(Integer id);



    /**
     *  前台Ip白名单列表
     * @return
     */
    List<IpWhiteListEntity> getIpWebList();

    /**
     *  后台删除Ip白名单
     * @param id
     * @return
     */
    Boolean deleteIpWebList(Integer id);

    /**
     *  前台批量添加Ip地址
     * @param ipSite
     * @return
     */
    Boolean saveIpWebList(String ipSite);



    /**
     *  前台修改Ip地址
     * @param ipWhiteListEntity
     * @return
     */
    Boolean updateIpWebList(IpWhiteListEntity ipWhiteListEntity);

    /**
     *  修改Ip白名单
     * @param ipWhiteListVo
     * @param session
     * @return
     */
    Boolean updateIpList(IpWhiteListVo ipWhiteListVo, HttpSession session);
}

