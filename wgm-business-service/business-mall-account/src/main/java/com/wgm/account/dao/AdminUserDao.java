package com.wgm.account.dao;

import com.wgm.account.entity.AdminUserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.account.vo.AdminUserVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author song-huan
 * @email 3325074245@qq.com
 * @date 2021-01-22 10:40:42
 */
@Mapper
public interface AdminUserDao extends BaseMapper<AdminUserEntity> {

    /**
     *  注册
     * @param adminUser
     * @return
     */
    int insertAdminUser(@Param("adminUser") AdminUserEntity adminUser);

    /**
     * 根据用户名称查询
     * @param name
     * @return
     */
    AdminUserEntity selectByName(String name);
}
