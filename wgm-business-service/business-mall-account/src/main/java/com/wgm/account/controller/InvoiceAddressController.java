package com.wgm.account.controller;

import com.wgm.account.entity.InvoiceAddressEntity;
import com.wgm.account.service.InvoiceAddressService;
import com.wgm.common.constant.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 发票地址管理
 *
 * @Author donghaolin
 * @Date 2021/1/20 20:33
 * @Version 1.0
 */
@RestController
@RequestMapping("invoice/address")
public class InvoiceAddressController {

    @Autowired
    private InvoiceAddressService invoiceAddressService;

    /**
     * 发票地址信息
     *
     * @return
     */
    @GetMapping("/invoiceAddressList")
    public CommonResponse invoiceAddressList(){
        List<Map<String,Object>> list = invoiceAddressService.invoiceAddressList();
        return CommonResponse.ok(list);
    }

    /**
     * 发票地址添加
     * @param invoiceAddressEntity
     * @return
     */
    @PostMapping()
    public CommonResponse add(@RequestBody InvoiceAddressEntity invoiceAddressEntity){
        invoiceAddressService.add(invoiceAddressEntity);
        return CommonResponse.ok();
    }

    /**
     * 地址回显
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public CommonResponse invoiceAddressFindById(@PathVariable("id")String id){
        InvoiceAddressEntity invoiceAddressEntity
                = invoiceAddressService.invoiceAddressFindById(id);
        return CommonResponse.ok(invoiceAddressEntity);
    }

    /**
     * 地址修改
     * @param invoiceAddressEntity
     * @return
     */
    @PostMapping("/update")
    public CommonResponse updateInvoiceAddress(@RequestBody InvoiceAddressEntity invoiceAddressEntity ){
        invoiceAddressService.updateInvoiceAddress(invoiceAddressEntity);
        return CommonResponse.ok();
    }

    /**
     * 逻辑删除地址
     * @param id
     * @return
     */
    @PostMapping("/{id}")
    public CommonResponse deleteInvoiceAddress(@PathVariable("id")String id ){
        invoiceAddressService.deleteInvoiceAddress(id);
        return CommonResponse.ok();
    }



}
