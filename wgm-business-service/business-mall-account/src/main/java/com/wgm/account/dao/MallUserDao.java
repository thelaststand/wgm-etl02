package com.wgm.account.dao;

import com.wgm.account.entity.MallUserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.account.vo.UpdatePhoneVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户表
 * 
 * @author song-huan
 * @email 3325074245@qq.com
 * @date 2021-01-20 10:13:24
 */
@Mapper
public interface MallUserDao extends BaseMapper<MallUserEntity> {

    /**
     *  测试查询
     * @return
     */
    List<MallUserEntity> userList();

    /**
     *  登录
     * @param entity
     * @return
     */
    int register(@Param("entity") MallUserEntity entity);

    /**
     * 根据id查找
     * @param userId
     * @return
     */
    MallUserEntity findById(@Param("userId") String userId);
    /**
     *  根据手机号查找到登录用户
     * @param phone
     * @return
     */
    MallUserEntity selectByPhone(String phone);

    /**
     *  忘记密码 修改
     * @param entity
     * @return
     */
    int updateByMallUser(@Param("entity") MallUserEntity entity);

    /**
     * 修改手机号
     * @param updatePhoneVo
     */
    void updatePhone(@Param("updatePhoneVo") UpdatePhoneVo updatePhoneVo);
}
