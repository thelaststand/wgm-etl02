package com.wgm.account.controller;


import com.wgm.account.entity.UserWarningEntity;
import com.wgm.account.service.UserWarningService;
import com.wgm.account.vo.WarningVo;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.valid.AddGroup;
import com.wgm.common.valid.DeleteGroup;
import com.wgm.common.valid.UpdateGroup;
import com.wgm.common.valid.UpdateStatusGroup;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 用户预警号码
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-21 19:39:57
 */
@RestController
@RequestMapping("user/warning")
@Slf4j
public class UserWarningController {

    @Autowired
    private UserWarningService userWarningService;


    /**
     * 添加预警手机号
     * @param warningVo
     * @return
     */
    @PostMapping("/save")
    public CommonResponse saveUserWarning(@Validated(AddGroup.class) @RequestBody WarningVo warningVo){
        log.info("UserWarningController:saveUserWarning->{}",warningVo);
        return userWarningService.saveUserWarning(warningVo)?CommonResponse.ok():CommonResponse.error();
    }

    /**
     * 删除预警手机号
     * @param warningVo
     * @return
     */
    @DeleteMapping("/del")
    public CommonResponse delUserWarning(@Validated(DeleteGroup.class) @RequestBody WarningVo warningVo){
        log.info("UserWarningController:delUserWarning->{}",warningVo);
        return userWarningService.delUserWarning(warningVo)?CommonResponse.ok():CommonResponse.error();
    }

    /**
     * 修改预警手机号
     * @param warningVo
     * @return
     */
    @PutMapping("/change")
    public CommonResponse updateUserWarning(@Validated(UpdateGroup.class) @RequestBody WarningVo warningVo){
        log.info("UserWarningController:updateUserWarning->{}",warningVo);
        return userWarningService.updateUserWarning(warningVo)?CommonResponse.ok():CommonResponse.error();
    }

    /**
     * 修改预警手机号状态状态
     * @param warningVo
     * @return
     */
    @PutMapping("/change/status")
    public CommonResponse updateUserWarningStatus(@Validated(UpdateStatusGroup.class) @RequestBody WarningVo warningVo){
        log.info("UserWarningController:updateUserWarningStatus->{}",warningVo);
        return userWarningService.updateUserWarningStatus(warningVo)?CommonResponse.ok():CommonResponse.error();
    }


    /**
     * 查询个人所有预警号码
     * @param uid
     * @return
     */
    @GetMapping("/list/{uid}")
    public CommonResponse getUserWarningListByUid(@PathVariable("uid")String uid){
        log.info("UserWarningController:getUserWarningListByUid->{}",uid);
        List<UserWarningEntity> list = userWarningService.getUserWarningListByUid(uid);
        return CommonResponse.ok(list);
    }





}
