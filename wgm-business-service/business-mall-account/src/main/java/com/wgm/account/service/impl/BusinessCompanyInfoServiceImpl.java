package com.wgm.account.service.impl;

import com.wgm.account.dao.BusinessCompanyInfoDao;
import com.wgm.account.entity.BusinessCompanyInfoEntity;
import com.wgm.account.entity.MallUserEntity;
import com.wgm.account.service.BusinessCompanyInfoService;
import com.wgm.account.service.MallUserService;
import com.wgm.account.vo.CompanyInfoVo;
import com.wgm.common.constant.CommonStatus;
import com.wgm.common.utils.DateUtils;
import com.wgm.common.utils.PageUtils;
import com.wgm.common.utils.UuidUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
/**
 * 商业公司信息service实现类

 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-20 11:15:03
 */
@Service("businessCompanyInfoService")
@Transactional(rollbackFor = Exception.class)
public class BusinessCompanyInfoServiceImpl extends ServiceImpl<BusinessCompanyInfoDao, BusinessCompanyInfoEntity> implements BusinessCompanyInfoService {

    @Autowired
    private BusinessCompanyInfoDao companyInfoDao;

    @Autowired
    private MallUserService userService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveInfo(CompanyInfoVo companyInfoVo) {
        //属性对拷
        BusinessCompanyInfoEntity companyInfoEntity = new BusinessCompanyInfoEntity();
        BeanUtils.copyProperties(companyInfoVo,companyInfoEntity);
        //uuid
        companyInfoEntity.setId(UuidUtils.generateUuid());
        //创建时间
        companyInfoEntity.setCreateTime(DateUtils.getNowDate());
        //修改时间
        companyInfoEntity.setUpdateTime(DateUtils.getNowDate());
        //默认状态为0 待审核
        companyInfoEntity.setState(CommonStatus.WAIT_CHECK.getCode());
        //默认展示
        companyInfoEntity.setShowDesc(CommonStatus.DATA_DISPLAY.getCode());
        this.baseMapper.save(companyInfoEntity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer examinationCompany(CompanyInfoVo companyInfoVo) {
        //1、属性对拷
        BusinessCompanyInfoEntity companyInfoEntity = new BusinessCompanyInfoEntity();
        BeanUtils.copyProperties(companyInfoVo,companyInfoEntity);
        //2、修改时间
        companyInfoEntity.setUpdateTime(DateUtils.getNowDate());
        //3、根据id修改状态
        Integer i = this.baseMapper.updateCompanyInfoById(companyInfoEntity);
        return i;
    }

    @Override
    public PageUtils getCompanyInfoList(Integer pageSize, Integer curPage,Integer state) {
        //1、查询所有可展示的公司信息
        List<BusinessCompanyInfoEntity> companyInfoEntities = this.baseMapper.selectByPage((curPage-1)*pageSize,pageSize,state);
        //2、根据公司信息 获取 申请人信息 返回集合
        List<CompanyInfoVo> collect = companyInfoEntities.stream().map(item -> {
            MallUserEntity userEntity = userService.findById(item.getUserId());
            CompanyInfoVo infoVo = new CompanyInfoVo();
            BeanUtils.copyProperties(item, infoVo);
            infoVo.setUsername(userEntity.getName());
            infoVo.setPhone(userEntity.getPhone());
            return infoVo;
        }).collect(Collectors.toList());
         //3、获取总条数
        Integer count = this.baseMapper.getCount();
         //4、封装分页信息
        return new PageUtils(collect,count,pageSize,curPage);
    }

    @Override
    public BusinessCompanyInfoEntity findByUid(String id) {
        return  this.baseMapper.findByUid(id);

    }

    @Override
    public BusinessCompanyInfoEntity findById(String companyId) {
        return this.baseMapper.findById(companyId);
    }

    @Override
    public BusinessCompanyInfoEntity findByCompanyName(String companyName) {
        return this.baseMapper.findByCompanyName(companyName);
    }

    /**
     * 查询对应公司信息
     * @param id
     * @return
     */
    @Override
    public BusinessCompanyInfoEntity findCompanyById(String id) {
        return companyInfoDao.findCompanyById(id);
    }

}