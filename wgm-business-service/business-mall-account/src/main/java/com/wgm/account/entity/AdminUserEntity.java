package com.wgm.account.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author song-huan
 * @email 3325074245@qq.com
 * @date 2021-01-22 10:40:42
 */
@Data
@TableName("admin_user")
public class AdminUserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 是否是管理员 1是0否
	 */
	private Integer isAdmin;
	/**
	 * 状态 1正常 0不正常
	 */
	private Integer status;
	/**
	 * 乐观锁
	 */
	private Integer revision;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新人
	 */
	private String updateBy;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 0展示 1不展示(逻辑删除)
	 */
	private Integer showStatus;

}
