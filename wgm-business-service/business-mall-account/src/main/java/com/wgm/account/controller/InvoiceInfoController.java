package com.wgm.account.controller;

import com.wgm.account.entity.InvoiceInfoEntity;
import com.wgm.account.service.InvoiceInfoService;
import com.wgm.account.vo.UpdateInvoiceInfoVo;
import com.wgm.common.constant.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 *
 * 发票信息管理
 *
 * @Author donghaolin
 * @Date 2021/1/20 11:36
 * @Version 1.0
 */
@RestController
@RequestMapping("invoice/info")
public class InvoiceInfoController {

    @Autowired
    private InvoiceInfoService invoiceInfoService;


    /**
     * 发票信息管理列表
     * @param
     * @return
     */
    @GetMapping("showDataBase")
    public CommonResponse invoiceInfoList(){
        List<Map<String,Object>> list = invoiceInfoService.invoiceInfoList();
        return CommonResponse.ok(list);
    }

    /**
     * 发票信息添加
     * @param invoiceInfoEntity
     * @return
     */
    @PostMapping()
    public CommonResponse add(@RequestBody InvoiceInfoEntity invoiceInfoEntity){
        invoiceInfoService.add(invoiceInfoEntity);
        return CommonResponse.ok();
    }

    /**
     * 当前类的回显
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public CommonResponse invoiceInfoFindById(@PathVariable("id")String id){
        InvoiceInfoEntity invoiceInfoEntity = invoiceInfoService.invoiceInfoFindById(id);
        return CommonResponse.ok(invoiceInfoEntity);
    }

    /**
     * 修改发票信息
     * @param updateInvoiceInfoVo
     * @return
     */
    @PutMapping()
    public CommonResponse updateInvoiceInfo(@RequestBody UpdateInvoiceInfoVo updateInvoiceInfoVo){
        invoiceInfoService.updateInvoiceInfo(updateInvoiceInfoVo);
        return CommonResponse.ok();
    }

    /**
     * 发票信息逻辑删除
     * @param id
     * @return
     */
    @PostMapping("/delete/{id}")
    public CommonResponse deleteInvoiceInfo(@PathVariable("id")String id){
        invoiceInfoService.deleteInvoiceInfo(id);
        return CommonResponse.ok();
    }



}
