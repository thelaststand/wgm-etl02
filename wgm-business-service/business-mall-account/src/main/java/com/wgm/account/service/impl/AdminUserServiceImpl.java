package com.wgm.account.service.impl;

import com.wgm.account.vo.AdminUserVo;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.exception.AdException;
import com.wgm.common.utils.Md5Utils;
import com.wgm.common.utils.UuidUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


import com.wgm.account.dao.AdminUserDao;
import com.wgm.account.entity.AdminUserEntity;
import com.wgm.account.service.AdminUserService;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 用户表
 *
 * @author song-huan
 * @email 3325074245@qq.com
 * @date 2021-01-20 10:13:24
 */
@Service("adminUserService")
@Transactional(rollbackFor = Exception.class)
public class AdminUserServiceImpl extends ServiceImpl<AdminUserDao, AdminUserEntity> implements AdminUserService {


    @Value("${business-md5-salt}")
    private String salt;
    /**
     *  登录
     * @param adminUser
     * @return
     */
    @Override
    public Boolean login(AdminUserVo adminUser) {
        //调用 MD5工具类  为密码加密
        Md5Utils md5Utils = new Md5Utils();
        String password = md5Utils.md5(md5Utils.md5(adminUser.getPassword()+md5Utils.md5(salt)));
        adminUser.setPassword(password);

        //根据手机号判断对象是否存在
        AdminUserEntity entity = baseMapper.selectByName(adminUser.getName());

        if(entity==null){
            throw new AdException(CommonConstants.RegisteredAndLoginError.THE_USER_IS_NOT_REGISTERED);
        }

        //判断用户名和密码是否正确
        if( adminUser.getName().equals(entity.getName()) && password.equals(entity.getPassword())){
            return true;
        }
        throw new AdException(CommonConstants.RegisteredAndLoginError.INCORRECT_PASSWORD_OR_USERNAME);

    }

    /**
     *  注册
     * @param adminUser
     * @return
     */
    @Override
    public Boolean register(AdminUserVo adminUser) {
        //先判断数据库是否有这个用户
        AdminUserEntity adminUserEntity = baseMapper.selectByName(adminUser.getName());
        if(adminUserEntity!=null){
            throw new AdException(CommonConstants.RegisteredAndLoginError.USER_ALREADY_EXISTS);
        }
        //调用 MD5工具类  为密码加密
        Md5Utils md5Utils = new Md5Utils();
        String password = md5Utils.md5(md5Utils.md5(adminUser.getPassword()+md5Utils.md5(salt)));
        //默认值
        adminUser.setId(UuidUtils.generateUuid());
        adminUser.setPassword(password);
        adminUser.setIsAdmin(1);
        adminUser.setStatus(1);
        adminUser.setRevision(0);
        adminUser.setCreateTime(new Date());
        adminUser.setUpdateTime(new Date());
        adminUser.setShowStatus(0);

        AdminUserEntity entity = new AdminUserEntity();
        //属性对拷
        BeanUtils.copyProperties(adminUser,entity);

        int i = baseMapper.insertAdminUser(entity);
        if(i>0){
            return true;
        }
        return false;
    }
}