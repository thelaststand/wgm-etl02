package com.wgm.account.service;

import com.wgm.account.entity.InvoiceAddressEntity;

import java.util.List;
import java.util.Map;

/**
 * 发票地址管理
 *
 * @Author donghaolin
 * @Date 2021/1/20 20:42
 * @Version 1.0
 */
public interface InvoiceAddressService {

    /**
     * 发票信息管理
     * @return
     */
    List<Map<String, Object>> invoiceAddressList();

    /**
     * 发票地址添加
     * @param invoiceAddressEntity
     */
    void add(InvoiceAddressEntity invoiceAddressEntity);

    /**
     * 发票地址回显
     * @param id
     * @return
     */
    InvoiceAddressEntity invoiceAddressFindById(String id);

    /**
     * 发票地址修改
     * @param invoiceAddressEntity
     */
    void updateInvoiceAddress(InvoiceAddressEntity invoiceAddressEntity);

    /**
     * 逻辑删除地址
     * @param id
     */
    void deleteInvoiceAddress(String id);
}
