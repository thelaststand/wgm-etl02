package com.wgm.account.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.beans.Transient;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 用户表
 * 
 * @author song-huan
 * @email 3325074245@qq.com
 * @date 2021-01-20 10:13:24
 */
@Data
@TableName("mall_user")
public class MallUserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 编号
	 */
	@TableId
	private String id;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 手机号
	 */
	private String phone;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 头像
	 */
	private String imgUrl;
	/**
	 * 职位编号
	 */
	private String postId;
	/**
	 * 乐观锁
	 */
	private Integer revision;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 更新人
	 */
	private String updateBy;
	/**
	 * 纳税人识别号
	 */
	private String taxpayerIdentificationNumbe;
	/**
	 * 公司
	 */
	private String companyInformationId;
	/**
	 * 0展示 1不展示(逻辑删除)
	 */
	private Integer showStatus;


}
