package com.wgm.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.account.entity.SysRegionEntity;
import com.wgm.account.vo.AddressVo;
import com.wgm.account.vo.CompanyInfoVo;

import java.util.List;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/20
 */
public interface SysRegionService extends IService<SysRegionEntity> {

    /**
     * 获取地址列表
     * @return
     */
    List<AddressVo> getAddressList();

}