package com.wgm.account.vo;

import com.wgm.common.valid.AddGroup;
import com.wgm.common.valid.UpdateGroup;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Null;

/**
 * 企业认证vo
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/20
 */
@Data
public class CompanyInfoVo {

    @Null(message = "新增不能指定id",groups = {AddGroup.class})
    @NotEmpty(message = "修改必须指定id",groups = {UpdateGroup.class})
    private String id;
    /**
     * 申请人id
     */
    @NotEmpty(message = "认证人id不能为空",groups = {AddGroup.class,UpdateGroup.class})
    private String userId;
    /**
     * 申请人姓名
     */
    private String username;
    /**
     *申请人手机号
     */
    private String phone;
    /**
     * 申请人头像
     */
    private String imgUrl;
    /**
     * 企业名称
     */
    @NotEmpty(message = "企业名称不能为空",groups = {AddGroup.class,UpdateGroup.class})
    @Length(max = 50,message = "企业名称不能大于50个字符",groups = {AddGroup.class,UpdateGroup.class})
    private String enterpriseName;
    /**
     * 公司法人
     */
    @NotEmpty(message = "公司法人不能为空",groups = {AddGroup.class,UpdateGroup.class})
    @Length(max = 20,message = "公司法人不能大于20个字符",groups = {AddGroup.class,UpdateGroup.class})
    private String legalPerson;

    /**
     * 法人手机号
     */
    @NotEmpty(message = "法人手机号不能为空",groups = {AddGroup.class,UpdateGroup.class})
    @Length(min = 11,max = 11,message = "手机号必须为11位",groups = {AddGroup.class,UpdateGroup.class})
    private String legalPersonPhone;

    /**
     * 职位
     */
    @NotEmpty(message = "职位不能为空",groups = {AddGroup.class,UpdateGroup.class})
    @Length(max = 20,message = "职位不能大于20个字符",groups = {AddGroup.class,UpdateGroup.class})
    private String position;

    /**
     * 公司所在省市区
     */
    @NotEmpty(message = "公司所在省市区不能为空",groups = {AddGroup.class,UpdateGroup.class})
    private String provinceCity;
    /**
     * 详细地址
     */
    @NotEmpty(message = "公司详细地址不能为空",groups = {AddGroup.class,UpdateGroup.class})
    @Length(max = 100,message = "详细地址不能大于100个字符",groups = {AddGroup.class,UpdateGroup.class})
    private String detailedAddress;
    /**
     * 信用编码
     */
    @NotEmpty(message = "信用编号不能为空",groups = {AddGroup.class, UpdateGroup.class})
    @Length(min = 18,max = 18,message = "信用编码必须为18位",groups = {AddGroup.class,UpdateGroup.class})
    private String creditCode;

    /**
     * 营业执照
     */
    @NotEmpty(message = "营业执照不能为空",groups = {AddGroup.class,UpdateGroup.class})
    private String businessLicense;

    /**
     * 审核状态：0-待审核 1-审核通过 2-审核不通过
     */
    private Integer state;

}
