package com.wgm.account.dao;

import com.wgm.account.entity.MallLoginLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 登录日志
 * 
 * @author donghaolin
 * @email 2326818551@qq.com
 * @date 2021-01-25 10:48:08
 */
@Mapper
public interface MallLoginLogDao extends BaseMapper<MallLoginLogEntity> {

    /**
     * 登录日志列表
     * @return
     */
    List<MallLoginLogEntity> selectList();

    /**
     * 重置日志
     */
    void reset();

}
