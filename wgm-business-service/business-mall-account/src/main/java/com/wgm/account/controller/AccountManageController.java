package com.wgm.account.controller;


import com.wgm.account.service.BusinessCompanyInfoService;
import com.wgm.account.service.MallUserService;
import com.wgm.account.service.SysRegionService;
import com.wgm.account.vo.AddressVo;
import com.wgm.account.vo.CompanyInfoVo;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.valid.AddGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 账户管理
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/20
 */
@RestController
@RequestMapping("/account/manage")
public class AccountManageController {

    @Autowired
    private SysRegionService sysRegionService;

    @Autowired
    private MallUserService mallUserService;

    @Autowired
    private BusinessCompanyInfoService businessCompanyInfoService;


    /**
     * 获取地区
     * @return
     */
    @GetMapping("/address/list")
    public CommonResponse getAddressList(){
        List<AddressVo> vos = sysRegionService.getAddressList();
        return CommonResponse.ok(vos);
    }

    /**
     * 获取个人企业信息
     * @param id
     * @return
     */
    @GetMapping("/company/{id}/info")
    public CommonResponse getCompanyInfoByUserId(@PathVariable("id")String id){
        CompanyInfoVo vo = mallUserService.getCompanyInfoByUserId(id);
        return CommonResponse.ok(vo);
    }

    /**
     * 企业认证（添加）
     * @param companyInfoVo
     * @return
     */
    @PostMapping("/save")
    public CommonResponse saveCompanyInfo(@Validated(AddGroup.class)  @RequestBody CompanyInfoVo companyInfoVo){
        businessCompanyInfoService.saveInfo(companyInfoVo);
        return CommonResponse.ok();
    }




}
