package com.wgm.account.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.account.entity.BusinessCompanyInfoEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商业公司信息

 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-20 11:15:03
 */
@Mapper
public interface BusinessCompanyInfoDao extends BaseMapper<BusinessCompanyInfoEntity> {

    /**
     * 添加
     * @param companyInfoEntity
     */
   void save(BusinessCompanyInfoEntity companyInfoEntity);

    /**
     * 根据id修改
     * @param companyInfoEntity
     * @return
     */
   Integer updateCompanyInfoById(BusinessCompanyInfoEntity companyInfoEntity);

    /**
     * 查询所有
     * @return
     */
   List<BusinessCompanyInfoEntity> selectAll();

    /**
     * 分页查询+审核状态
     * @param curPage
     * @param pageSize
     * @param state
     * @return
     */
   List<BusinessCompanyInfoEntity> selectByPage(@Param("curPage") Integer curPage, @Param("pageSize") Integer pageSize, @Param("state") Integer state);

    /**
     * 获取总条数
     * @return
     */
    Integer getCount();

    /**
     * 查询BusinessCompanyInfo对象
     * @param id
     * @return
     */
    BusinessCompanyInfoEntity findByUid(@Param("id") String id);

    /**
     * 根据id查询
     * @param companyId
     * @return
     */
    BusinessCompanyInfoEntity findById(@Param("companyId") String companyId);

    /**
     * 根据公司信息查询
     * @param companyName
     * @return
     */
    BusinessCompanyInfoEntity findByCompanyName(@Param("companyName") String companyName);

    /**
     * 查询公司信息
     * @param id
     * @return
     */
    BusinessCompanyInfoEntity findCompanyById(String id);
}
