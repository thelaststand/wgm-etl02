package com.wgm.account.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.account.entity.UserWarningEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-21 19:39:57
 */
@Mapper
public interface UserWarningDao extends BaseMapper<UserWarningEntity> {

    /**
     * 添加
     * @param warningEntity
     * @return
     */
    Integer saveUserWarning(UserWarningEntity warningEntity);

    /**
     * 根据用户id和预警手机号查询数量
     * @param uid
     * @param warningPhone
     * @return
     */
    Integer getCountByUidAndPhone(@Param("uid") String uid, @Param("warningPhone") String warningPhone);

    /**
     * 根据id删除
     * @param warningCode
     * @return
     */
    Integer delUserWarning(@Param("warningCode") String warningCode);

    /**
     * 修改预警人名称及手机号
     * @param warningName
     * @param warningPhone
     * @param warningCode
     * @return
     */
    Integer updateUserWarning(@Param("warningName") String warningName, @Param("warningPhone") String warningPhone, @Param("warningCode") String warningCode);

    /**
     * 修改预警手机号状态
     * @param warningCode
     * @param enabledState
     * @return
     */
    Integer updateUserWarnixngStatus(@Param("warningCode") String warningCode, @Param("enabledState") Integer enabledState);

    /**
     * 查询用户所有预警号码
     * @param uid
     * @return
     */
    List<UserWarningEntity> getUserWarningListByUid(@Param("uid") String uid);
}
