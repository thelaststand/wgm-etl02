package com.wgm.account.service.impl;

import com.wgm.account.dao.MallUserDao;
import com.wgm.account.entity.BusinessCompanyInfoEntity;
import com.wgm.account.service.BusinessCompanyInfoService;
import com.wgm.account.vo.CompanyInfoVo;
import com.wgm.account.vo.MallUserVo;
import com.wgm.account.vo.UpdatePhoneVo;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.exception.AdException;
import com.wgm.common.exception.TaskException;
import com.wgm.common.utils.Md5Utils;
import com.wgm.common.utils.PhoneUtils;
import com.wgm.common.utils.UuidUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


import com.wgm.account.entity.MallUserEntity;
import com.wgm.account.service.MallUserService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 *
 * @Author donghaolin
 * @Date 2021/1/20 11:45
 * @Version 1.0
 */
@Service("mallUserService")
@Transactional(rollbackFor = Exception.class)
public class MallUserServiceImpl extends ServiceImpl<MallUserDao, MallUserEntity> implements MallUserService {
    @Autowired
    private MallUserDao mallUserDao;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private BusinessCompanyInfoService companyInfoService;

    @Value("${business-md5-salt}")
    private String salt;

    /**
     *  测试查询
     * @return
     */
    @Override
    public List<MallUserEntity> userList() {
        return mallUserDao.userList();
    }


    /**
     *  注册
     * @param mallUser
     * @return
     */
    @Override
    public Boolean register(MallUserVo mallUser) {
        //调用工具类 判断手机号格式是否正确
        boolean phoneLegal = PhoneUtils.isPhoneLegal(mallUser.getPhone());

        if(!phoneLegal){
            throw new AdException(CommonConstants.RegisteredAndLoginError.PHONE_NUMBER_ERROR);
        }

        //获取redis存的验证码
        String code = (String) redisTemplate.boundValueOps(mallUser.getPhone()).get();

        //判断输入的验证码是否和 redis取出的验证码是否一致 如果不一直返回false
        if(StringUtils.isEmpty(code) && !code.equals(mallUser.getCode())){
            throw new AdException(CommonConstants.RegisteredAndLoginError.INCORRECT_VERIFICATION_CODE);
        }

        //调用 MD5工具类  为密码加密
        Md5Utils md5Utils = new Md5Utils();
        String password = md5Utils.md5(md5Utils.md5(mallUser.getPassword()+md5Utils.md5(salt)));

        //如果redis里有值 并且和输入的验证码一致 则注册成功
        mallUser.setId(UuidUtils.generateUuid());
        mallUser.setCreateTime(new Date());
        mallUser.setUpdateTime(new Date());
        mallUser.setPassword(password);
        mallUser.setRevision(0);
        mallUser.setShowStatus(0);

        MallUserEntity entity = new MallUserEntity();
        BeanUtils.copyProperties(mallUser,entity);
        int i = baseMapper.register(entity);
        if(i>0){
            redisTemplate.delete(mallUser.getPhone());
            return true;
        }
        return false;
    }


    /**
     *  登录
     * @param mallUser user对象
     * @return Boolean
     */
    @Override
    public Boolean login(MallUserVo mallUser) {
        //判断手机号格式是否正确
        boolean phoneLegal = PhoneUtils.isPhoneLegal(mallUser.getPhone());

        if(!phoneLegal){
            throw new AdException(CommonConstants.RegisteredAndLoginError.PHONE_NUMBER_ERROR);
        }


        //获取redis存的验证码
        String code = (String) redisTemplate.boundValueOps(mallUser.getPhone()).get();

        //判断输入的验证码是否和 redis取出的验证码是否一致 如果不一直返回false
        if(StringUtils.isEmpty(code) && !code.equals(mallUser.getCode())){
            throw new AdException(CommonConstants.RegisteredAndLoginError.INCORRECT_VERIFICATION_CODE);
        }

        //根据手机号判断对象是否存在
        MallUserEntity entityTwo = baseMapper.selectByPhone(mallUser.getPhone());
        //判断是否找到这条对象
        if(entityTwo == null){
            throw new AdException(CommonConstants.RegisteredAndLoginError.PHONE_NUMBER_ERROR);
        }

        //调用 MD5工具类  为密码加密
        Md5Utils md5Utils = new Md5Utils();
        String password = md5Utils.md5(md5Utils.md5(mallUser.getPassword()+md5Utils.md5(salt)));

        //判断手机号是否正确
        if(!mallUser.getPhone().equals(entityTwo.getPhone())){
            throw new AdException(CommonConstants.RegisteredAndLoginError.PHONE_NUMBER_ERROR);
        }

        //判断密码是否正确
        if(!password.equals(entityTwo.getPassword())){
            throw new AdException(CommonConstants.RegisteredAndLoginError.INCORRECT_PASSWORD_ERROR);

        }
        redisTemplate.delete(mallUser.getPhone());
        return true;
    }
    @Override
    public MallUserEntity findById(String userId) {
        return mallUserDao.findById(userId);
    }

    @Override
    public CompanyInfoVo getCompanyInfoByUserId(String id) {
        //1、根据id查到用户信息
        MallUserEntity userEntity = mallUserDao.findById(id);
        //2、查询对应的企业信息
        BusinessCompanyInfoEntity companyInfoEntity = companyInfoService.findByUid(id);
        //3、属性对拷
        CompanyInfoVo infoVo = new CompanyInfoVo();
        BeanUtils.copyProperties(companyInfoEntity,infoVo);
        infoVo.setUsername(userEntity.getName());
        infoVo.setPhone(userEntity.getPhone());
        infoVo.setImgUrl(userEntity.getImgUrl());
        return infoVo;
    }

    /**
     * 修改手机号
     * @param updatePhoneVo
     */
    @Override
    public void updatePhone(UpdatePhoneVo updatePhoneVo) {
        //请求参数错误
        if (updatePhoneVo==null){
            throw new TaskException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //用户未登录
        if (updatePhoneVo.getUid().equals(null)){
            throw new AdException(CommonConstants.RegisteredAndLoginError.NOT_LOGGED_ERROR);
        }
        //判断旧手机验证码
        MallUserEntity userEntity = mallUserDao.findById(updatePhoneVo.getUid());
        String code = (String) redisTemplate.boundValueOps(userEntity.getPhone()).get();
        if (!code.equals(updatePhoneVo.getCode())){
            throw new AdException(CommonConstants.RegisteredAndLoginError.INCORRECT_VERIFICATION_CODE);
        }
        //判断新手机号
        boolean flag = false;
        List<MallUserEntity> mallUserEntities = mallUserDao.userList();
        for (MallUserEntity mallUserEntity : mallUserEntities) {
            if (mallUserEntity.getPhone().equals(updatePhoneVo.getNewPhone())){
                flag = true;
                break;
            }
        }
        if (false){
            throw new AdException(CommonConstants.RegisteredAndLoginError.PHONE_EXISTENCE_ERROR);
        }
        //redis中取出验证码
        String newCode = redisTemplate.boundValueOps(updatePhoneVo.getNewPhone()).get().toString();
        if (!newCode.equals(updatePhoneVo.getNewCode())){
            throw new AdException(CommonConstants.RegisteredAndLoginError.INCORRECT_VERIFICATION_CODE);
        }
        updatePhoneVo.setUpdateTime(new Date());
        mallUserDao.updatePhone(updatePhoneVo);

    }

    /**
     *  忘记密码
     * @param mallUser
     * @return
     */
    @Override
    public Boolean forgetPassword(MallUserVo mallUser) {
        //判断手机号格式是否正确
        boolean phoneLegal = PhoneUtils.isPhoneLegal(mallUser.getPhone());

        if(!phoneLegal){
            throw new AdException(CommonConstants.RegisteredAndLoginError.PHONE_NUMBER_ERROR);
        }

        //获取redis存的验证码
        String code = (String) redisTemplate.boundValueOps(mallUser.getPhone()).get();

        //判断输入的验证码是否和 redis取出的验证码是否一致 如果不一直返回false
        if(StringUtils.isEmpty(code) && !code.equals(mallUser.getCode())){
            throw new AdException(CommonConstants.RegisteredAndLoginError.INCORRECT_VERIFICATION_CODE);
        }

        //根据手机号判断对象是否存在
        MallUserEntity entityTwo = baseMapper.selectByPhone(mallUser.getPhone());
        //判断是否找到这条对象
        if(entityTwo == null){
            throw new AdException(CommonConstants.RegisteredAndLoginError.PHONE_NUMBER_ERROR);
        }

        //调用 MD5工具类  为密码加密
        Md5Utils md5Utils = new Md5Utils();

        String password = md5Utils.md5(md5Utils.md5(mallUser.getPassword()+md5Utils.md5(salt)));

        String passwordTwo = md5Utils.md5(md5Utils.md5(mallUser.getPasswordTwo()+md5Utils.md5(salt)));

        //判断手机号是否正确
        if(!mallUser.getPhone().equals(entityTwo.getPhone())){
            throw new AdException(CommonConstants.RegisteredAndLoginError.PHONE_NUMBER_ERROR);
        }
        //判断两次密码是否一致 如果不一致抛出异常
        if(!password.equals(passwordTwo)){
            throw new AdException(CommonConstants.RegisteredAndLoginError.TWO_PASSWORDS_ARE_INCONSISTENT);
        }
        entityTwo.setPassword(password);
        entityTwo.setUpdateTime(new Date());
        int i = baseMapper.updateByMallUser(entityTwo);
        //清空redis缓存
        redisTemplate.delete(mallUser.getPhone());
        return true;
    }


}