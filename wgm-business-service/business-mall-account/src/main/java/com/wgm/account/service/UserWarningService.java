package com.wgm.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.account.entity.UserWarningEntity;
import com.wgm.account.vo.WarningVo;

import java.util.List;


/**
 * 
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-21 19:39:57
 */
public interface UserWarningService extends IService<UserWarningEntity> {
    /**
     * 添加预警手机号
     * @param warningVo
     * @return
     */
    Boolean saveUserWarning(WarningVo warningVo);
    /**
     * 删除预警手机号
     * @param warningVo
     * @return
     */
    Boolean delUserWarning(WarningVo warningVo);
    /**
     * 修改预警手机号
     * @param warningVo
     * @return
     */
    Boolean updateUserWarning(WarningVo warningVo);
    /**
     * 修改预警手机号状态状态
     * @param warningVo
     * @return
     */
    Boolean updateUserWarningStatus(WarningVo warningVo);
    /**
     * 查询个人所有预警号码
     * @param uid
     * @return
     */
    List<UserWarningEntity> getUserWarningListByUid(String uid);
}

