package com.wgm.account.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.account.entity.SysRegionEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 地区表
 *
 * @author wudongrun
 * @email 3325074245@qq.com
 * @date 2021-01-20 10:13:24
 */
@Mapper
public interface SysRegionDao extends BaseMapper<SysRegionEntity> {
    /**
     * 根据父级id查询
     * @param id
     * @return
     */
    List<SysRegionEntity> findByPid(@Param("id") String id);
}
