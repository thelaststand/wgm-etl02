package com.wgm.account.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.wgm.common.valid.AddGroup;
import com.wgm.common.valid.UpdateGroup;
import lombok.Data;
import lombok.Value;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author songhuan
 * @email 3325074245@qq.com
 * @date 2021/01/22
 */
@Data
public class AdminUserVo {
    /**
     * id
     */
    @TableId
    private String id;
    /**
     * 姓名
     */
    @NotEmpty(message = "用户名不能为空",groups = {AddGroup.class})
    @Length(max = 20,message = "用户名不能大于20个字符",groups = {AddGroup.class})
    private String name;
    /**
     * 密码
     */
    @NotEmpty(message = "密码不能为空",groups = {AddGroup.class})
    @Length(min = 8,max = 20,message = "密码长度必须大于8位且小于20位",groups = {AddGroup.class, UpdateGroup.class})
    private String password;
    /**
     * 是否是管理员 1是0否
     */
    private Integer isAdmin;
    /**
     * 状态 1正常 0不正常
     */
    private Integer status;
    /**
     * 乐观锁
     */
    private Integer revision;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新人
     */
    private String updateBy;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 0展示 1不展示(逻辑删除)
     */
    private Integer showStatus;
}
