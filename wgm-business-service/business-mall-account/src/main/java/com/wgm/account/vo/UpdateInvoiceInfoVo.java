package com.wgm.account.vo;

import lombok.Data;

/**
 * @Author donghaolin
 * @Date 2021/1/20 18:44
 * @Version 1.1
 */
@Data
public class UpdateInvoiceInfoVo {

    /**
     * 修改id
     */
    private String id;

    /**
     * 开户行名称
     */
    private String bankName;

    /**
     *开户行账号
     */
    private String bankAccount;
}
