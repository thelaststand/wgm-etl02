package com.wgm.account.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.account.dao.InvoiceAddressDao;
import com.wgm.account.entity.InvoiceAddressEntity;
import com.wgm.account.service.InvoiceAddressService;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.exception.TaskException;
import com.wgm.common.utils.UuidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 发票地址管理
 *
 *
 * @Author donghaolin
 * @Date 2021/1/20 20:42
 * @Version 1.0
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class InvoiceAddressServiceImpl extends ServiceImpl<InvoiceAddressDao, InvoiceAddressEntity> implements InvoiceAddressService {

    @Autowired
    private InvoiceAddressDao invoiceAddressDao;

    /**
     * 发票地址管理
     * @return
     */
    @Override
    public List<Map<String, Object>> invoiceAddressList() {
        List<Map<String,Object>> list= invoiceAddressDao.invoiceAddressList();
        return list;
    }

    /**
     * 发票地址添加
     * @param invoiceAddressEntity
     */
    @Override
    public void add(InvoiceAddressEntity invoiceAddressEntity) {

        //判断输入参数
        if (invoiceAddressEntity==null
                ||invoiceAddressEntity.getUserId().equals(null)
                ||invoiceAddressEntity.getCompanyInfoId().equals(null)){
            throw new TaskException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //添加
        invoiceAddressEntity.setId(UuidUtils.generateUuid());
        invoiceAddressEntity.setCreateTime(new Date());
        invoiceAddressEntity.setUpdateTime(new Date());
        invoiceAddressEntity.setShowStatus(0);
        invoiceAddressDao.add(invoiceAddressEntity);
    }

    /**
     * 发票地址回显
     * @param id
     * @return
     */
    @Override
    public InvoiceAddressEntity invoiceAddressFindById(String id) {

        //判断输入的参数
        if (id.equals(null)){
            throw new TaskException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //查询
        InvoiceAddressEntity invoiceAddressEntity = invoiceAddressDao.invoiceAddressFindById(id);

        return invoiceAddressEntity;
    }

    /**
     * 发票地址修改
     * @param invoiceAddressEntity
     */
    @Override
    public void updateInvoiceAddress(InvoiceAddressEntity invoiceAddressEntity) {

        //判断输入参数
        if (invoiceAddressEntity==null){
            throw new TaskException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //修改
        invoiceAddressEntity.setUpdateTime(new Date());
        invoiceAddressDao.updateInvoiceAddress(invoiceAddressEntity);
    }

    /**
     * 逻辑删除地址
     * @param id
     */
    @Override
    public void deleteInvoiceAddress(String id) {
        //判断输入的参数
        if (id.equals(null)){
            throw new TaskException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //删除
        invoiceAddressDao.deleteInvoiceAddress(id);
    }


}
