package com.wgm.account.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-21 19:39:57
 */
@Data
@TableName("user_warning")
public class UserWarningEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 预警手机编号
	 */
	@TableId
	private String warningCode;
	/**
	 * 预警联系人名称
	 */
	private String warningName;
	/**
	 * 预警手机号
	 */
	private String warningPhone;
	/**
	 * 启用状态：0-启用 1-不启用
	 */
	private Integer enabledState;
	/**
	 * 用户id
	 */
	private String uid;

}
