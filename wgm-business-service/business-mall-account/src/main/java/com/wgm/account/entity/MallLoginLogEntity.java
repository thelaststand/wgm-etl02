package com.wgm.account.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 登录日志
 *
 * @author donghaolin
 * @email 2326818551@qq.com
 * @date 2021-01-25 10:48:08
 */
@Data
@TableName("mall_login_log")
public class MallLoginLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String id;
	/**
	 * 登录状态
	 */
	private String loginStatus;
	/**
	 * 登录方式
	 */
	private String loginMode;
	/**
	 * 登录ip
	 */
	private String loginIp;
	/**
	 * 登录城市
	 */
	private String loginCity;
	/**
	 * 登录时间
	 */
	private Date loginTime;
	/**
	 * 逻辑删除
	 */
	private Integer logicalState;

}
