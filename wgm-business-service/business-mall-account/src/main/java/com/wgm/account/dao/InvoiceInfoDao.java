package com.wgm.account.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.account.entity.InvoiceInfoEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 发票信息管理
 *
 * @Author donghaolin
 * @Date 2021/1/20 11:46
 * @Version 1.0
 */
@Mapper
public interface InvoiceInfoDao  extends BaseMapper<InvoiceInfoEntity> {

    /**
     * 显示发票信息表数据
     * @param
     * @return
     */
    List<Map<String,Object>> invoiceInfoList();

    /**
     * 发票信息修改
     * @param infoEntity
     */
    void update(InvoiceInfoEntity infoEntity);

    /**
     * 发票信息逻辑删除
     * @param id
     */
    void delete(@Param("id") String id);

    /**
     * 添加发票信息
     * @param invoiceInfoEntity
     */
    void add(@Param("invoiceInfoEntity") InvoiceInfoEntity invoiceInfoEntity);
}
