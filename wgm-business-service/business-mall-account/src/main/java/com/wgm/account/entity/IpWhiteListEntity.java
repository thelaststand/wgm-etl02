package com.wgm.account.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * ip白名单
 * 
 * @author song-huan
 * @email 3325074245@qq.com
 * @date 2021-01-22 18:59:00
 */
@Data
@TableName("ip_white_list")
public class IpWhiteListEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * ip地址
	 */
	private String ipSite;
	/**
	 * 公司id
	 */
	private String companyId;
	/**
	 * 0启用 1禁用
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date creatTime;
	/**
	 * 创建人
	 */
	private String creatBy;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 修改人
	 */
	private String updateBy;
	/**
	 * 0展示 1不展示
	 */
	@TableLogic(value = "0",delval = "1")
	private Integer showStatus;
	/**
	 * 乐观锁
	 */
	private Integer revision;

}
