package com.wgm.account.service;

import com.wgm.account.entity.InvoiceInfoEntity;
import com.wgm.account.vo.UpdateInvoiceInfoVo;

import java.util.List;
import java.util.Map;

/**
 *
 * 发票信息管理
 *
 * @Author donghaolin
 * @Date 2021/1/20 11:36
 * @Version 1.0
 */
public interface InvoiceInfoService {
    /**
     * 显示发票信息表数据
     * @param
     * @return
     */
    List<Map<String,Object>> invoiceInfoList();

    /**
     * 添加发票信息
     * @param invoiceInfoEntity
     */
    void add(InvoiceInfoEntity invoiceInfoEntity);

    /**
     * 回显当前id的信息
     * @param id
     * @return
     */
    InvoiceInfoEntity invoiceInfoFindById(String id);

    /**
     * 修改发票信息
     * @param updateInvoiceInfoVo
     */
    void updateInvoiceInfo(UpdateInvoiceInfoVo updateInvoiceInfoVo);

    /**
     * 删除发票信息
     * @param id
     */
    void deleteInvoiceInfo(String id);
}
