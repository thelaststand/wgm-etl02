package com.wgm.account.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.account.entity.BusinessCompanyInfoEntity;
import com.wgm.account.entity.InvoiceAddressEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 发票地址管理
 *
 * @Author donghaolin
 * @Date 2021/1/20 20:47
 * @Version 1.0
 */
@Mapper
public interface InvoiceAddressDao extends BaseMapper<InvoiceAddressEntity> {

    /**
     * 发票地址管理
     * @return
     */
    List<Map<String, Object>> invoiceAddressList();

    /**
     * 发票地址添加
     * @param invoiceAddressEntity
     */
    void add(@Param("invoiceAddressEntity") InvoiceAddressEntity invoiceAddressEntity);

    /**
     * 发票地址回显
     * @param id
     * @return
     */
    InvoiceAddressEntity invoiceAddressFindById(@Param("id") String id);

    /**
     * 发票地址修改
     * @param invoiceAddressEntity
     */
    void updateInvoiceAddress(InvoiceAddressEntity invoiceAddressEntity);

    /**
     * 逻辑删除地址
     * @param id
     */
    void deleteInvoiceAddress(@Param("id")String id);
}
