package com.wgm.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.account.vo.CompanyInfoVo;
import com.wgm.account.vo.MallUserVo;
import com.wgm.account.entity.MallUserEntity;
import com.wgm.account.vo.UpdatePhoneVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户表
 *
 * @author song-huan
 * @email 3325074245@qq.com
 * @date 2021-01-20 10:13:24
 */
public interface MallUserService extends IService<MallUserEntity> {

    /**
     *  测试查询
     * @return
     */
    List<MallUserEntity> userList();

    /**
     *  注册
     * @param mallUser
     * @return
     */
    Boolean register(MallUserVo mallUser);

    /**
     *  登录
     * @param mallUser
     * @return
     */
    Boolean login(MallUserVo mallUser);

    /**
     * 根据用户id查找
     * @param userId
     * @return
     */
    MallUserEntity findById(@Param("userId") String userId);

    /**
     * 通过用户ID获取公司信息
     * @param id
     * @return
     */
    CompanyInfoVo getCompanyInfoByUserId(String id);

    /**
     * 修改手机号
     * @param updatePhoneVo
     */
    void updatePhone(UpdatePhoneVo updatePhoneVo);


    /**
     *  忘记密码
     * @param mallUser
     * @return
     */
    Boolean forgetPassword(MallUserVo mallUser);
}

