package com.wgm.account.controller;

import com.wgm.account.entity.BusinessCompanyInfoEntity;
import com.wgm.account.service.BusinessCompanyInfoService;
import com.wgm.account.vo.CompanyInfoVo;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.utils.PageUtils;
import com.wgm.common.valid.UpdateStatusGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 商业公司管理
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/21
 */
@RestController
@RequestMapping("business/company/manage")
public class BusinessCompanyManageController {

    @Autowired
    private BusinessCompanyInfoService businessCompanyInfoService;


    /**
     * 审核
     * @param companyInfoVo
     * @return
     */
    @PutMapping("/examination")
    public CommonResponse examinationCompany(@Validated(UpdateStatusGroup.class)  @RequestBody CompanyInfoVo companyInfoVo){
        Integer i = businessCompanyInfoService.examinationCompany(companyInfoVo);
        if (i>0){
            return CommonResponse.ok();
        }else {
            return CommonResponse.error();
        }
    }

    /**
     * 企业列表分页查询
     * @param pageSize 每页条数
     * @param curPage 当前页
     * @param state 审核状态
     * @return
     */
    @GetMapping("/info/list")
    public CommonResponse getCompanyInfoList(@RequestParam(value = "pageSize",defaultValue = "10",required = false) Integer pageSize,
                                             @RequestParam(value = "curPage",defaultValue = "1",required = false) Integer curPage,
                                             @RequestParam(value = "state",required = false) Integer state){
       PageUtils page = businessCompanyInfoService.getCompanyInfoList(pageSize,curPage,state);
       return CommonResponse.ok(page);
    }

    /**
     * 查询对应公司信息
     * @param id
     * @return
     */
    @GetMapping("/findCompanyById/{id}")
    public CommonResponse findCompanyById(@PathVariable("id") String id){
        BusinessCompanyInfoEntity data = businessCompanyInfoService.findCompanyById(id);
        return CommonResponse.ok(data);
    }



}
