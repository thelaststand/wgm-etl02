package com.wgm.account.controller;

import com.wgm.account.entity.IpWhiteListEntity;
import com.wgm.account.service.IpWhiteListService;
import com.wgm.account.vo.IpWhiteListVo;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.constant.CommonStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author songhuan
 * @email 3325074245@qq.com
 * @date 2021/01/24
 */
@RestController
@RequestMapping("account/ipwhitelist")
public class IpWhiteListWebController {
    @Autowired
    private IpWhiteListService ipWhiteListService;

    /**
     *  前台查询Ip白名单
     * @return
     */
    @GetMapping("/getIpWebList")
    public CommonResponse getIpWebList(){
        List<IpWhiteListEntity> data =  ipWhiteListService.getIpWebList();
        return CommonResponse.ok(data);
    }


    /**
     *  前台根据Id删除信息
     * @param id
     * @return
     */
    @GetMapping("/deleteIpWebList/{id}")
    public CommonResponse deleteIpWebList(@PathVariable("id") Integer id){
        return ipWhiteListService.deleteIpWebList(id) ?
                new CommonResponse(CommonStatus.VALID) :
                new CommonResponse(CommonStatus.INVALID);
    }

    /**
     *  前台批量添加Ip地址
     * @param ipSite
     * @return
     */
    @PostMapping("/saveIpWebList")
    public CommonResponse saveIpWebList(@RequestParam String ipSite){
        return ipWhiteListService.saveIpWebList(ipSite) ?
                new CommonResponse(CommonStatus.VALID) :
                new CommonResponse(CommonStatus.INVALID);
    }


    /**
     *  前台修改Ip地址
     * @param ipWhiteListEntity
     * @return
     */
    @PostMapping("/updateIpWebList")
    public CommonResponse updateIpWebList(@RequestBody IpWhiteListEntity ipWhiteListEntity){
        return ipWhiteListService.updateIpWebList(ipWhiteListEntity) ?
                new CommonResponse(CommonStatus.VALID) :
                new CommonResponse(CommonStatus.INVALID);
    }


}
