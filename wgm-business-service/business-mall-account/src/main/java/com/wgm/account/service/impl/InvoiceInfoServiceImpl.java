package com.wgm.account.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.account.dao.BusinessCompanyInfoDao;
import com.wgm.account.dao.InvoiceInfoDao;
import com.wgm.account.entity.BusinessCompanyInfoEntity;
import com.wgm.account.entity.InvoiceInfoEntity;
import com.wgm.account.service.InvoiceInfoService;
import com.wgm.account.vo.UpdateInvoiceInfoVo;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.exception.TaskException;
import com.wgm.common.utils.UuidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @Author donghaolin
 * @Date 2021/1/20 11:45
 * @Version 1.0
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class InvoiceInfoServiceImpl extends ServiceImpl<InvoiceInfoDao, InvoiceInfoEntity> implements InvoiceInfoService {

    @Autowired
    InvoiceInfoDao invoiceInfoDao;
    @Autowired
    BusinessCompanyInfoDao businessCompanyInfoDao;

    /**
     * 显示发票信息表数据
     * @param
     * @return
     */
    @Override
    public List<Map<String,Object>> invoiceInfoList() {
        List<Map<String,Object>> list= invoiceInfoDao.invoiceInfoList();
        return list;
    }

    /**
     * 添加发票信息
     * @param invoiceInfoEntity
     */
    @Override
    public void add(InvoiceInfoEntity invoiceInfoEntity) {
        //判断输入参数
        if (invoiceInfoEntity==null||
                invoiceInfoEntity.getBankName().equals(null)||
                invoiceInfoEntity.getBankAccount().equals(null)){
            throw new TaskException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //添加
        invoiceInfoEntity.setId(UuidUtils.generateUuid());
        invoiceInfoEntity.setCreateTime(new Date());
        invoiceInfoEntity.setUpdateTime(new Date());
        invoiceInfoEntity.setShowStatus(0);
        invoiceInfoDao.add(invoiceInfoEntity);
    }

    /**
     * 当前类的回显
     * @param id
     * @return
     */
    @Override
    public InvoiceInfoEntity invoiceInfoFindById(String id) {
        //判断id是否为空
        if (id.equals(null)){
            throw new TaskException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //查询
        InvoiceInfoEntity invoiceInfoEntity = invoiceInfoDao.selectById(id);
        return invoiceInfoEntity;
    }

    /**
     * 发票信息修改
     * @param updateInvoiceInfoVo
     */
    @Override
    public void updateInvoiceInfo(UpdateInvoiceInfoVo updateInvoiceInfoVo) {
        //判断输入参数
        if (updateInvoiceInfoVo==null||
                updateInvoiceInfoVo.getBankName().equals(null)||
                updateInvoiceInfoVo.getBankAccount().equals(null)){
            throw new TaskException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        InvoiceInfoEntity infoEntity = invoiceInfoDao.selectById(updateInvoiceInfoVo.getId());
        //参数修改
        infoEntity.setBankName(updateInvoiceInfoVo.getBankName());
        infoEntity.setBankAccount(updateInvoiceInfoVo.getBankAccount());
        infoEntity.setUpdateTime(new Date());
        //修改
        invoiceInfoDao.update(infoEntity);

    }

    /**
     * 发票信息逻辑删除
     * @param id
     */
    @Override
    public void deleteInvoiceInfo(String id) {
        //判断id是否为空
        if (id.equals(null)){
            throw new TaskException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        //逻辑删除
        invoiceInfoDao.delete(id);
    }
}
