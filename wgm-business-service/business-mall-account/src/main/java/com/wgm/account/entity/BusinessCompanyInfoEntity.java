package com.wgm.account.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.wgm.common.valid.AddGroup;
import com.wgm.common.valid.UpdateGroup;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;


/**
 * 商业公司信息

 * 
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-20 11:15:03
 */
@Data
@TableName("business_company_info")
public class BusinessCompanyInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@TableId
	private String id;
	/**
	 * 认证人id
	 */
	private String userId;
	/**
	 * 企业名称
	 */
	private String enterpriseName;
	/**
	 * 公司法人
	 */
	private String legalPerson;
	/**
	 * 法人手机号
	 */
	private String legalPersonPhone;
	/**
	 * 职位
	 */
	private String position;
	/**
	 * 公司所在省市区
	 */
	private String provinceCity;
	/**
	 * 详细地址
	 */
	private String detailedAddress;
	/**
	 * 信用编码
	 */
	private String creditCode;
	/**
	 * 营业执照
	 */
	private String businessLicense;
	/**
	 * 审核状态：0-待审核 1-审核通过 2-审核不通过
	 */
	private Integer state;
	/**
	 * 不通过描述
	 */
	private String failureDescription;
	/**
	 * 创建日期
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 快速展示：0-展示 1-不展示
	 */
	private Integer showDesc;

}
