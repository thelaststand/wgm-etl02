package com.wgm.account.vo;

import lombok.Data;

import java.util.Date;

/**
 * @Author donghaolin
 * @Date 2021/1/21 15:33
 * @Version 1.0
 */
@Data
public class UpdatePhoneVo {

    /**
     * 当前登录人id
     */
    private String uid;

    /**
     * 当前登录人手机验证码
     */
    private String code;

    /**
     * 修改的手机号
     */
    private String newPhone;

    /**
     * 修改手机号验证码
     */
    private String newCode;

    /**
     * 修改时间
     */
    private Date updateTime;
}
