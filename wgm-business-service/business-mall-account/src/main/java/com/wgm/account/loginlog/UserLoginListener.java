package com.wgm.account.loginlog;

import com.wgm.account.entity.MallLoginLogEntity;
import com.wgm.account.service.MallLoginLogService;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.*;
import java.util.Date;

/**
 * @Author donghaolin
 * @Date 2021/1/25 10:30
 * @Version 1.0
 */
public class UserLoginListener implements HttpSessionListener,ServletRequestListener,HttpSessionAttributeListener{
    //直接注入空指针
    /**
     * ip
     */
    private String ipAddress;
    /**
     * 用户名
     */
    private String userAgent;
    /**
     * 城市
     */
    private String address;
    MallLoginLogService userLogService ;
    @Override
    public void sessionCreated(HttpSessionEvent se) {
        if (userLogService==null) {
            userLogService= WebApplicationContextUtils.
                    getWebApplicationContext(
                            se.getSession().
                                    getServletContext()
                    ).getBean(MallLoginLogService.class);
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        //登出时操作
        //可以得到session对象
        //销毁对象前      触发这个  就不会出现空指针错误   可以用来 记录用户什么时候登出
        //问题  如果对象 是直接关闭浏览器   而不是点击退出  只能等到session自动销毁  触发这个事件
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String userName = (String) request.getSession().getAttribute("username");
        System.out.println("获取session内容" + request.getSession().getAttribute("username"));
        System.out.println(ipAddress);
        String type = "1";
        //在这里  向登录日志表中添加数据
        MallLoginLogEntity userLog = new MallLoginLogEntity();
        userLog.setLoginIp(ipAddress);
        userLog.setLoginTime(new Date());
        userLog.setLoginStatus(type);
        userLog.setLoginCity(address);
        userLogService.addLoginLogger(userLog);
    }

    @Override
    public void attributeAdded(HttpSessionBindingEvent se) {
        //往session中放值时 触发     这时session中有值  所以可以取到  不会空指针错误
        //这里 获取 登录用户的信息  用户名  登录时间
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String userName = (String) request.getSession().getAttribute("username");
        System.out.println("获取session内容" + request.getSession().getAttribute("username"));
        System.out.println(ipAddress+"/"+userAgent);
        String type = "1";
        //在这里  向登录日志表中添加数据
        MallLoginLogEntity userLog = new MallLoginLogEntity();
        userLog.setLoginIp(ipAddress);
        userLog.setLoginTime(new Date());
        userLog.setLoginStatus(type);
        userLog.setLoginCity(address);
        userLogService.addLoginLogger(userLog);
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent se) {
        /*UserDTO userDTO = (UserDTO)se.getSession().getAttribute("userDTO");*/
        //用户退出后    这里的 里是空值  所以报错
        //移除session中的值  之后触发   所以 上面代码会空指针
        /*System.out.println("用户退出"+userName);
        type = "1";*/
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent se) {
        /*System.out.println("修改用户");*/
    }

    @Override
    public void requestDestroyed(ServletRequestEvent requestEvent) {
    }

    @Override
    public void requestInitialized(ServletRequestEvent requestEvent) {
        //这里  获取用户的  ip地址  和  登录设备
        HttpServletRequest request = (HttpServletRequest)requestEvent.getServletRequest();
        userAgent = request.getHeader("user-agent");
        ipAddress = request.getRemoteAddr();
        /*System.out.println(ipAddress+"/"+userAgent);*/
    }
}
