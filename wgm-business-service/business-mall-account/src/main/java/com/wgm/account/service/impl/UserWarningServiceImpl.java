package com.wgm.account.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.account.dao.UserWarningDao;
import com.wgm.account.entity.UserWarningEntity;
import com.wgm.account.service.UserWarningService;
import com.wgm.account.vo.WarningVo;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonStatus;
import com.wgm.common.exception.AdException;
import com.wgm.common.utils.PhoneUtils;
import com.wgm.common.utils.UuidUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 *用户预警号码
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-21 19:39:57
 */
@Service("userWarningService")
@Transactional(rollbackFor = Exception.class)
public class UserWarningServiceImpl extends ServiceImpl<UserWarningDao, UserWarningEntity> implements UserWarningService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveUserWarning(WarningVo warningVo) {
        //1、验证手机号码
        if (!PhoneUtils.isPhoneLegal(warningVo.getWarningPhone())){
            throw new AdException(CommonConstants.RegisteredAndLoginError.PHONE_NUMBER_ERROR);
        }
        //2、判断是否重复
        Integer count = this.baseMapper.getCountByUidAndPhone(warningVo.getUid(),warningVo.getWarningPhone());
        if (count>0){
            throw new AdException(CommonConstants.RegisteredAndLoginError.PHONE_NUMBER_EXIST);
        }
        //3、判断验证码
        String code = (String) redisTemplate.boundValueOps(warningVo.getWarningPhone()).get();
        if (!StringUtils.isEmpty(code)&&!code.equals(warningVo.getCode())){
            throw new AdException(CommonConstants.RegisteredAndLoginError.INCORRECT_VERIFICATION_CODE);
        }
        //4、添加
        UserWarningEntity warningEntity = new UserWarningEntity();
        BeanUtils.copyProperties(warningVo,warningEntity);
        warningEntity.setWarningCode(UuidUtils.generateUuid());
        warningEntity.setEnabledState(CommonStatus.IS_USING_ENUM.getCode());
        Integer i = this.baseMapper.saveUserWarning(warningEntity);
        if (i>0){
            return true;
        }
        return false;


    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean delUserWarning(WarningVo warningVo) {
        Integer i = this.baseMapper.delUserWarning(warningVo.getWarningCode());
        if (i>0){
            return true;
        }
        return false;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateUserWarning(WarningVo warningVo) {
        //1、验证手机号码
        if (!PhoneUtils.isPhoneLegal(warningVo.getWarningPhone())){
            throw new AdException(CommonConstants.RegisteredAndLoginError.PHONE_NUMBER_ERROR);
        }
        //2、判断是否重复
        Integer count = this.baseMapper.getCountByUidAndPhone(warningVo.getUid(),warningVo.getWarningPhone());
        if (count>0){
            throw new AdException(CommonConstants.RegisteredAndLoginError.PHONE_NUMBER_EXIST);
        }
        //3、判断验证码
        String code = (String) redisTemplate.boundValueOps(warningVo.getWarningPhone()).get();
        if (!StringUtils.isEmpty(code)&&!code.equals(warningVo.getCode())){
            throw new AdException(CommonConstants.RegisteredAndLoginError.INCORRECT_VERIFICATION_CODE);
        }
        Integer i = this.baseMapper.updateUserWarning(warningVo.getWarningName(),warningVo.getWarningPhone(),warningVo.getWarningCode());
        if (i>0){
            return true;
        }
        return false;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateUserWarningStatus(WarningVo warningVo) {
        Integer i = this.baseMapper.updateUserWarnixngStatus(warningVo.getWarningCode(),warningVo.getEnabledState());
        if (i>0){
            return true;
        }
        return false;
    }

    @Override
    public List<UserWarningEntity> getUserWarningListByUid(String uid) {
        List<UserWarningEntity> list = this.baseMapper.getUserWarningListByUid(uid);
        return list;
    }
}