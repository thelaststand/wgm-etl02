package com.wgm.account.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.wgm.common.valid.AddGroup;
import com.wgm.common.valid.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Null;
import java.util.Date;

/**
 * @author songhuan
 * @email 3325074245@qq.com
 * @date 2021/01/22
 */
@Data
public class IpWhiteListVo {
    /**
     * id
     */
    @Null(message = "新增不能指定id",groups = {AddGroup.class})
    @NotEmpty(message = "修改必须指定id",groups = {UpdateGroup.class})
    private Integer id;
    /**
     * ip地址
     */
    @NotEmpty(message = "ip地址不能为空",groups = {AddGroup.class})
    private String ipSite;
    /**
     * 公司id
     */
    private String companyId;
    /**
     * 公司名称
     */
    private String  companyName;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 0启用 1禁用
     */
    private Integer status;
    /**
     * 创建时间
     */
    private Date creatTime;
    /**
     * 创建人
     */
    private String creatBy;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 修改人
     */
    private String updateBy;
    /**
     * 0展示 1不展示
     */
    private Integer showStatus;
    /**
     * 乐观锁
     */
    private Integer revision;
}
