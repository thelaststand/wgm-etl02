package com.wgm.account.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wgm.common.utils.UuidUtils;
import lombok.Data;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/20
 */
@Data
@TableName("sys_region")
public class SysRegionEntity {

    @TableId
    private String regionId;
    /**
     * 地区名称
     */
    private String regionName;
    /**
     * 地区父id
     */
    private String regionParentId;
    /**
     * 地区级别
     */
    private Integer regionLevel;

    public static void main(String[] args) {
        String s = UuidUtils.generateUuid();
        System.out.println(s);
    }

}
