package com.wgm.account.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * 发票信息管理
 *
 * @Author donghaolin
 * @Date 2021/1/20 11:36
 * @Version 1.0
 */
@Data
@TableName("invoice_info")
public class InvoiceInfoEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId
    private String id;

    /**
     * 公司id
     */
    private String companyInfoId;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 开户行名称
     */
    private String bankName;

    /**
     * 开户行账号
     */
    private String bankAccount;

    /**
     * 0展示 1不展示(逻辑删除)
     */
    private Integer showStatus;
    /**
     * 创建日期
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
}
