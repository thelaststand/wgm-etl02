package com.wgm.account.vo;

import com.wgm.common.valid.*;
import lombok.Data;
import org.springframework.web.bind.annotation.DeleteMapping;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Null;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/21
 */
@Data
public class WarningVo {

    /**
     * 预警号码编号
     */
    @Null(message = "不能设置预警号码编号",groups = {AddGroup.class})
    @NotEmpty(message = "预警号码编号不能为空",groups = {DeleteGroup.class, UpdateGroup.class,UpdateStatusGroup.class})
    private String warningCode;
    /**
     * 预警联系人名称
     */
    @NotEmpty(message = "预警联系人名称不能为空",groups = {AddGroup.class,UpdateGroup.class})
    private String warningName;
    /**
     * 预警手机号
     */
    @NotEmpty(message = "预警手机号不能为空",groups = {AddGroup.class,UpdateGroup.class})
    private String warningPhone;
    /**
     * 启用状态：0-启用 1-不启用
     */
    @NotEmpty(message = "状态不能为空",groups = {UpdateStatusGroup.class})
    @ListValue(vals = {0,1},message = "状态值不符合规定",groups = UpdateStatusGroup.class)
    private Integer enabledState;
    /**
     * 用户id
     */
    @NotEmpty(message = "用户id不能为空",groups = {AddGroup.class})
    private String uid;
    /**
     * 验证码
     */
    @NotEmpty(message = "验证码不能为空",groups = {AddGroup.class,UpdateGroup.class})
    private String code;


}
