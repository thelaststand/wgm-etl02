package com.wgm.account.controller;

import com.wgm.account.entity.MallLoginLogEntity;
import com.wgm.account.service.MallLoginLogService;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 登录日志
 * @Author donghaolin
 * @Date 2021/1/25 11:29
 * @Version 1.0
 */
@RestController
@RequestMapping("account/mallloginlog")
public class MallLoginLogController {
    @Autowired
    private MallLoginLogService mallLoginLogService;

    /**
     * 登录日志列表
     * @return
     */
    @GetMapping()
    public CommonResponse mallLoginLogList(){
        List<MallLoginLogEntity> list = mallLoginLogService.mallLoginLogList();
        return CommonResponse.ok(list);
    }

    /**
     * 重置登录日志
     * @return
     */
    @PostMapping()
    public CommonResponse reset(){
        mallLoginLogService.reset();
        return CommonResponse.ok();
    }
}
