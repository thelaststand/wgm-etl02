package com.wgm.account.dao;

import com.wgm.account.entity.IpWhiteListEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * ip白名单
 * 
 * @author song-huan
 * @email 3325074245@qq.com
 * @date 2021-01-22 18:59:00
 */
@Mapper
public interface IpWhiteListDao extends BaseMapper<IpWhiteListEntity> {

    /**
     *  获取所有可能展示的ip白名单列表
     * @param curPage
     * @param pageSize
     * @return
     */
    List<IpWhiteListEntity> selectByPage(@Param("curPage") int curPage, @Param("pageSize") Integer pageSize);

    /**
     * 获取总条数
     * @return
     */
    Integer getCount();

    /**
     *  后台添加Ip白名单
     * @param ipWhiteListEntity
     * @return
     */
    int saveIpList(@Param("ipWhiteListEntity") IpWhiteListEntity ipWhiteListEntity);

    /**
     *  根据Ip编号查询白名单信息
     * @param id
     * @return
     */
    IpWhiteListEntity selectByIpListId(Integer id);

    /**
     *  根据Id删除
     * @param id
     * @return
     */
    int deleteIpList(Integer id);

    /**
     *  前台查询Ip白名单列表
     * @return
     */
    List<IpWhiteListEntity> getIpWebList();

    /**
     *  前台根据Id删除
     * @param id
     * @return
     */
    int deleteIpWebList(Integer id);

    /**
     *  前台Ip添加
     * @param entity
     * @return
     */
    int saveIpWebList(@Param("entity") IpWhiteListEntity entity);

    /**
     *  前台Ip修改
     * @param ipWhiteListEntity
     * @return
     */
    int updateIpWebList(IpWhiteListEntity ipWhiteListEntity);
}
