package com.wgm.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.account.entity.MallLoginLogEntity;

import java.util.List;

/**
 * 登录日志
 *
 * @author donghaolin
 * @email 2326818551@qq.com
 * @date 2021-01-25 10:48:08
 */
public interface MallLoginLogService extends IService<MallLoginLogEntity> {


    /**
     * 登录日志添加
     * @param userLog
     */
    void addLoginLogger(MallLoginLogEntity userLog);

    /**
     * 日志列表
     * @return
     */
    List<MallLoginLogEntity> mallLoginLogList();

    /**
     * 重置日志
     */
    void reset();
}

