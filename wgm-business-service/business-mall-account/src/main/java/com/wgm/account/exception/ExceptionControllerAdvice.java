package com.wgm.account.exception;

import com.baomidou.mybatisplus.extension.api.R;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/20
 */
@RestControllerAdvice(basePackages = {"com.wgm.account.controller"})
@Slf4j
public class ExceptionControllerAdvice {

    private final String PHONE="phone";


    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public CommonResponse handleVaildException(MethodArgumentNotValidException e){
        log.error("数据校验出现问题{}，异常类型：{}",e.getMessage(),e.getClass());
        BindingResult bindingResult = e.getBindingResult();
        Map<String, String> errerMap = new ConcurrentHashMap<>(16);
        bindingResult.getFieldErrors().forEach(fieldError->{
            errerMap.put(fieldError.getField(),fieldError.getDefaultMessage());
        });
        return CommonResponse.error(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR,errerMap);
    }


    @ExceptionHandler(value = DuplicateKeyException.class)
    public CommonResponse duplicateKeyException(DuplicateKeyException e){
        String message = e.getMessage();
        if(message.contains(PHONE)){
            return CommonResponse.error("手机号已注册");
        }
        return CommonResponse.error();
    }


    @ExceptionHandler(value = Throwable.class)
    public CommonResponse handleException(Throwable throwable){
        log.error("异常信息->{}:",throwable);
        return CommonResponse.error(throwable.getMessage());
    }


}
