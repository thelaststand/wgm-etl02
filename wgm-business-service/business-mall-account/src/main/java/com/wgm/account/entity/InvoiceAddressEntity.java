package com.wgm.account.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author donghaolin
 * @Date 2021/1/20 20:30
 * @Version 1.0
 */
@Data
@TableName("invoice_address")
public class InvoiceAddressEntity implements Serializable {
    /**
     * 编号
     */
    @TableId
    private String id;

    /**
     * 公司id
     */
    private String companyInfoId;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 收货地址
     */
    private String address;

    /**
     * 0展示 1不展示(逻辑删除)
     */
    private Integer showStatus;
    /**
     * 创建日期
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
}
