package com.wgm.account.controller;

import java.util.List;

import com.wgm.account.vo.UpdatePhoneVo;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.constant.CommonStatus;
import com.wgm.account.vo.MallUserVo;
import com.wgm.common.valid.AddGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.wgm.account.entity.MallUserEntity;
import com.wgm.account.service.MallUserService;




/**
 * 用户表
 *
 * @author song-huan
 * @email 3325074245@qq.com
 * @date 2021-01-20 10:13:24
 */
@RestController
@RequestMapping("account/malluser")
public class MallUserController {
    @Autowired
    private MallUserService mallUserService;

    /**
     *  测试查询
     * @return
     */
    @GetMapping("/list")
    public CommonResponse list(){
        List<MallUserEntity> data = mallUserService.userList();
        return CommonResponse.ok(data);
    }


    /**
     *  register 注册
     * @param mallUser
     * @return Boolean
     */
    @PostMapping("/register")
    public CommonResponse register(@Validated(AddGroup.class) @RequestBody MallUserVo mallUser){
        return mallUserService.register(mallUser) ?
                new CommonResponse(CommonStatus.VALID) :
                new CommonResponse(CommonStatus.INVALID);
    }


    /**
     *  登录
     * @param mallUser
     * @return Boolean
     */
    @PostMapping("/login")
    public CommonResponse login(@RequestBody MallUserVo mallUser){
        return mallUserService.login(mallUser) ?
                new CommonResponse(CommonStatus.VALID) :
                new CommonResponse(CommonStatus.INVALID);
    }


    /**
     * 修改手机号
     * @param updatePhoneVo
     * @return
     */
    @PostMapping("/updatePhone")
    public CommonResponse updatePhone(@RequestBody UpdatePhoneVo updatePhoneVo){
        mallUserService.updatePhone(updatePhoneVo);
        return CommonResponse.ok();
    }

    /**
     *  忘记密码
     * @param mallUser
     * @return
     */
    @PostMapping("/forgetPassword")
    public CommonResponse forgetPassword(@RequestBody MallUserVo mallUser){
        return mallUserService.forgetPassword(mallUser) ?
                new CommonResponse(CommonStatus.VALID) :
                new CommonResponse(CommonStatus.INVALID);
    }

    /**
     * 查询用户信息
     * @param id 用户id
     * @return
     */
    @GetMapping("/findById/{id}")
    public CommonResponse findById(@PathVariable("id") String id){
        MallUserEntity data = mallUserService.findById(id);
        return CommonResponse.ok(data);
    }
}
