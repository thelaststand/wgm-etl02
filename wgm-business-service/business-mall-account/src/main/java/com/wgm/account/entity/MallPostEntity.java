package com.wgm.account.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 职位表
 * 
 * @author song-huan
 * @email 3325074245@qq.com
 * @date 2021-01-20 10:13:24
 */
@Data
@TableName("mall_post")
public class MallPostEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 编号
	 */
	@TableId
	private String id;
	/**
	 * 职位名称
	 */
	private String name;

}
