package com.wgm.account.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.common.utils.PageUtils;

import com.wgm.account.dao.MallLoginLogDao;
import com.wgm.account.entity.MallLoginLogEntity;
import com.wgm.account.service.MallLoginLogService;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author song-huan
 * @email 3325074245@qq.com
 * @date 2021-01-20 10:13:24
 */
@Service("mallLoginLogService")
@Transactional(rollbackFor = Exception.class)
public class MallLoginLogServiceImpl extends ServiceImpl<MallLoginLogDao, MallLoginLogEntity> implements MallLoginLogService {

    @Autowired
    private MallLoginLogDao mallLoginLogDao;

    /**
     * 登录日志添加
     * @param userLog
     */
    @Override
    public void addLoginLogger(MallLoginLogEntity userLog) {
        mallLoginLogDao.insert(userLog);
    }

    /**
     * 登录日志列表
     * @return
     */
    @Override
    public List<MallLoginLogEntity> mallLoginLogList() {
        List<MallLoginLogEntity> list = mallLoginLogDao.selectList();
        return list;
    }

    /**
     * 重置日志
     */
    @Override
    public void reset() {
        mallLoginLogDao.reset();
    }
}