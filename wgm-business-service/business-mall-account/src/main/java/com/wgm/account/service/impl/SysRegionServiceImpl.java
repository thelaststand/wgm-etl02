package com.wgm.account.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.account.dao.SysRegionDao;
import com.wgm.account.entity.SysRegionEntity;

import com.wgm.account.service.SysRegionService;
import com.wgm.account.vo.AddressVo;
import com.wgm.common.constant.CommonStatus;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/20
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysRegionServiceImpl extends ServiceImpl<SysRegionDao, SysRegionEntity> implements SysRegionService {
    /**
     * 获取地址列表
     * @return
     */
    @Override
    public List<AddressVo> getAddressList() {
        //获取地区级别为1的地区
        List<SysRegionEntity> regionLevel1 = this.baseMapper.selectList(new QueryWrapper<SysRegionEntity>().eq("region_level", CommonStatus.PROVINCE.getCode()));
        List<AddressVo> collect = regionLevel1.stream().map(item -> {
            AddressVo addressVo = new AddressVo();
            BeanUtils.copyProperties(item, addressVo);
            addressVo.setChildren(getRegionSon(item.getRegionId()));
            return addressVo;
        }).collect(Collectors.toList());
        return collect;
    }

    /**
     * 得到子地区
     * @param id
     * @return
     */
    public List<AddressVo> getRegionSon(String id){
        List<SysRegionEntity> regionEntities = this.baseMapper.findByPid(id);
        if (regionEntities==null){
            return null;
        }

        List<AddressVo> childrens = regionEntities.stream().map(item -> {
            AddressVo addressVo = new AddressVo();
            BeanUtils.copyProperties(item, addressVo);
            addressVo.setChildren(getRegionSon(item.getRegionId()));
            return addressVo;
        }).collect(Collectors.toList());
        return childrens;
    }
}
