package com.wgm.account.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.account.entity.BusinessCompanyInfoEntity;
import com.wgm.account.vo.CompanyInfoVo;
import com.wgm.common.utils.PageUtils;
import org.apache.ibatis.annotations.Param;

/**
 * 商业公司信息

 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021-01-20 11:15:03
 */
public interface BusinessCompanyInfoService extends IService<BusinessCompanyInfoEntity> {
    /**
     * 企业认证（添加）
     * @param companyInfoVo
     * @return
     */
    void saveInfo(CompanyInfoVo companyInfoVo);
    /**
     * 审核
     * @param companyInfoVo
     * @return
     */
    Integer examinationCompany(CompanyInfoVo companyInfoVo);
    /**
     * 企业列表分页查询
     * @param pageSize 每页条数
     * @param curPage 当前页
     * @param state 审核状态
     * @return
     */
    PageUtils getCompanyInfoList(Integer pageSize, Integer curPage,Integer state);

    /**
     * 根据用户id查询
     * @param id
     * @return
     */
    BusinessCompanyInfoEntity findByUid(@Param("id") String id);

    /**
     * 根据id查询
     * @param companyId
     * @return
     */
    BusinessCompanyInfoEntity findById(String companyId);

    /**
     * 根据公司名查询
     * @param companyName
     * @return
     */
    BusinessCompanyInfoEntity findByCompanyName(String companyName);

    /**
     * 查询公司信息
     * @param id
     * @return
     */
    BusinessCompanyInfoEntity findCompanyById(String id);
}

