package com.wgm.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.account.vo.AdminUserVo;
import com.wgm.common.utils.PageUtils;
import com.wgm.account.entity.AdminUserEntity;

import java.util.Map;

/**
 * 
 *
 * @author song-huan
 * @email 3325074245@qq.com
 * @date 2021-01-22 10:40:42
 */
public interface AdminUserService extends IService<AdminUserEntity> {

    /**
     *  登录
     * @param adminUser
     * @return
     */
    Boolean login(AdminUserVo adminUser);

    /**
     *  注册
     * @param adminUser
     * @return
     */
    Boolean register(AdminUserVo adminUser);
}

