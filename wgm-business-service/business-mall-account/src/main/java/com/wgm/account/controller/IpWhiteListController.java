package com.wgm.account.controller;

import com.wgm.account.vo.IpWhiteListVo;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.constant.CommonStatus;
import com.wgm.common.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wgm.account.service.IpWhiteListService;

import javax.servlet.http.HttpSession;


/**
 * ip白名单 后台
 *
 * @author song-huan
 * @email 3325074245@qq.com
 * @date 2021-01-22 18:59:00
 */
@RestController
@RequestMapping("account/ipwhitelist")
public class IpWhiteListController {
    @Autowired
    private IpWhiteListService ipWhiteListService;

    /**
     *  Ip白名单查询
     * @param pageSize 每页条数
     * @param curPage 当前页
     * @return
     */
    @GetMapping("/getIpList")
    public CommonResponse getIpList(@RequestParam(value = "pageSize",defaultValue = "10",required = false) Integer pageSize,
                                    @RequestParam(value = "curPage",defaultValue = "1",required = false) Integer curPage){
        PageUtils data =  ipWhiteListService.getIpList(pageSize,curPage);
        return CommonResponse.ok(data);
    }

    /**
     *  添加IP白名单
     * @param ipWhiteList
     * @return
     */
    @PostMapping("/saveIpList")
    public CommonResponse saveIpList(@RequestBody IpWhiteListVo ipWhiteList,HttpSession session){
        return ipWhiteListService.saveIpList(ipWhiteList,session) ?
                new CommonResponse(CommonStatus.VALID) :
                new CommonResponse(CommonStatus.INVALID);
    }

    /**
     *  删除Ip白名单
     * @param id
     * @return
     */
    @GetMapping("/deleteIpList/{id}")
    public CommonResponse deleteIpList(@PathVariable("id") Integer id){
        return ipWhiteListService.deleteIpList(id) ?
                new CommonResponse(CommonStatus.VALID) :
                new CommonResponse(CommonStatus.INVALID);
    }


    /**
     *  修改Ip白名单
     * @param ipWhiteListVo
     * @param session
     * @return
     */
    public CommonResponse updateIpList(@RequestBody IpWhiteListVo ipWhiteListVo,HttpSession session){
        return ipWhiteListService.updateIpList(ipWhiteListVo,session) ?
                new CommonResponse(CommonStatus.VALID) :
                new CommonResponse(CommonStatus.INVALID);
    }






}
