package com.wgm.account.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/20
 */
@Data
public class AddressVo {

    private String regionId;
    /**
     * 地区名称
     */
    private String regionName;
    /**
     * 子地区
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<AddressVo> children;


}
