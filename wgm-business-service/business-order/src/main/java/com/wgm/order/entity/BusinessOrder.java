package com.wgm.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单
 * 
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-20 11:46:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("business_order")
public class BusinessOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String id;
	/**
	 * 用户id
	 */
	private String userId;
	/**
	 * 订单编号
	 */
	private String orderSn;
	/**
	 * 充值方式
	 */
	private String rechargeType;
	/**
	 * 交易金额
	 */
	private BigDecimal totalAmount;
	/**
	 * 用户名
	 */
	private String username;
	/**
	 * 手机号
	 */
	private String phone;
	/**
	 * 审核状态 (0-待审核,1-审核通过,2-审核未通过)
	 */
	private Integer status;
	/**
	 * 公司id
	 */
	private String companyId;
	/**
	 * 公司名称
	 */
	private String companyName;
	/**
	 * 充值时间
	 */
	private Date rechargeTime;
	/**
	 * 开票状态 (0-未开票,1-已开票)
	 */
	private Integer invoiceStatus;
	/**
	 * 账户编号
	 */
	private String accountId;

}
