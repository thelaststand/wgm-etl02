package com.wgm.order.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 账户信息
 * 
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-22 10:24:27
 */
@Data
public class AccountResponse{

	/**
	 * 
	 */
	private String id;
	/**
	 * 预警状态 （0-禁用,1-开启）
	 */
	private String warningStatus;
	/**
	 * 预警阈值
	 */
	private BigDecimal warningThreshould;
	/**
	 * 预警号码
	 */
	private String warningPhone;
	/**
	 * 余额
	 */
	private BigDecimal balance;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;

}
