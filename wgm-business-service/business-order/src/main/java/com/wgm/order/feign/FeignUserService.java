package com.wgm.order.feign;

import com.wgm.common.constant.CommonResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/21 11:57
 */
@FeignClient(name = "business-mall-account")
public interface FeignUserService {

    /**
     * 查询用户信息
     * @param id 用户id
     * @return
     */
    @GetMapping("/account/malluser/findById/{id}")
    public CommonResponse findById(@PathVariable("id") String id);

    /**
     * 查询对应公司信息
     * @param id
     * @return
     */
    @GetMapping("/business/company/manage/findCompanyById/{id}")
    public CommonResponse findCompanyById(@PathVariable("id") String id);
}
