package com.wgm.order.service;

import com.alipay.api.AlipayApiException;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.common.constant.CommonResponse;
import com.wgm.order.entity.BusinessOrder;
import com.wgm.order.pojo.OrderRequest;
import com.wgm.order.pojo.RechargeRequest;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/20 12:02
 */
public interface BusinessOrderService extends IService<BusinessOrder> {

    /**
     * 前台 列表 + 模糊 + 分页
     * @param request
     * @param currPage
     * @param pageSize
     * @return
     */
    CommonResponse findList(RechargeRequest request, Integer currPage, Integer pageSize);

    /**
     * 后台 列表 + 模糊 + 分页
     * @param request
     * @param currPage
     * @param pageSize
     * @return
     */
    CommonResponse findOrderList(OrderRequest request, Integer currPage, Integer pageSize);

    /**
     * 待审核 + 分页
     * @param currPage
     * @param pageSize
     * @return
     */
    CommonResponse checkPendingList(Integer currPage, Integer pageSize);

    /**
     * 充值
     * @param businessOrder
     * @throws AlipayApiException
     */
    void recharge(BusinessOrder businessOrder) throws AlipayApiException;

    /**
     * 开票
     * @param id
     */
    void openInvoice(String id);

    /**
     * 每月消费金额
     * @return
     */
    Map<String, BigDecimal> accountStatics();

    /**
     * 每月订单金额
     * @return
     */
    Map<String, BigDecimal> orderMoney();
}
