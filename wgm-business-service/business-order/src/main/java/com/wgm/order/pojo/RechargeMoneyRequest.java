package com.wgm.order.pojo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/20 16:44
 */
@Data
public class RechargeMoneyRequest {

    /**
     * 充值金额
     */
    private BigDecimal totalAmount;
    /**
     * 公司id
     */
    private String companyId;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 账户id
     */
    private String accountId;
}
