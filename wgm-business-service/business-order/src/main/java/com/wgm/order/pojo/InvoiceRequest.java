package com.wgm.order.pojo;

import lombok.Data;

/**
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/20 16:44
 */
@Data
public class InvoiceRequest {

    /**
     * 发票类型 (0-专用,1-普通)
     */
    private Integer type;
    /**
     * 发票状态 (0-待审核,1-审核通过,2-审核未通过)
     */
    private Integer status;
    /**
     * 发票申请时间
     */
    private String applyTime;

}
