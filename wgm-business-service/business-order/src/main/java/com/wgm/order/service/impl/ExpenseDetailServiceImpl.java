package com.wgm.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.constant.CommonStatus;
import com.wgm.common.utils.PageUtils;
import com.wgm.order.dao.ExpenseDetailDao;
import com.wgm.order.entity.ExpenseDetail;
import com.wgm.order.pojo.ExpenseRequest;
import com.wgm.order.service.ExpenseDetailService;
import com.wgm.order.vo.ExpenseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/20 16:44
 */
@Service
public class ExpenseDetailServiceImpl extends ServiceImpl<ExpenseDetailDao, ExpenseDetail> implements ExpenseDetailService {

    @Autowired
    private ExpenseDetailDao detailDao;

    /**
     * 消费明细 + 分页+ 模糊
     * @param request
     * @param currPage 当前页数
     * @param pageSize 每页记录数
     * @return
     */
    @Override
    public CommonResponse findList(ExpenseRequest request, Integer currPage, Integer pageSize) {
//        查询所有消费信息
        List<ExpenseResponse> detailList =  detailDao.findList(request.getExpenseTime(), (currPage-1)*pageSize, pageSize);
//        总记录数
        int total = detailDao.getCount(request.getExpenseTime());
//        分页
        PageUtils pageUtils = new PageUtils(detailList, total, pageSize, currPage);
        return new CommonResponse(CommonStatus.VALID,pageUtils);
    }
}