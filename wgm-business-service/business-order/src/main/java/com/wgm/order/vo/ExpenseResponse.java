package com.wgm.order.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 消费明细
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/20 15:49
 */
@Data
public class ExpenseResponse {

    /**
     * id
     */
    private String id;
    /**
     * 数据块名称
     */
    private String dataBlockName;
    /**
     * 时间周期/天
     */
    private String timePeriod;
    /**
     * 金额/元
     */
    private BigDecimal amount;
    /**
     * 消费时间
     */
    private Date expenseTime;
}
