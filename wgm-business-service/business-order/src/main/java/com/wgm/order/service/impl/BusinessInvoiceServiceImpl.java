package com.wgm.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.constant.CommonStatus;
import com.wgm.common.utils.PageUtils;
import com.wgm.order.dao.BusinessInvoiceDao;
import com.wgm.order.entity.BusinessInvoice;
import com.wgm.order.pojo.InvoiceRequest;
import com.wgm.order.service.BusinessInvoiceService;
import com.wgm.order.vo.InvoiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/20 16:44
 */
@Service
public class BusinessInvoiceServiceImpl extends ServiceImpl<BusinessInvoiceDao, BusinessInvoice> implements BusinessInvoiceService {

    @Autowired
    private BusinessInvoiceDao invoiceDao;

    /**
     * 前台充值明细 + 分页 + 模糊
     * @param request
     * @param currPage 当前页数
     * @param pageSize 每页记录数
     * @return
     */
    @Override
    public CommonResponse findList(InvoiceRequest request, Integer currPage, Integer pageSize) {
//        查询 前台充值明细
        List<InvoiceResponse> invoiceList = invoiceDao.findList(request.getType(),request.getStatus(),request.getApplyTime(),
                                            (currPage-1)*pageSize,pageSize);
//        判断
//        查询总记录数
        int total = invoiceDao.getCount(request.getType(),request.getStatus(),request.getApplyTime());
//        分页
        PageUtils pageUtils = new PageUtils(invoiceList, total, pageSize, currPage);
        return new CommonResponse(CommonStatus.VALID,pageUtils);
    }

}