package com.wgm.order;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 〈启动类〉<br>
 *
 * @author cgxu
 * 2021/1/20
 */
@SpringBootApplication
@EnableTransactionManagement
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.wgm.order.feign")
@MapperScan("com.wgm.order.dao")
public class WgmOrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(WgmOrderApplication.class);
    }
}
