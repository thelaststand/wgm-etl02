package com.wgm.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonStatus;
import com.wgm.common.exception.AdException;
import com.wgm.order.dao.AccountInformationDao;
import com.wgm.order.entity.AccountInformation;
import com.wgm.order.pojo.AccountRequest;
import com.wgm.order.service.AccountInformationService;
import com.wgm.order.util.SendSmsUtil;
import com.wgm.order.vo.AccountResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/20 16:44
 */
@Service
public class AccountInformationServiceImpl extends ServiceImpl<AccountInformationDao, AccountInformation> implements AccountInformationService {

    @Autowired
    private AccountInformationDao informationDao;


    /**
     * 查询对应账号
     * @param id
     * @return
     */
    @Override
    public AccountResponse findById(String id) {
//        查询
        AccountInformation byId = this.baseMapper.findById(id);
        AccountResponse response = new AccountResponse();
        BeanUtils.copyProperties(byId,response);
        return response;
    }

    /**
     * 关闭预警
     * @param id
     */
    @Override
    public void shutWarning(String id) {
//        修改预警状态 为禁用 0
        informationDao.shutWarning(id);
    }

    /**
     * 开启预警
     * @param request
     */
    @Override
    public void openWarning(AccountRequest request) {
        if(StringUtils.isEmpty(request.getId())){
            throw new AdException(CommonConstants.Account.ACCOUNT_ID_NULL);
        }
        if(StringUtils.isEmpty(request.getWarningThreshould())){
            throw new AdException(CommonConstants.Account.WARNING_THRESHOULD_NULL);
        }
        if(StringUtils.isEmpty(request.getWarningPhone())){
            throw new AdException(CommonConstants.Account.WARNING_PHONE_NULL);
        }
//        查询账户信息
        AccountInformation byId = informationDao.findById(request.getId());
//        比较账户的余额 与 预警阈值
        if(byId.getBalance().doubleValue() <  request.getWarningThreshould().doubleValue() && CommonStatus.DISABLED_ENUM.getCode().equals(byId.getWarningStatus())){
//            发送短信
            SendSmsUtil.sendSms(request.getWarningPhone());
        }
//        修改预警状态 为开启 1
        informationDao.openWarning(request.getId());
    }

    /**
     * 查询所有账户信息
     * @return
     */
    @Override
    public List<AccountInformation> findList() {
        return informationDao.findList();
    }
}