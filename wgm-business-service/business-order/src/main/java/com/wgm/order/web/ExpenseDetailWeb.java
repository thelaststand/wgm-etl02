package com.wgm.order.web;

import com.wgm.common.constant.CommonResponse;
import com.wgm.order.pojo.ExpenseRequest;
import com.wgm.order.service.ExpenseDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 消费明细
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/20 18:38
 */
@RestController
@RequestMapping("/web/expense/detail")
public class ExpenseDetailWeb {

    @Autowired
    private ExpenseDetailService detailService;

    /**
     * 消费明细 + 模糊 + 分页
     * @param request 消费时间
     * @param currPage 当前页数
     * @param pageSize 每页记录数
     * @return
     */
    @PostMapping("/findList")
    public CommonResponse findList(@RequestBody ExpenseRequest request,
                                   @RequestParam(value = "currPage",defaultValue = "1") Integer currPage,
                                   @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize){
        return detailService.findList(request, currPage, pageSize);
    }

}
