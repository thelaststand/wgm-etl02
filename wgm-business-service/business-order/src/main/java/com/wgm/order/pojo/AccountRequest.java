package com.wgm.order.pojo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/20 16:44
 */
@Data
public class AccountRequest {

    /**
     *
     */
    private String id;
    /**
     * 预警阈值
     */
    private BigDecimal warningThreshould;
    /**
     * 预警号码
     */
    private String warningPhone;
}
