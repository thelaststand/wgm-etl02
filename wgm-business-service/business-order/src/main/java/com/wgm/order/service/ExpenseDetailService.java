package com.wgm.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.common.constant.CommonResponse;
import com.wgm.order.entity.ExpenseDetail;
import com.wgm.order.pojo.ExpenseRequest;

/**
 * 消费明细
 *
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-20 18:46:42
 */
public interface ExpenseDetailService extends IService<ExpenseDetail> {

    /**
     * 消费明细 + 分页+ 模糊
     * @param request
     * @param currPage
     * @param pageSize
     * @return
     */
    CommonResponse findList(ExpenseRequest request, Integer currPage, Integer pageSize);
}

