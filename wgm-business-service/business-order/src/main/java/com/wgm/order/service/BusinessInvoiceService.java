package com.wgm.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.common.constant.CommonResponse;
import com.wgm.order.entity.BusinessInvoice;
import com.wgm.order.pojo.InvoiceRequest;

/**
 * 充值记录
 *
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-20 19:36:48
 */
public interface BusinessInvoiceService extends IService<BusinessInvoice> {

    /**
     * 发票申请记录 + 分页 + 模糊
     * @param request
     * @param currPage
     * @param pageSize
     * @return
     */
    CommonResponse findList(InvoiceRequest request, Integer currPage, Integer pageSize);

}

