package com.wgm.order.pojo;

import lombok.Data;

/**
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/20 16:44
 */
@Data
public class OrderRequest {

    /**
     * 开始时间
     */
    private String startTime;
    /**
     * 结束时间
     */
    private String endTime;
    /**
     * 订单编号
     */
    private String orderSn;
}
