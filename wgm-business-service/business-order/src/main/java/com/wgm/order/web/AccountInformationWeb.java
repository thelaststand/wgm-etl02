package com.wgm.order.web;

import com.alibaba.fastjson.JSON;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.constant.CommonStatus;
import com.wgm.order.entity.AccountInformation;
import com.wgm.order.pojo.AccountRequest;
import com.wgm.order.service.AccountInformationService;
import com.wgm.order.vo.AccountResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 账户信息
 *
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-22 10:24:27
 */
@RestController
@RequestMapping("/web/account/information")
@Slf4j
public class AccountInformationWeb {

    @Autowired
    private AccountInformationService informationService;

    /**
     * 查询所有账户信息
     * @return
     */
    @GetMapping("/findList")
    public CommonResponse findList(){
        List<AccountInformation> data = informationService.findList();
        return CommonResponse.ok(data);
    }

    /**
     * 查询对应账号
     * @param id
     * @return
     */
    @GetMapping("/findById/{id}")
    public CommonResponse findById(@PathVariable("id") String id){
        log.info("account-information:findById->{}", JSON.toJSONString(id));
        AccountResponse data = informationService.findById(id);
        return CommonResponse.ok(data);
    }

    /**
     * 开启预警
     * @param request
     * @return
     */
    @PostMapping("/openWarning")
    public CommonResponse openWarning(@RequestBody AccountRequest request){
        informationService.openWarning(request);
        return new CommonResponse(CommonStatus.VALID);
    }

    /**
     * 关闭预警
     * @param id 账号编号
     * @return
     */
    @GetMapping("/shutWarning/{id}")
    public CommonResponse shutWarning(@PathVariable("id") String id){
        informationService.shutWarning(id);
        return CommonResponse.ok();
    }

}
