package com.wgm.order.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/20 15:49
 */
@Data
public class RechargeResponse {

    /**
     *
     */
    private String id;
    /**
     * 订单编号
     */
    private String orderSn;
    /**
     * 充值方式
     */
    private String rechargeType;
    /**
     * 交易金额
     */
    private BigDecimal totalAmount;
    /**
     * 充值时间
     */
    private Date rechargeTime;
}
