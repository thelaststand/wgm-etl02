package com.wgm.order.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/20 15:51
 */
@Data
public class InvoiceResponse {

    /**
     *
     */
    private String id;
    /**
     * 发票抬头
     */
    private String title;
    /**
     * 发票类型 (0-专用,1-普通)
     */
    private Integer type;
    /**
     * 发票金额/元
     */
    private BigDecimal amount;
    /**
     * 发票状态 (0-待审核,1-审核通过,2-审核未通过)
     */
    private Integer status;
    /**
     * 发票申请时间
     */
    private Date applyTime;
}
