package com.wgm.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.account.entity.BusinessCompanyInfoEntity;
import com.wgm.account.entity.MallUserEntity;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.constant.CommonStatus;
import com.wgm.common.exception.AdException;
import com.wgm.common.utils.PageUtils;
import com.wgm.common.utils.UuidUtils;
import com.wgm.order.alipay.AlipayConfig;
import com.wgm.order.dao.AccountInformationDao;
import com.wgm.order.dao.BusinessOrderDao;
import com.wgm.order.dao.ExpenseDetailDao;
import com.wgm.order.entity.AccountInformation;
import com.wgm.order.entity.BusinessOrder;
import com.wgm.order.entity.ExpenseDetail;
import com.wgm.order.feign.FeignUserService;
import com.wgm.order.pojo.OrderRequest;
import com.wgm.order.pojo.RechargeRequest;
import com.wgm.order.service.BusinessOrderService;
import com.wgm.order.util.OrderUtils;
import com.wgm.order.vo.CheckPendingResponse;
import com.wgm.order.vo.OrderResponse;
import com.wgm.order.vo.RechargeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/20 16:44
 */
@Service
public class BusinessOrderServiceImpl extends ServiceImpl<BusinessOrderDao, BusinessOrder> implements BusinessOrderService {

    @Autowired
    private BusinessOrderDao orderDao;

    @Autowired
    private AlipayConfig alipayConfig;

    @Resource
    private AccountInformationDao informationDao;

    @Autowired
    private FeignUserService userService;

    @Resource
    private ExpenseDetailDao detailDao;

    /**
     * 查询 充值明细 分页 + 模糊
     * @param request 充值时间
     * @param currPage 当前页数
     * @param pageSize 每页记录数
     * @return
     */
    @Override
    public CommonResponse findList(RechargeRequest request, Integer currPage, Integer pageSize) {
//        查询 前台充值明细
        List<RechargeResponse> orderList = orderDao.findList(request.getRechargeTime(),(currPage-1)*pageSize,pageSize);
//        查询总记录数
        int total = orderDao.getCount(request.getRechargeTime());
//        分页
        PageUtils pageUtils = new PageUtils(orderList, total, pageSize, currPage);
        return new CommonResponse(CommonStatus.VALID,pageUtils);
    }

    /**
     * 查询 订单 分页 + 模糊
     * @param request 支付时间
     * @param currPage 当前页数
     * @param pageSize 每页记录数
     * @return
     */
    @Override
    public CommonResponse findOrderList(OrderRequest request, Integer currPage, Integer pageSize) {
//        查询 后台订单
        List<OrderResponse> orderList = orderDao.findOrderList(request.getStartTime(),request.getEndTime(),request.getOrderSn(), (currPage-1)*pageSize, pageSize);
//        查询总记录数
        int total = orderDao.getOrderCount(request.getStartTime(),request.getEndTime(),request.getOrderSn());
//        分页
        PageUtils pageUtils = new PageUtils(orderList, total, pageSize, currPage);
        return new CommonResponse(CommonStatus.VALID,pageUtils);
    }

    /**
     * 待审核 + 分页 + 模糊
     * @param currPage 当前页数
     * @param pageSize 每页记录数
     * @return
     */
    @Override
    public CommonResponse checkPendingList(Integer currPage, Integer pageSize) {
//        查询待审核列表
        List<CheckPendingResponse> checklist = orderDao.checkPendingList(currPage, pageSize);
//        查询总记录数
        int total = orderDao.getCheckCount();
//        分页
        PageUtils pageUtils = new PageUtils(checklist, total, pageSize, currPage);
        return new CommonResponse(CommonStatus.VALID,pageUtils);
    }

    /**
     * 开票
     * @param id
     */
    @Override
    public void openInvoice(String id) {
        orderDao.openInvoice(id);
    }

    /**
     * 账户充值/添加充值记录
     * @param businessOrder
     * @return
     */
    @Override
    public void recharge(BusinessOrder businessOrder){
        if(StringUtils.isEmpty(businessOrder.getUserId())){
            throw new AdException(CommonConstants.Recharge.USER_ID_NULL);
        }
        if(StringUtils.isEmpty(businessOrder.getAccountId())){
            throw new AdException(CommonConstants.Recharge.ACCOUNT_ID_NULL);
        }
        if(StringUtils.isEmpty(businessOrder.getTotalAmount())){
            throw new AdException(CommonConstants.Recharge.AMOUNT_ID_NULL);
        }
        if(StringUtils.isEmpty(businessOrder.getCompanyId())){
            throw new AdException(CommonConstants.Recharge.COMPANY_ID_NULL);
        }

        System.out.println(businessOrder.getUserId());
//            查询用户信息
        Object obj = userService.findById(businessOrder.getUserId()).getData();
        MallUserEntity user = JSON.parseObject(JSONObject.toJSON(obj).toString(),MallUserEntity.class);
        System.out.println(user);
        businessOrder.setUsername(user.getName());
        businessOrder.setPhone(user.getPhone());
//            查询公司信息
        Object data = userService.findCompanyById(businessOrder.getCompanyId()).getData();
        BusinessCompanyInfoEntity company = JSON.parseObject(JSONObject.toJSON(data).toString(),BusinessCompanyInfoEntity.class);
        businessOrder.setCompanyName(company.getEnterpriseName());
        String accountId = businessOrder.getAccountId();
        String rechargeId = OrderUtils.getOrderCode(Integer.parseInt(accountId));
        businessOrder.setOrderSn(UuidUtils.generateUuid());
        businessOrder.setId(UuidUtils.generateUuid());
        businessOrder.setRechargeType("支付宝");
        businessOrder.setRechargeTime(new Date());
        businessOrder.setStatus(0);
        businessOrder.setInvoiceStatus(0);
        String pay = null;
        try {
            pay = pay(rechargeId, "支付宝", String.valueOf(businessOrder.getTotalAmount()), accountId);
            System.out.println("生成表单:"+pay);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
//            添加一条充值信息
        orderDao.addRecharge(businessOrder);
        //更新当前账户余额
          AccountInformation account = informationDao.findById(businessOrder.getAccountId());
        account.setBalance(new BigDecimal(account.getBalance().doubleValue()+businessOrder.getTotalAmount().doubleValue()));
//            更新账户余额
        informationDao.update(account);

    }

    /**
     * 支付宝支付接口
     */
    public String pay(@RequestParam("rechargeId") String rechargeId,
                      @RequestParam("tradingName") String tradingName,
                      @RequestParam("tradingMoney") String tradingMoney,
                      @RequestParam("id") String id) throws AlipayApiException {
        /**
         * 1.封装RSA签名方式
         * gatewayUrl支付宝网关
         * app_id当前支付宝账户编号
         * merchant_private_key商户私钥
         * format格式化json
         * charset字符编码 utf-8
         * alipay_public_key支付宝公钥
         * sign_type支付宝签名方式 RSA2
         */
        AlipayClient alipayClient = new DefaultAlipayClient(
                AlipayConfig.gatewayUrl,
                alipayConfig.appId,
                alipayConfig.merchantPrivateKey,
                AlipayConfig.format,
                AlipayConfig.charset,
                alipayConfig.alipayPublicKey,
                AlipayConfig.sign_type);
        //2.创建Request请求
        AlipayTradeWapPayRequest request = new AlipayTradeWapPayRequest();
        //3.封装对象   封装页面传递过来的参数
        AlipayTradeWapPayModel model = new AlipayTradeWapPayModel();
        model.setOutTradeNo(rechargeId);
        model.setSubject(tradingName);
        model.setTotalAmount(tradingMoney);
        model.setProductCode(id);
        request.setBizModel(model);
        //设置异步回调地址
        request.setNotifyUrl(AlipayConfig.notify_url);
        //设置同步回调地址
        request.setReturnUrl(AlipayConfig.return_url);
        //生成表单
        String body = alipayClient.pageExecute(request).getBody();
        return body;
    }

    /**
     * 每月消费金额
     * @return
     */
    @Override
    public Map<String, BigDecimal> accountStatics() {
        Map<String,BigDecimal> map = new HashMap<>(16);
        //获取当前月份和年份
        int month = Calendar.getInstance().get(Calendar.MONTH) + CommonConstants.ONE;
        int year = Calendar.getInstance().get(Calendar.YEAR);
        //循环十二次 只获取一年的财务统计
        for (int i = CommonConstants.TWELVE; i > CommonConstants.ZERO; i--) {
            BigDecimal sum1 = new BigDecimal(0);
            //如果是个位月份  就+0
            if(month>0 && month <10){
                //根据月份查询到当前信息
                List<ExpenseDetail> account = detailDao.selectAccount(year+"-0"+month);
                //循环获取消费金额 相加
                for (ExpenseDetail expense : account) {
                    sum1 = expense.getAmount().add(sum1);
                }
                //十位月份 不+0
            }else{
                List<ExpenseDetail> account = detailDao.selectAccount(year+"-"+month);
                for (ExpenseDetail expense : account) {
                    sum1 = expense.getAmount().add(sum1);
                }
            }
            //数据使用map集合存储  键为年月 值为当月的总额
            map.put(year+"-"+month,sum1);
            month--;
            if(month == 0){
                month = 12;
                year = year-1;
            }
        }
        return map;
    }

    /**
     * 每月订单金额
     * @return
     */
    @Override
    public Map<String, BigDecimal> orderMoney() {
        Map<String,BigDecimal> map = new HashMap<>(16);
        //获取当前月份和年份
        int month = Calendar.getInstance().get(Calendar.MONTH) + CommonConstants.ONE;
        int year = Calendar.getInstance().get(Calendar.YEAR);
        //循环六次 只获取最近六个月的订单
        for (int i = CommonConstants.SIX; i > CommonConstants.ZERO; i--) {
            BigDecimal sum1 = new BigDecimal(0);
            //如果是个位月份  就+0
            if(month>0 && month <10){
                //根据月份查询到当前信息
                List<BusinessOrder> account = orderDao.selectOrderAccount(year+"-0"+month);
                //循环获取订单金额 相加
                for (BusinessOrder order : account) {
                    sum1 = order.getTotalAmount().add(sum1);
                }
                //十位月份 不+0
            }else{
                List<BusinessOrder> account = orderDao.selectOrderAccount(year+"-"+month);
                for (BusinessOrder order : account) {
                    sum1 = order.getTotalAmount().add(sum1);
                }
            }
            //数据使用map集合存储  键为年月 值为当月的总额
            map.put(year+"-"+month,sum1);
            month--;
            if(month == 0){
                month = 12;
                year = year-1;
            }
        }
        return map;
    }


}