package com.wgm.order.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 待审核
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/20 15:51
 */
@Data
public class CheckPendingResponse {

    /**
     *
     */
    private String id;
    /**
     * 订单编号
     */
    private String orderSn;
    /**
     * 交易金额
     */
    private BigDecimal totalAmount;
    /**
     * 用户名
     */
    private String username;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 支付时间
     */
    private Date rechargeTime;
    /**
     * 开票状态 (0-未开票,1-已开票)
     */
    private Integer invoiceStatus;
}
