package com.wgm.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.order.entity.AccountInformation;
import com.wgm.order.pojo.AccountRequest;
import com.wgm.order.vo.AccountResponse;

import java.util.List;

/**
 * 账户信息
 *
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-22 10:24:27
 */
public interface AccountInformationService extends IService<AccountInformation> {

    /**
     * 查询对应账号
     * @param id
     * @return
     */
    AccountResponse findById(String id);
    /**
     * 关闭预警
     * @param id 账号编号
     * @return
     */
    void shutWarning(String id);
    /**
     * 开启预警
     * @param request
     * @return
     */
    void openWarning(AccountRequest request);
    /**
     * 查询所有账户信息
     * @return
     */
    List<AccountInformation> findList();
}

