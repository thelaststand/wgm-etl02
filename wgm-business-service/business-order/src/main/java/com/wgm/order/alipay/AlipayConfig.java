package com.wgm.order.alipay;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;


/**
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-22 10:24:27
 */
@Component
public class AlipayConfig implements Serializable, InitializingBean {
    /**
     * 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
     */
    @Value("${app_id}")
    public String appId;

    /**
     * 商户私钥，您的PKCS8格式RSA2私钥
     */
    @Value("${merchant_private_key}")
    public String merchantPrivateKey;

    /**
     * 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
     */
    @Value("${alipay_public_key}")
    public String alipayPublicKey;

    /**
     * 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
     */
    public static String notify_url = "https://www.baidu.com";

    /**
     * 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
     */
    public static String return_url = "https://www.baidu.com";

    /**
     * 签名方式
     */
    public static String sign_type = "RSA2";

    /**
     * 字符编码
     */
    public static String charset = "utf-8";

    /**
     * 返回格式
     */
    public static String format="json";

    /**
     * 支付宝网关
     */
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";


    public static String APP_ID;
    public static String MERCHANT_PRIVATE_KEY;
    public static String ALIPAY_PUBLIC_KEY;

    @Override
    public void afterPropertiesSet() throws Exception {
        APP_ID=appId;
        MERCHANT_PRIVATE_KEY=merchantPrivateKey;
        ALIPAY_PUBLIC_KEY=alipayPublicKey;
    }
}
