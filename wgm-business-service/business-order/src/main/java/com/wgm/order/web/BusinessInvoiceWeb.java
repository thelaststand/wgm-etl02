package com.wgm.order.web;

import com.wgm.common.constant.CommonResponse;
import com.wgm.order.pojo.InvoiceRequest;
import com.wgm.order.service.BusinessInvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 *
 * 发票申请记录
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-20 19:36:48
 */
@RestController
@RequestMapping("/web/business/invoice")
public class BusinessInvoiceWeb {

    @Autowired
    private BusinessInvoiceService invoiceService;

    /**
     * 前台 发票申请记录 + 模糊 + 分页
     * @param request 充值时间
     * @param currPage 当前页数
     * @param pageSize 每页记录数
     * @return
     */
    @PostMapping("/findList")
    public CommonResponse findList(@RequestBody InvoiceRequest request,
                                   @RequestParam(value = "currPage",defaultValue = "1") Integer currPage,
                                   @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize){
        return invoiceService.findList(request,currPage,pageSize);
    }
}
