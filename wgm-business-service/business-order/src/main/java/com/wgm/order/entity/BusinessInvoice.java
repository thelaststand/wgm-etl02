package com.wgm.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 充值记录
 * 
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-20 19:36:48
 */
@Data
@TableName("business_invoice")
public class BusinessInvoice implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String id;
	/**
	 * 用户id
	 */
	private String userId;
	/**
	 * 发票抬头
	 */
	private String title;
	/**
	 * 发票类型 (0-专用,1-普通)
	 */
	private Integer type;
	/**
	 * 发票金额/元
	 */
	private BigDecimal amount;
	/**
	 * 发票状态 (0-待审核,1-审核通过,2-审核未通过)
	 */
	private Integer status;
	/**
	 * 发票申请时间
	 */
	private Date applyTime;

}
