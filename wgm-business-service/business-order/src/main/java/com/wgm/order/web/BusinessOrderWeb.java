package com.wgm.order.web;

import com.alipay.api.AlipayApiException;
import com.wgm.common.constant.CommonResponse;
import com.wgm.order.entity.BusinessOrder;
import com.wgm.order.pojo.RechargeMoneyRequest;
import com.wgm.order.pojo.RechargeRequest;
import com.wgm.order.service.BusinessOrderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Map;


/**
 * 订单
 *
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-20 11:46:58
 */
@RestController
@RequestMapping("/web/business/order")
public class BusinessOrderWeb {

    @Autowired
    private BusinessOrderService orderService;

    /**
     * 前台 查询充值明细 + 模糊 + 分页
     * @param request 充值时间
     * @param currPage 当前页数
     * @param pageSize 每页记录数
     * @return
     */
    @PostMapping("/findList")
    public CommonResponse findList(@RequestBody RechargeRequest request,
                                   @RequestParam(value = "currPage",defaultValue = "1") Integer currPage,
                                   @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize){
        return orderService.findList(request,currPage,pageSize);
    }

    /**
     * 开票
     * @param id
     * @return
     */
    @GetMapping("/openInvoice/{id}")
    public CommonResponse openInvoice(@PathVariable("id") String id){
        orderService.openInvoice(id);
        return CommonResponse.ok();
    }

    /**
     * 充值
     * @param request
     * @return
     * @throws AlipayApiException
     */
    @PostMapping("/recharge")
    public CommonResponse recharge(@RequestBody RechargeMoneyRequest request) throws AlipayApiException {
        BusinessOrder businessOrder = new BusinessOrder();
        BeanUtils.copyProperties(request,businessOrder);
        orderService.recharge(businessOrder);
        return CommonResponse.ok(businessOrder);
    }

    /**
     * 每月消费金额
     * @return
     */
    @GetMapping("/accountStatics")
    public CommonResponse accountStatics(){
        Map<String, BigDecimal> data  = orderService.accountStatics();
        return CommonResponse.ok(data);
    }


}
