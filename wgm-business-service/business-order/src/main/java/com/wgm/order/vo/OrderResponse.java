package com.wgm.order.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/20 15:51
 */
@Data
public class OrderResponse {

    /**
     *
     */
    private String id;
    /**
     * 订单编号
     */
    private String orderSn;
    /**
     * 充值方式
     */
    private String rechargeType;
    /**
     * 交易金额
     */
    private BigDecimal totalAmount;
    /**
     * 用户名
     */
    private String username;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 支付时间
     */
    private Date rechargeTime;
}
