package com.wgm.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.order.entity.AccountInformation;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 账户信息
 * 
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-22 10:24:27
 */
@Mapper
public interface AccountInformationDao extends BaseMapper<AccountInformation> {

    /**
     * 查询对应账号
     * @param id
     * @return
     */
    AccountInformation findById(String id);

    /**
     * 关闭预警
     * @param id
     */
    void shutWarning(String id);

    /**
     * 开启预警
     * @param id
     */
    void openWarning(String id);

    /**
     * 查询账户信息
     * @return
     */
    List<AccountInformation> findList();

    /**
     * 修改账户余额
     * @param account
     */
    void update(AccountInformation account);
}
