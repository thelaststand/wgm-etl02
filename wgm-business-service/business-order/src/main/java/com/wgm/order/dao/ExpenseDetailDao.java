package com.wgm.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.order.entity.ExpenseDetail;
import com.wgm.order.vo.ExpenseResponse;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 消费明细
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-20 11:46:58
 */
@Mapper
@Component
public interface ExpenseDetailDao extends BaseMapper<ExpenseDetail> {

    /**
     * 消费明细 + 分页 + 模糊
     * @param expenseTime 消费时间
     * @param currPage 当前页数
     * @param pageSize 每页记录数
     * @return
     */
    List<ExpenseResponse> findList(@Param("expenseTime") String expenseTime,
                                   @Param("currPage") Integer currPage,
                                   @Param("pageSize") Integer pageSize);

    /**
     * 总数
     * @param expenseTime 消费时间
     * @return
     */
    int getCount(String expenseTime);

    /**
     * 导出 当前人的消费明细  excel
     * @param userId
     * @return
     */
    List<ExpenseDetail> findUserId(String userId);

    /**
     *
     * @param time
     * @return
     */
    List<ExpenseDetail> selectAccount(String time);
}
