package com.wgm.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.order.entity.BusinessOrder;
import com.wgm.order.entity.ExpenseDetail;
import com.wgm.order.vo.CheckPendingResponse;
import com.wgm.order.vo.OrderResponse;
import com.wgm.order.vo.RechargeResponse;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 订单
 * 
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-20 11:46:58
 */
@Mapper
@Component
public interface BusinessOrderDao extends BaseMapper<BusinessOrder> {

    /**
     * 前台 充值明细 + 分页 + 模糊
     * @param rechargeTime 充值时间
     * @param currPage 当前页数
     * @param pageSize 每页记录数
     * @return
     */
    List<RechargeResponse> findList(@Param("rechargeTime") String rechargeTime,
                                    @Param("currPage") Integer currPage,
                                    @Param("pageSize") Integer pageSize);

    /**
     * 充值明细总数
     * @param rechargeTime
     * @return
     */
    int getCount(String rechargeTime);

    /**
     * 订单列表 + 分页+ 模糊
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @param orderSn 订单编号
     * @param currPage 当前页数
     * @param pageSize 每页记录数
     * @return
     */
    List<OrderResponse> findOrderList(@Param("startTime") String startTime,
                                      @Param("endTime") String endTime,
                                      @Param("orderSn") String orderSn,
                                      @Param("currPage") Integer currPage,
                                      @Param("pageSize") Integer pageSize);

    /**
     * 订单列表总数
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @param orderSn 订单编号
     * @return
     */
    int getOrderCount(@Param("startTime") String startTime,
                      @Param("endTime") String endTime,
                      @Param("orderSn") String orderSn);

    /**
     * 待审核 + 分页
     * @param currPage 当前页数
     * @param pageSize 每页记录数
     * @return
     */
    List<CheckPendingResponse> checkPendingList(@Param("currPage") Integer currPage,
                                                @Param("pageSize") Integer pageSize);

    /**
     * 待审核总数
     * @return
     */
    int getCheckCount();

    /**
     * 根据id查询对象
     * @param id
     * @return
     */
    List<BusinessOrder> getAllbusinessOrders(String id);

    /**
     * 开票
     * @param id
     */
    void openInvoice(String id);

    /**
     * 充值信息
     * @param businessOrder
     */
    void addRecharge(BusinessOrder businessOrder);

    /**
     * 每月消费金额
     * @param time
     * @return
     */
    List<ExpenseDetail> selectAccount(String time);

    /**
     * 近半年的订单
     * @param time
     * @return
     */
    List<BusinessOrder> selectOrderAccount(String time);
}
