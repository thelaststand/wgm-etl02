package com.wgm.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.order.entity.BusinessInvoice;
import com.wgm.order.vo.InvoiceResponse;
import com.wgm.order.vo.OrderResponse;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 充值记录
 * 
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-20 19:36:48
 */
@Mapper
public interface BusinessInvoiceDao extends BaseMapper<BusinessInvoice> {

    /**
     * 发票申请记录 + 分页 + 模糊
     * @param type 类型
     * @param status 转台
     * @param applyTime 支付时间
     * @param currPage 当前转台
     * @param pageSize 每页记录数
     * @return
     */
    List<InvoiceResponse> findList(@Param("type") Integer type,
                                   @Param("status") Integer status,
                                   @Param("applyTime") String applyTime,
                                   @Param("currPage") Integer currPage,
                                   @Param("pageSize") Integer pageSize);

    /**
     * 总数
     * @param type 类型
     * @param status 状态
     * @param applyTime 支付时间
     * @return
     */
    int getCount(@Param("type") Integer type,
                 @Param("status") Integer status,
                 @Param("applyTime") String applyTime);

    /**
     * 根据Id查询对象
     * @param id
     * @return
     */
    List<BusinessInvoice> getAllbusinessInvoice(@Param("id") String id);
}
