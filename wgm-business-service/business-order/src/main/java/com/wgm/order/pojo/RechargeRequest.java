package com.wgm.order.pojo;

import lombok.Data;

/**
 * @author chenyile
 * @email 2334665134@qq.com
 * @Date 2021/1/20 16:44
 */
@Data
public class RechargeRequest {

    /**
     * 充值时间
     */
    private String rechargeTime;
}
