package com.wgm.order.controller;

import com.wgm.common.constant.CommonResponse;
import com.wgm.order.pojo.OrderRequest;
import com.wgm.order.service.BusinessOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Map;


/**
 * 订单
 *
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-20 11:46:58
 */
@RestController
@RequestMapping("/controller/business/order")
public class BusinessOrderController {

    @Autowired
    private BusinessOrderService orderService;

    /**
     * 后台 查询订单 + 模糊 + 分页
     * @param request 订单时间
     * @param currPage 当前页数
     * @param pageSize 每页记录数
     * @return
     */
    @PostMapping("/findOrderList")
    public CommonResponse findOrderList(@RequestBody OrderRequest request,
                                        @RequestParam(value = "currPage",defaultValue = "1") Integer currPage,
                                        @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize){
        return orderService.findOrderList(request, currPage, pageSize);
    }

    /**
     * 待审核 + 分页
     * @param currPage
     * @param pageSize
     * @return
     */
    @GetMapping("/checkPendingList")
    public CommonResponse checkPendingList(@RequestParam(value = "currPage",defaultValue = "1") Integer currPage,
                                           @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize){
        return orderService.checkPendingList(currPage, pageSize);
    }

    /**
     * 近半年订单金额
     * @return
     */
    @GetMapping("/orderMoney")
    public CommonResponse orderMoney(){
        Map<String, BigDecimal> data  = orderService.orderMoney();
        return CommonResponse.ok(data);
    }

}
