package com.wgm.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 消费明细
 * 
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-20 18:46:42
 */
@Data
@TableName("wgm_expense_detail")
@AllArgsConstructor
@NoArgsConstructor
public class ExpenseDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 序号
	 */
	@TableId
	private String id;
	/**
	 * 用户id
	 */
	private String userId;
	/**
	 * 数据块名称
	 */
	private String dataBlockName;
	/**
	 * 时间周期/天
	 */
	private String timePeriod;
	/**
	 * 金额/元
	 */
	private BigDecimal amount;
	/**
	 * 消费时间
	 */
	private Date expenseTime;

}
