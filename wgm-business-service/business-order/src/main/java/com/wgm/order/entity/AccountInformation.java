package com.wgm.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 账户信息
 * 
 * @author chenyile
 * @email chenyile@gmail.com
 * @date 2021-01-22 10:24:27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("account_information")
public class AccountInformation implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String id;
	/**
	 * 预警状态 （0-禁用,1-开启）
	 */
	private String warningStatus;
	/**
	 * 余额
	 */
	private BigDecimal balance;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;

}
