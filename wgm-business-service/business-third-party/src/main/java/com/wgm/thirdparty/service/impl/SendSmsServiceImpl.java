package com.wgm.thirdparty.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.wgm.thirdparty.service.SendSmsService;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * @author songhuan
 * @email 3325074245@qq.com
 * @date 2021/01/20
 */
@Service
public class SendSmsServiceImpl implements SendSmsService {
    private static String REGIONID = "cn-hangzhou";
    private static String SignName = "宋氏学习网站";
    private static String TemplateCode = "SMS_206737755";
    private static String AccessKeyId = "LTAI4GBJrauaGBRqNcjbUtbJ";
    private static String Secret = "rFhMiXCXMpUOaNBmGnSwrvPhTLKo8Y";

    @Override
    public Boolean send(String phone, HashMap<String, Object> code) {
        DefaultProfile profile = DefaultProfile.getProfile(REGIONID, AccessKeyId, Secret);
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", REGIONID);
        request.putQueryParameter("PhoneNumbers", phone);
        request.putQueryParameter("SignName", SignName);
        request.putQueryParameter("TemplateCode", TemplateCode);
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(code));

        try {
            CommonResponse response = client.getCommonResponse(request);
            return response.getHttpResponse().isSuccess();
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return false;
    }
}
