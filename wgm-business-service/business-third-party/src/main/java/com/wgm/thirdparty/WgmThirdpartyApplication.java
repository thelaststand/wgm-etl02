package com.wgm.thirdparty;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author songhuan
 * @email 3325074245@qq.com
 * @date 2021/01/20
 */
@SpringBootApplication
@EnableTransactionManagement
@EnableDiscoveryClient
@EnableFeignClients
public class WgmThirdpartyApplication {
    public static void main(String[] args) {
        SpringApplication.run(WgmThirdpartyApplication.class,args);
    }
}
