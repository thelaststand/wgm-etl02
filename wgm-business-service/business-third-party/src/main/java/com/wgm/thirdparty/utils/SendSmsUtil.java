package com.wgm.thirdparty.utils;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author songhuan
 * @email 3325074245@qq.com
 * @date 2021/01/20
 */
public class SendSmsUtil {


    private static String REGIONID = "cn-hangzhou";
    private static String SignName = "宋氏学习网站";
    private static String TemplateCode = "SMS_206737755";
    private static String AccessKeyId = "LTAI4GBJrauaGBRqNcjbUtbJ";
    private static String Secret = "rFhMiXCXMpUOaNBmGnSwrvPhTLKo8Y";

    public static void sendSms(String phone) {
        DefaultProfile profile = DefaultProfile.getProfile(REGIONID, AccessKeyId, Secret);
        IAcsClient client = new DefaultAcsClient(profile);
        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", REGIONID);
        request.putQueryParameter("PhoneNumbers", phone);
        request.putQueryParameter("SignName", SignName);
        request.putQueryParameter("TemplateCode", TemplateCode);

        Map<String, Object> map = new HashMap<String, Object>(16);
        String code = UUID.randomUUID().toString().substring(0, 4);
        map.put("code",code);
        try {
            request.putQueryParameter("TemplateParam", JSONObject.toJSONString(map));
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }

}
