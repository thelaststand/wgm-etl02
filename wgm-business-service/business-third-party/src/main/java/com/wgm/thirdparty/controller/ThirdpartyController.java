package com.wgm.thirdparty.controller;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.utils.StringUtils;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.exception.AdException;
import com.wgm.common.exception.BaseException;
import com.wgm.common.utils.PhoneUtils;
import com.wgm.thirdparty.service.SendSmsService;
import com.wgm.thirdparty.utils.OssUtil;
import com.wgm.thirdparty.utils.SendSmsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author songhuan
 * @email 3325074245@qq.com
 * @date 2021/01/20
 */
@RestController
@RequestMapping("/thirdparty")
public class ThirdpartyController {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private SendSmsService sendSmsService;
    /**
     *  获取验证码
     * @param phone 手机号
     * @return
     */
    @GetMapping("/send/{phone}")
    public CommonResponse send(@PathVariable("phone") String phone){

        //1、验证手机号是否正确
        if (!PhoneUtils.isPhoneLegal(phone)){
            throw new AdException(CommonConstants.RegisteredAndLoginError.PHONE_NUMBER_ERROR);
        }

        //2、调用方法发送
        String code = (String) redisTemplate.boundValueOps(phone).get();
        System.out.println(code);
        if(!StringUtils.isEmpty(code)){
            return CommonResponse.error("已存在还没过期");
        }

        //3、生成验证码并存储到Redis中
        //UUID
        code = UUID.randomUUID().toString().substring(0, 6);

        HashMap<String, Object> param = new HashMap<>(16);
        param.put("code",code);
        redisTemplate.boundValueOps(phone).set(code);

        //发送
        Boolean isSend = sendSmsService.send(phone, param);
        if(isSend){
            redisTemplate.boundHashOps(phone).expire(100, TimeUnit.MINUTES);
            return CommonResponse.ok(phone+":"+code+"发送成功");
        }else{
            return CommonResponse.error("发送失败");
        }
    }






}
