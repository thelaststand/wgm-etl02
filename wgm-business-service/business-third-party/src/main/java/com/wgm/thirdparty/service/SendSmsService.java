package com.wgm.thirdparty.service;

import java.util.HashMap;
/**
 * @author songhuan
 * @email 3325074245@qq.com
 * @date 2021/01/20
 */
public interface SendSmsService {
    /**
     * 发送
     * @param phone
     * @param code
     * @return
     */
    Boolean send(String phone, HashMap<String, Object> code);
}
