package com.wgm.center.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 接口类型
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
@Data
@TableName("interface_type")
public class InterfaceTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String id;
	/**
	 * 类型名称
	 */
	private String typeName;
	/**
	 * 类型描述
	 */
	private String typeDesc;
	/**
	 * 创建人
	 */
	private String createUser;
	/**
	 * 创建时间
	 */
	private Date createDate;
	/**
	 * 接口数量
	 */
	private Integer interfaceNum;
	/**
	 * 调用总次数
	 */
	private Integer callCount;
	/**
	 * 0展示 1不展示 (逻辑删除)
	 */
	private Integer showStatus;

}
