package com.wgm.center.dao;

import com.wgm.center.entity.ErrorCodeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 10:51:58
 */
@Mapper
public interface ErrorCodeDao extends BaseMapper<ErrorCodeEntity> {
	
}
