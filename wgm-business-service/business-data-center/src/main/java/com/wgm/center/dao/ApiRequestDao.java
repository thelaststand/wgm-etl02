package com.wgm.center.dao;

import com.wgm.center.entity.ApiRequestEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据块申请
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 18:52:59
 */
@Mapper
public interface ApiRequestDao extends BaseMapper<ApiRequestEntity> {
	
}
