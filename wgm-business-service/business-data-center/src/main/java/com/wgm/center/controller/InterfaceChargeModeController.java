package com.wgm.center.controller;


import com.wgm.common.constant.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.wgm.center.entity.InterfaceChargeModeEntity;
import com.wgm.center.service.InterfaceChargeModeService;


/**
 * 接口计费方式设置
 *
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
@RestController
@RequestMapping("center/interfacechargemode")
public class InterfaceChargeModeController {

    @Autowired
    private InterfaceChargeModeService interfaceChargeModeService;
    /**
     * 接口计费方式设置列表
     * @param page 当前页
     * @param size 每页条数
     * @return/
     */
    @GetMapping("list")
    public CommonResponse getList(@RequestParam(value = "page",defaultValue = "1") Integer page,
                                  @RequestParam(value = "size",defaultValue = "10") Integer size){
        return CommonResponse.ok(interfaceChargeModeService.getList(page,size));
    }
    /**
     * 根据ID查询
     * @param id
     * @return
     */
    @RequestMapping("/info/{id}")
    public CommonResponse info(@PathVariable("id") String id){
        return CommonResponse.ok(interfaceChargeModeService.getById(id));
    }
    /**
     * 保存
     * @param interfaceChargeMode
     * @return
     */
    @RequestMapping("/save")
    public CommonResponse save(@RequestBody InterfaceChargeModeEntity interfaceChargeMode){

		interfaceChargeModeService.add(interfaceChargeMode);
        return CommonResponse.ok();
    }

    /**
     * 修改
     * @param interfaceChargeMode
     * @return
     */
    @RequestMapping("/update")
    public CommonResponse update(@RequestBody InterfaceChargeModeEntity interfaceChargeMode){
		interfaceChargeModeService.updateById(interfaceChargeMode);
        return CommonResponse.ok();
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    public CommonResponse delete(@RequestParam("id") String id){
		interfaceChargeModeService.dele(id);
        return CommonResponse.ok();
    }

}
