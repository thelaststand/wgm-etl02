package com.wgm.center.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 接口计费方式设置
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
@Data
@TableName("interface_charge_mode")
public class InterfaceChargeModeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String id;
	/**
	 * 接口名称
	 */
	private String name;
	/**
	 * 计费类型
	 */
	private String charheType;
	/**
	 * 次数
	 */
	private Integer number;
	/**
	 * 天数
	 */
	private Integer dayNum;
	/**
	 * 价格
	 */
	private Double price;
	/**
	 * 创建人
	 */
	private String createUser;
	/**
	 * 创建时间
	 */
	private Date createDate;
	/**
	 * 0展示 1不展示 (逻辑删除)
	 */
	private Integer showStatus;

}
