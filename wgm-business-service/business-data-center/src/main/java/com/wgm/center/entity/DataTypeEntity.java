package com.wgm.center.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 类型
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
@Data
@TableName("data_type")
public class DataTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String id;
	/**
	 * 类型名称
	 */
	private String type;

}
