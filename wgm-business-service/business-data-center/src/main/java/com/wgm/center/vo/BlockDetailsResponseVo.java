package com.wgm.center.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author pangpang
 * @email 1963148908@qq.com
 * @date 2021/1/20 15:23
 */
@Data
public class BlockDetailsResponseVo {

    private String id;
    /**
     * 数据块id
     */
    private String dataId;
    /**
     * 接口地址
     */
    private String interfaceAddress;
    /**
     * 返回格式
     */
    private String returnFormat;
    /**
     * 请求方式
     */
    private String requestMode;
    /**
     * 请求示例
     */
    private String requestExample;
    /**
     * 简介
     */
    private String intro;
    /**
     * 请求参数说明
     */
    private String requestParam;
    /**
     * 返回参数说明
     */
    private String returnParam;
    /**
     * JSON返回示例
     */
    private String jsonReturn;
    /**
     * XML返回示例
     */
    private String xmlReturn;
    /**
     * 错误码id
     */
    private String  errorCodeId;

    /**
     * 服务级错误码参照
     */
    private String serverErrorCode;
    /**
     * 系统级错误码参照
     */
    private String sysErrorCode;
    /**
     * 错误格式说明
     */
    private String errorFormat;

    /**
     * 接口名称
     */
    private String name;
    /**
     * 接口类型id
     */
    private Integer typeId;
    /**
     * 费用（按次计费）
     */
    private Double money;
    /**
     * 描述
     */
    private String describe;
    /**
     * 接口图片
     */
    private String pic;
    /**
     * 接口来源
     */
    private String source;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新周期(单位：天)
     */
    private Integer updatePeriod;
    /**
     * 此接口使用的次数
     */
    private Integer useCount;

}
