package com.wgm.center.controller;

import java.util.Arrays;
import java.util.Date;

import com.alibaba.nacos.common.util.UuidUtils;
import com.wgm.common.constant.CommonResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wgm.center.entity.InterfaceRuleEntity;
import com.wgm.center.service.InterfaceRuleService;
/**
 * 接口规则
 *
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
@RestController
@RequestMapping("center/interfacerule")
public class InterfaceRuleController {
    @Autowired
    private InterfaceRuleService interfaceRuleService;

    /**
     * 接口规则列表
     * @param page 当前页
     * @param size 每页条数
     * @return
     */
    @GetMapping("list")
    public CommonResponse getList(@RequestParam(value = "page",defaultValue = "1") Integer page,
                                  @RequestParam(value = "size",defaultValue = "10") Integer size){
        return CommonResponse.ok(interfaceRuleService.getList(page,size));
    }

    /**
     * 根据ID查询
     * @param id
     * @return
     */
    @RequestMapping("/info/{id}")
    public CommonResponse info(@PathVariable("id") String id){
        return CommonResponse.ok(interfaceRuleService.getById(id));
    }

    /**
     * 保存
     * @param interfaceRule
     * @return
     */
    @RequestMapping("/save")
    public CommonResponse save(@RequestBody InterfaceRuleEntity interfaceRule){
        interfaceRule.setId(UuidUtils.generateUuid());
        interfaceRule.setCreateDate(new Date());
		interfaceRuleService.save(interfaceRule);

        return CommonResponse.ok();
    }

    /**
     * 修改
     * @param interfaceRule
     * @return
     */
    @RequestMapping("/update")
    public CommonResponse update(@RequestBody InterfaceRuleEntity interfaceRule){
		interfaceRuleService.updateById(interfaceRule);

        return CommonResponse.ok();
    }

    /**
     * 修改状态
     * @param id
     * @param status 状态（1启用2禁用）
     * @return
     */
    @GetMapping("/updateStatus")
    public CommonResponse updateStatus(@RequestParam("id") String id,
                                       @RequestParam("status") String status){
        interfaceRuleService.updateStatus(id,status);
        return CommonResponse.ok();
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/delete/{id}")
    public CommonResponse delete(@PathVariable("id") String id){
		interfaceRuleService.dele(id);
        return CommonResponse.ok();
    }

}
