package com.wgm.center;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
*   class
*   @Author 小脸蛋
*   @CreateDate 2021/1/19
*/
@SpringBootApplication
@EnableTransactionManagement
@EnableDiscoveryClient
@EnableFeignClients
@MapperScan("com.wgm.center.dao")
public class WgmDataCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(WgmDataCenterApplication.class, args);
    }

}
