package com.wgm.center.vo;

import lombok.Data;

/**
*   class
*   @Author 小脸蛋
*   @Date 2021/1/20 20:25
*/
@Data
public class InterfaceRequestVo {
    /**
     * 姓名
     */
    private String name;
    /**
     * 当前页
     */
    private Integer page = 1;
    /**
     * 每页数据条数
     */
    private Integer size = 10;
}
