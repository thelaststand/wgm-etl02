package com.wgm.center.dao;

import com.wgm.center.entity.InterfaceTypeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 接口类型
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
@Mapper
public interface InterfaceTypeDao extends BaseMapper<InterfaceTypeEntity> {

    /**
     * 接口类型列表
     * @param page 当前页
     * @param size 每页条数
     * @return
     */
    List<InterfaceTypeEntity> getList(@Param("page") Integer page,
                                      @Param("size") Integer size);

    /**
     * 查询总条数
     * @return
     */
    Integer getCount();

    /**
     * 删除
     * @param id
     */
    void dele(String id);
}
