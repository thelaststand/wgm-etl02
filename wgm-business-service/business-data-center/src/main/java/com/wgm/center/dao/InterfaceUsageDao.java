package com.wgm.center.dao;

import com.wgm.center.entity.InterfaceUsageEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.center.vo.InterfaceUsageRequestVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 接口使用量
 * 
 * @author Ð¡Á³µ°
 * @email 2859058385@qq.com
 * @date 2021-01-21 10:59:01
 */
@Mapper
public interface InterfaceUsageDao extends BaseMapper<InterfaceUsageEntity> {

    /**
     * 查看某接口的使用量详细情况
     * @param usageRequestVo
     * @return
     */
    List<InterfaceUsageEntity> interfaceUsage(InterfaceUsageRequestVo usageRequestVo);
}
