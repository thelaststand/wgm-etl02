package com.wgm.center.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 接口使用量
 * 
 * @author Ð¡Á³µ°
 * @email 2859058385@qq.com
 * @date 2021-01-21 10:59:01
 */
@Data
@TableName("interface_usage")
public class InterfaceUsageEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 公司名称
	 */
	private String name;
	/**
	 * 接口列表id
	 */
	private String interfaceId;
	/**
	 * 使用时间
	 */
	private Date useTime;
	/**
	 * 响应时间(毫秒)
	 */
	private Integer responseTime;
	/**
	 * 返回状态（0.失败1.成功）
	 */
	private String returnStatus;
	/**
	 * 失败原因
	 */
	private String causeFailure;

}
