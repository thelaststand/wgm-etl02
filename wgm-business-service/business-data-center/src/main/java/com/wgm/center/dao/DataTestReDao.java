package com.wgm.center.dao;

import com.wgm.center.entity.DataTestReEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据测试 请求 以及 返回信息
 * 
 * @author kangbaby
 * @email 2859058385@qq.com
 * @date 2021-01-22 11:35:24
 */
@Mapper
public interface DataTestReDao extends BaseMapper<DataTestReEntity> {
	
}
