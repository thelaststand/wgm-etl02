package com.wgm.center.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.center.vo.BlockDetailsResponseVo;
import com.wgm.center.entity.BlockDetailsEntity;

import java.util.Map;

/**
 * 
 *
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 10:51:57
 */
public interface BlockDetailsService extends IService<BlockDetailsEntity> {

    /**
     * 对应详情
     * @param dataId
     * @return
     */
    Map getDetails(String dataId);
}

