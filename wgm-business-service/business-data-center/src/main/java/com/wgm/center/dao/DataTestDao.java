package com.wgm.center.dao;

import com.wgm.center.entity.DataTestEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据api测试需要的信息
 * 
 * @author kangbaby
 * @email 2859058385@qq.com
 * @date 2021-01-22 11:35:24
 */
@Mapper
public interface DataTestDao extends BaseMapper<DataTestEntity> {
    /**
     * 测试需要的信息
     * @param blockId
     * @return
     */
    DataTestEntity testShow(String blockId);
}
