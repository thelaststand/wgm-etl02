package com.wgm.center.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

import javax.validation.constraints.Null;

/**
 * 数据块申请
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 18:52:59
 */
@Data
@TableName("api_request")
public class ApiRequestEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 申请数据块id
	 */
	private String dataId;
	/**
	 * 用户（申请人）
	 */
	private String userId;
	/**
	 * api预警方式( 1.预警次数 2. 预警天数)
	 */
	@Null
	private String apiWay;
	/**
	 * 预警阀值（按次数）
	 */
	private Integer apiValueNum;
	/**
	 * 预警阀值（按时间）
	 */
	private Integer apiValueTime;
	/**
	 * api预警号码选择
	 */
	private String phoneId;
	/**
	 * 申请时间
	 */
	private Date createTime;
	/**
	 * 申请状态
	 */
	private String status;
}
