package com.wgm.center.service.impl;

import com.wgm.center.entity.InterfaceTypeEntity;
import com.wgm.common.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


import com.wgm.center.dao.InterfaceConfigDao;
import com.wgm.center.entity.InterfaceConfigEntity;
import com.wgm.center.service.InterfaceConfigService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 小脸蛋
 */
@Service("interfaceConfigService")
@Transactional(rollbackFor = Exception.class)
public class InterfaceConfigServiceImpl extends ServiceImpl<InterfaceConfigDao, InterfaceConfigEntity> implements InterfaceConfigService {

    @Autowired
    InterfaceConfigDao interfaceConfigDao;

    /**
     * 接口配置列表
     * @param page 当前页
     * @param size 每页条数
     * @return
     */
    @Override
    public PageUtils getList(Integer page, Integer size) {
        List<InterfaceConfigEntity> list = interfaceConfigDao.getList((page - 1) * size, size);
        Integer count = interfaceConfigDao.getCount();

        return new PageUtils(list,count,size,page);
    }
    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public void dele(String id) {
        interfaceConfigDao.dele(id);
    }
}