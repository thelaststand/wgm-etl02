package com.wgm.center.web;

import com.github.pagehelper.PageInfo;
import com.wgm.center.vo.PageRequestParamVo;
import com.wgm.common.constant.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import com.wgm.center.service.MyApiService;

import java.util.Map;
import java.util.concurrent.ConcurrentMap;


/**
 * 我的api
 *
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 14:05:30
 */
@RestController
@RequestMapping("web/myapi")
public class MyApiController {
    @Autowired
    private MyApiService myApiService;

    /**
     * 我的 api 列表
     * @return CommonResponse
     */
    @GetMapping("/search")
    public CommonResponse search(PageRequestParamVo page){
        PageInfo<ConcurrentMap<String, Object>> search = myApiService.search(page);
        return CommonResponse.ok(search);
    }
    /**
     * 我的 api 接口 详情
     * @return CommonResponse
     */
    @GetMapping("/search/detail/{id}")
    public CommonResponse searchDetail(@PathVariable("id")String id){
       Map<String, Object> map = myApiService.searchDetail(id);
       return CommonResponse.ok(map);
    }

}
