package com.wgm.center.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.Query;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.common.utils.PageUtils;


import com.wgm.center.dao.ErrorCodeDao;
import com.wgm.center.entity.ErrorCodeEntity;
import com.wgm.center.service.ErrorCodeService;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 小脸蛋
 */
@Service("errorCodeService")
@Transactional(rollbackFor = Exception.class)
public class ErrorCodeServiceImpl extends ServiceImpl<ErrorCodeDao, ErrorCodeEntity> implements ErrorCodeService {


}