package com.wgm.center.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.common.utils.PageUtils;
import com.wgm.center.entity.ErrorCodeEntity;

import java.util.Map;

/**
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 10:51:58
 */
public interface ErrorCodeService extends IService<ErrorCodeEntity> {

}

