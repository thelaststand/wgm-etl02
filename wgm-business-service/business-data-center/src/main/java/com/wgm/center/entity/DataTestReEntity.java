package com.wgm.center.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 数据测试 请求 以及 返回信息
 * 
 * @author kangbaby
 * @email 2859058385@qq.com
 * @date 2021-01-22 11:35:24
 */
@Data
@TableName("data_test_re")
public class DataTestReEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 测试id
	 */
	private String testId;
	/**
	 * 测试该接口的用户
	 */
	private String userId;
	/**
	 * 请求地址
	 */
	private String requestAddress;
	/**
	 * 请求方式
	 */
	private String requestMode;
	/**
	 * 请求参数
	 */
	private String requestParam;
	/**
	 * 返回内容
	 */
	private String returnContent;
	/**
	 * 测试状态
	 */
	private String status;
	/**
	 * 测试时间
	 */
	private Date  createTime;

}
