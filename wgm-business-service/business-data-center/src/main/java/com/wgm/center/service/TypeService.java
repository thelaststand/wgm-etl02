package com.wgm.center.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.center.entity.DataTypeEntity;

/**
 * 类型
 *
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
public interface TypeService extends IService<DataTypeEntity> {

}

