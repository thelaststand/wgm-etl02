package com.wgm.center.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import com.wgm.center.dao.TypeDao;
import com.wgm.center.entity.DataTypeEntity;
import com.wgm.center.service.TypeService;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 小脸蛋
 */
@Service("typeService")
@Transactional(rollbackFor = Exception.class)
public class TypeServiceImpl extends ServiceImpl<TypeDao, DataTypeEntity> implements TypeService {


}