package com.wgm.center.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.center.entity.ApiRequestEntity;
import com.wgm.center.entity.DataTestEntity;
import com.wgm.center.entity.DataTestReEntity;
import com.wgm.center.vo.BlockRequestVo;
import com.wgm.center.vo.InterfaceRequestVo;
import com.wgm.center.vo.InterfaceUsageRequestVo;
import com.wgm.common.utils.PageUtils;
import com.wgm.center.entity.BlockEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.concurrent.ConcurrentMap;

/**
 * 
 *
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 10:51:58
 */
public interface BlockService extends IService<BlockEntity> {

    /**
     * 数据块列表
     * @param blockRequestVo
     * @param pageSize
     * @param currPage
     * @return
     */
    PageUtils getBlockList(BlockRequestVo blockRequestVo, Integer pageSize, Integer currPage);

    /**
     * 数据块 申请
     * @param apiRequestEntity
     */
    void datareQuest(ApiRequestEntity apiRequestEntity);

    /**
     * 接口列表
     * @param interfaceRequestVo
     * @return
     */
    PageUtils interfaceList(InterfaceRequestVo interfaceRequestVo);

    /**
     * 删除
     * @param id
     */
    void del(String id);

    /**
     * 接口修改
     * @param blockEntity
     */
    void updateInterface(BlockEntity blockEntity);

    /**
     * 查看某接口的使用量详细情况
     * @param usageRequestVo
     * @return
     */
    PageUtils interfaceUsage(InterfaceUsageRequestVo usageRequestVo);

    /**
     * 接口详情
     * @param id
     * @return
     */
    ConcurrentMap<String, Object> details(String id);

    /**
     * 接口添加
     * @param blockEntity
     */
    void add(BlockEntity blockEntity);

    /**
     * 查找信息
     * @param id
     * @return
     */
    BlockEntity findById(String id);

    /**
     * 上传文件
     * @param file
     * @return
     */
    String loadPic(MultipartFile file);

    /**
     * 展示测试需要的信息
     * @param blockId
     * @return
     */
    DataTestEntity testShow(String blockId);

    /**
     * 测试
     * @param dataTestReEntity
     * @return
     */
    DataTestReEntity test(DataTestReEntity dataTestReEntity);
}

