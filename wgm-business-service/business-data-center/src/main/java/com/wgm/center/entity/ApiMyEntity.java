package com.wgm.center.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

import javax.validation.constraints.Null;

/**
 * 我的api
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 14:05:30
 */
@Data
@TableName("api_my")
public class ApiMyEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * api名称
	 */
	private String name;
	/**
	 * 用户id
	 */
	private String userId;
	/**
	 * 数据块详情id
	 */
	private String blockId;
	/**
	 * 总天数
	 */
	private Integer days;
	/**
	 * 剩余次数
	 */
	private Integer residueNumber;
	/**
	 * 计费方式（1 按次计费，2.按时间计费）
	 */
	@Null
	private String chargeMode;
	/**
	 * 状态（0审核失败，1审核成功，2审核中）
	 */
	private String status;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;

}
