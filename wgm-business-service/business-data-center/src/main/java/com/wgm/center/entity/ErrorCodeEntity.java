package com.wgm.center.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 10:51:58
 */
@Data
@TableName("error_code")
public class ErrorCodeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 服务级错误码参照
	 */
	private String serverErrorCode;
	/**
	 * 系统级错误码参照
	 */
	private String sysErrorCode;
	/**
	 * 错误格式说明 
	 */
	private String errorFormat;

}
