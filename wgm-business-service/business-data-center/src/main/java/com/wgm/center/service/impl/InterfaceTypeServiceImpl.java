package com.wgm.center.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.common.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.wgm.center.dao.InterfaceTypeDao;
import com.wgm.center.entity.InterfaceTypeEntity;
import com.wgm.center.service.InterfaceTypeService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 小脸蛋
 */
@Service("interfaceTypeService")
@Transactional(rollbackFor = Exception.class)
public class InterfaceTypeServiceImpl extends ServiceImpl<InterfaceTypeDao, InterfaceTypeEntity> implements InterfaceTypeService {

    @Autowired
    InterfaceTypeDao interfaceTypeDao;

    /**
     * 接口类型列表
     * @param page 当前页
     * @param size 每页条数
     * @return
     */
    @Override
    public PageUtils getList(Integer page, Integer size) {
        List<InterfaceTypeEntity> list = interfaceTypeDao.getList((page - 1) * size, size);
        Integer count = interfaceTypeDao.getCount();

        return new PageUtils(list,count,size,page);
    }
    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public void dele(String id) {
        interfaceTypeDao.dele(id);

    }
}