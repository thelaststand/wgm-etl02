package com.wgm.center.dao;

import com.wgm.center.entity.InterfaceDetailsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 接口详情
 * 
 * @author Ð¡Á³µ°
 * @email 2859058385@qq.com
 * @date 2021-01-21 15:02:18
 */
@Mapper
public interface InterfaceDetailsDao extends BaseMapper<InterfaceDetailsEntity> {

    /**
     * 接口详情
     * @param id
     * @return
     */
    InterfaceDetailsEntity details(String id);
}
