package com.wgm.center.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.common.utils.PageUtils;
import com.wgm.center.entity.InterfaceRuleEntity;

import java.util.Map;

/**
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
public interface InterfaceRuleService extends IService<InterfaceRuleEntity> {

    /**
     * 删除
     * @param id
     */
    void dele(String id);

    /**
     * 接口规则列表
     * @param page
     * @param size
     * @return
     */
    PageUtils getList(Integer page, Integer size);
    /**
     * 修改状态
     * @param id
     * @param status
     */
    void updateStatus(String id,String status);
}

