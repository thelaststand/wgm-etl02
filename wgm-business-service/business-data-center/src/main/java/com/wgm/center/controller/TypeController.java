package com.wgm.center.controller;

import java.util.Arrays;
import java.util.Map;

import com.baomidou.mybatisplus.extension.api.R;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wgm.center.entity.DataTypeEntity;
import com.wgm.center.service.TypeService;




/**
 * 类型
 *
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
@RestController
@RequestMapping("center/type")
public class TypeController {
    @Autowired
    private TypeService typeService;


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public CommonResponse info(@PathVariable("id") String id){
		DataTypeEntity type = typeService.getById(id);

        return CommonResponse.ok();
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public CommonResponse save(@RequestBody DataTypeEntity type){
		typeService.save(type);

        return CommonResponse.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public CommonResponse update(@RequestBody DataTypeEntity type){
		typeService.updateById(type);

        return CommonResponse.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public CommonResponse delete(@RequestBody String[] ids){
		typeService.removeByIds(Arrays.asList(ids));

        return CommonResponse.ok();
    }

}
