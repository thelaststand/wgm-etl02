package com.wgm.center.service.impl;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wgm.center.dao.*;
import com.wgm.center.entity.*;
import com.wgm.center.vo.BlockRequestVo;
import com.wgm.center.vo.BlockResponseVo;
import com.wgm.center.vo.InterfaceRequestVo;
import com.wgm.center.vo.InterfaceUsageRequestVo;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.exception.ExceptionCast;
import com.wgm.common.utils.HttpRequestUtil;
import com.wgm.common.utils.PageUtils;
import com.wgm.common.utils.UuidUtils;
import com.wgm.thirdparty.utils.OssUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.center.service.BlockService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;


/**
 * @author kangbaby
 */
@Service("blockService")
@Transactional(rollbackFor = Exception.class)
public class BlockServiceImpl extends ServiceImpl<BlockDao, BlockEntity> implements BlockService {

    @Autowired
    BlockDao blockDao;
    @Autowired
    ApiRequestDao apiRequestDao;
    @Autowired
    InterfaceUsageDao interfaceUsageDao;
    @Autowired
    InterfaceDetailsDao interfaceDetailsDao;
    @Autowired
    DataTestDao testDao;
    @Autowired
    DataTestReDao testReDao;
    /**
     * 数据块列表
     * @param blockRequestVo
     * @param pageSize
     * @param currPage
     * @return
     */
    @Override
    public PageUtils getBlockList(BlockRequestVo blockRequestVo, Integer pageSize, Integer currPage) {
        //查询列表
        List<BlockResponseVo> blockList = blockDao.getBlockList(blockRequestVo.getName(),
                                                                blockRequestVo.getType(),
                                                                (currPage - 1) * pageSize,
                                                                pageSize);

        //总记录数
        int totalCount = blockDao.getCount();
        return new PageUtils(blockList,totalCount,pageSize,currPage);
    }

    /**
     * 数据块 申请
     * @return CommonResponse
     */
    @Override
    public void datareQuest(ApiRequestEntity apiRequestEntity) {
        apiRequestEntity.setId(UuidUtils.generateUuid());
        apiRequestEntity.setStatus("2");
        apiRequestEntity.setCreateTime(new Date());
        apiRequestDao.insert(apiRequestEntity);
    }

    /**
     * 接口列表
     * @param interfaceRequestVo
     * @return
     */
    @Override
    public PageUtils interfaceList(InterfaceRequestVo interfaceRequestVo) {
        PageHelper.startPage(interfaceRequestVo.getPage(), interfaceRequestVo.getSize());

        List<BlockEntity> blockEntities = blockDao.interfaceList(interfaceRequestVo.getName());
        PageInfo<BlockEntity> page = PageInfo.of(blockEntities);

        return new PageUtils(page.getList(),(int)page.getTotal(),page.getPages(),page.getPageSize());
    }

    /**
     * 添加列表
     * @param blockEntity
     */
    @Override
    public void add(BlockEntity blockEntity) {
        blockEntity.setId(UuidUtils.generateUuid());
        blockEntity.setCreateTime(new Date());
        blockEntity.setShowStatus(0);
        blockDao.insert(blockEntity);
    }

    /**
     * 获取实体
     * @param id
     * @return
     */
    @Override
    public BlockEntity findById(String id) {
        if (StringUtils.isEmpty(id)){
            ExceptionCast.interfaceException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        BlockEntity blockEntity = blockDao.selectById(id);
        return blockEntity;
    }

    /**
     * 接口删除
     * @param id
     */
    @Override
    public void del(String id) {
        if (StringUtils.isEmpty(id)){
            ExceptionCast.interfaceException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        BlockEntity blockEntity = blockDao.selectById(id);
        if (blockEntity==null){
            ExceptionCast.interfaceException(CommonConstants.ErrorMsg.DOESNO_TEXIST);
        }
        blockEntity.setShowStatus(1);
        blockDao.updateById(blockEntity);
    }

    /**
     * 接口修改
     * @param blockEntity
     */
    @Override
    public void updateInterface(BlockEntity blockEntity) {
        if (blockEntity!=null && blockEntity.getId()!=null){
            ExceptionCast.interfaceException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        blockEntity.setUpdateTime(new Date());
        blockDao.updateById(blockEntity);
    }
    /**
     * 查看某接口的使用量详细情况
     * @param usageRequestVo
     */
    @Override
    public PageUtils interfaceUsage(InterfaceUsageRequestVo usageRequestVo) {
        PageHelper.startPage(usageRequestVo.getPage(), usageRequestVo.getSize());
        List<InterfaceUsageEntity> usageEntities =interfaceUsageDao.interfaceUsage(usageRequestVo);
        PageInfo<InterfaceUsageEntity> page = PageInfo.of(usageEntities);
        return new PageUtils(page.getList(),(int)page.getTotal(),page.getPages(),page.getPageSize());
    }

    /**
     * 接口详情
     * @param id
     */
    @Override
    public ConcurrentMap<String, Object> details(String id) {
        /**
         * 查询接口使用量 情况
         */
        InterfaceUsageRequestVo usageRequestVo = new InterfaceUsageRequestVo();
        usageRequestVo.setId(id);
        PageHelper.startPage(usageRequestVo.getPage(), usageRequestVo.getSize());
        List<InterfaceUsageEntity> usageEntities =interfaceUsageDao.interfaceUsage(usageRequestVo);
        PageInfo<InterfaceUsageEntity> page = PageInfo.of(usageEntities);
        /**
         * 查询详情
         */
        InterfaceDetailsEntity detailsEntity = interfaceDetailsDao.details(id);
        //创建 map容器 将这连个结果放进去 并返回
        ConcurrentMap<String, Object> concurrentMap = new ConcurrentHashMap<String, Object>(16);
        concurrentMap.put(CommonConstants.Data.INTERFACEUSAGE,page.getList());
        concurrentMap.put(CommonConstants.Data.INTERFACEDETAILS, detailsEntity);
        return concurrentMap;
    }

    /**
     * 上传文件
     * @param file
     * @return
     */
    @Override
    public String loadPic(MultipartFile file) {
        if (file==null){
            ExceptionCast.interfaceException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        String pic = OssUtil.uploadFileInputSteam(file.getOriginalFilename(), file);
        return pic;
    }

    /**
     * 展示测试需要的信息
     * @param blockId
     * @return
     */
    @Override
    public DataTestEntity testShow(String blockId) {
        if (StringUtils.isEmpty(blockId)){
            ExceptionCast.interfaceException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        DataTestEntity testEntity = testDao.testShow(blockId);
        return testEntity;
    }
    /**
     * 测试
     * @param dataTestReEntity
     * @return
     */
    @Override
    public DataTestReEntity test(DataTestReEntity dataTestReEntity) {
        if (dataTestReEntity==null){
            ExceptionCast.interfaceException(CommonConstants.ErrorMsg.REQUEST_PARAM_ERROR);
        }
        dataTestReEntity.setCreateTime(new Date());
        if (CommonConstants.Data.GET.equalsIgnoreCase(dataTestReEntity.getRequestMode())){
            dataTestReEntity.setStatus("0");
            StringBuilder builder = new StringBuilder();
            builder.append(dataTestReEntity.getRequestAddress());
            if (dataTestReEntity.getRequestParam()!=null){
                builder.append("?"+dataTestReEntity.getRequestParam());
            }
            //路径和 参数进行 拼接
            String requestGet = HttpRequestUtil.requestGet(builder.toString());
            dataTestReEntity.setId(UuidUtils.generateUuid());
            dataTestReEntity.setReturnContent(requestGet);
            testReDao.insert(dataTestReEntity);
            return dataTestReEntity;
        }else if (CommonConstants.Data.POST.equalsIgnoreCase(dataTestReEntity.getRequestMode())){
            dataTestReEntity.setStatus("0");
            String requestPost = HttpRequestUtil.requestPost(dataTestReEntity.getRequestAddress(), dataTestReEntity.getRequestParam());
            dataTestReEntity.setReturnContent(requestPost);
            testReDao.insert(dataTestReEntity);
            return dataTestReEntity;
        }
        if (dataTestReEntity.getStatus()==null){
            dataTestReEntity.setStatus("1");
            testReDao.insert(dataTestReEntity);
            return null;
        }
        return dataTestReEntity;
    }
}