package com.wgm.center.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import com.baomidou.mybatisplus.extension.api.R;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.utils.UuidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wgm.center.entity.InterfaceConfigEntity;
import com.wgm.center.service.InterfaceConfigService;
import com.wgm.common.utils.PageUtils;




/**
 * 接口配置
 *
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
@RestController
@RequestMapping("center/interfaceconfig")
public class InterfaceConfigController {
    @Autowired
    private InterfaceConfigService interfaceConfigService;

    /**
     * 接口配置列表
     * @param page 当前页
     * @param size 每页条数
     * @return
     */
    @GetMapping("list")
    public CommonResponse getList(@RequestParam(value = "page",defaultValue = "1") Integer page,
                                  @RequestParam(value = "size",defaultValue = "10") Integer size){
        return CommonResponse.ok(interfaceConfigService.getList(page,size));
    }

    /**
     * 根据ID查询
     * @param id
     * @return
     */
    @GetMapping("/info/{id}")
    public CommonResponse info(@PathVariable("id") String id){
        return CommonResponse.ok(interfaceConfigService.getById(id));
    }

    /**
     * 新增
     * @param interfaceConfig
     * @return
     */
    @PostMapping("/save")
    public CommonResponse save(@RequestBody InterfaceConfigEntity interfaceConfig){
        interfaceConfig.setId(UuidUtils.generateUuid());
        interfaceConfig.setCreateDate(new Date());
		interfaceConfigService.save(interfaceConfig);

        return CommonResponse.ok();
    }

    /**
     * 修改
     * @param interfaceConfig
     * @return
     */
    @PostMapping("/update")
    public CommonResponse update(@RequestBody InterfaceConfigEntity interfaceConfig){
		interfaceConfigService.updateById(interfaceConfig);

        return CommonResponse.ok();
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @GetMapping("/delete")
    public CommonResponse delete(@RequestParam String id){
		interfaceConfigService.dele(id);

        return CommonResponse.ok();
    }

}
