package com.wgm.center.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.wgm.center.vo.PageRequestParamVo;
import com.wgm.center.entity.ApiMyEntity;

import java.util.Map;
import java.util.concurrent.ConcurrentMap;

/**
 * 我的api
 *
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 14:05:30
 */
public interface MyApiService extends IService<ApiMyEntity> {
    /**
     * 查询 my API
     * @param page
     * @return
     */
    PageInfo<ConcurrentMap<String, Object>> search(PageRequestParamVo page);

    /**
     * 查询 my API 详情
     * @param id
     * @return
     */
    Map<String, Object> searchDetail(String id);
}

