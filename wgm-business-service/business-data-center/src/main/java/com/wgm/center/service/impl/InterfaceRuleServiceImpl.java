package com.wgm.center.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wgm.center.entity.InterfaceTypeEntity;
import com.wgm.common.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.wgm.center.dao.InterfaceRuleDao;
import com.wgm.center.entity.InterfaceRuleEntity;
import com.wgm.center.service.InterfaceRuleService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 小脸蛋
 */
@Service("interfaceRuleService")
@Transactional(rollbackFor = Exception.class)
public class InterfaceRuleServiceImpl extends ServiceImpl<InterfaceRuleDao, InterfaceRuleEntity> implements InterfaceRuleService {

    @Autowired
    InterfaceRuleDao interfaceRuleDao;

    /**
     * 删除
     * @param id
     */
    @Override
    public void dele(String id) {
        interfaceRuleDao.delet(id);
    }

    /**
     * 接口规则列表
     * @param page
     * @param size
     * @return
     */
    @Override
    public PageUtils getList(Integer page, Integer size) {
        List<InterfaceRuleEntity> list = interfaceRuleDao.getList((page - 1) * size, size);
        Integer count = interfaceRuleDao.getCount();

        return new PageUtils(list,count,size,page);
    }
    /**
     * 修改状态
     * @param id
     * @param status
     */
    @Override
    public void updateStatus(String id,String status) {
        interfaceRuleDao.updateStatus(id,status);
    }
}