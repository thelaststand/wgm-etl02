package com.wgm.center.controller;

import com.wgm.center.entity.BlockEntity;
import com.wgm.center.entity.DataTestEntity;
import com.wgm.center.entity.DataTestReEntity;
import com.wgm.center.service.BlockService;
import com.wgm.center.vo.InterfaceRequestVo;
import com.wgm.center.vo.InterfaceUsageRequestVo;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.constant.CommonStatus;
import com.wgm.common.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.concurrent.ConcurrentMap;

/**
 * 接口管理
*   class
*   @Author 小脸蛋
*   @Date 2021/1/20 20:00
*/
@RestController
@RequestMapping("/interface")
public class InterfaceAdController {

    @Autowired
    private BlockService blockService;
    /**
     * 接口列表
     * @param interfaceRequestVo
     * @return
     */
    @GetMapping("/interfaceList")
    public CommonResponse interfaceList(InterfaceRequestVo interfaceRequestVo){
        PageUtils pageUtils = blockService.interfaceList(interfaceRequestVo);
        return CommonResponse.ok(pageUtils);
    }

    /**
     * 添加 接口列表
     * @param blockEntity
     * @return
     */
    @PostMapping("/add")
    public CommonResponse add(@RequestBody BlockEntity blockEntity){

        blockService.add(blockEntity);
        return CommonResponse.ok();
    }
    /**
     * 图片上传
     */
    @PostMapping("/loadPic")
    public CommonResponse loadPic(MultipartFile file){
        return CommonResponse.ok(blockService.loadPic(file));
    }
    /**
     * 删除
     */
    @PostMapping("/del/{id}")
    public CommonResponse del(@PathVariable("id") String id){
        blockService.del(id);
        return CommonResponse.ok();
    }
    /**
     * 获取信息
     */
    @GetMapping("/findById/{id}")
    public CommonResponse findById(@PathVariable("id")String id){
        return CommonResponse.ok(blockService.findById(id));
    }
    /**
     * 修改接口
     * @param blockEntity
     * @return
     */
    @PutMapping("/updateInterface")
    public CommonResponse updateInterface(@RequestBody BlockEntity blockEntity){
        blockService.updateInterface(blockEntity);
        return CommonResponse.ok();
    }

    /**
     *  查询 某接口的使用量详细情况
     */
    @GetMapping("/usage")
    public CommonResponse interfaceUsage(InterfaceUsageRequestVo usageRequestVo){
        PageUtils pageUtils = blockService.interfaceUsage(usageRequestVo);
        return CommonResponse.ok(pageUtils);
    }

    /**
     * 接口详情
     * @param id
     * @return
     */
    @GetMapping("/details/{id}")
    public CommonResponse details(@PathVariable("id") String id){
        ConcurrentMap<String, Object> details = blockService.details(id);
        return CommonResponse.ok(details);
    }

    /**
     * 测试需要的信息展示
     * @param blockId
     * @return
     */
    @GetMapping("/testShow/")
    public CommonResponse testShow(@RequestParam("blockId")String blockId){
        DataTestEntity testEntity = blockService.testShow(blockId);
        return CommonResponse.ok(testEntity);
    }

    /**
     * 测试
     * @param dataTestReEntity
     * @return
     */
    @PostMapping("/test")
    public CommonResponse test(@RequestBody DataTestReEntity dataTestReEntity){
        DataTestReEntity reEntity = blockService.test(dataTestReEntity);
        return CommonResponse.ok(reEntity==null? CommonStatus.INVALID:CommonStatus.VALID);
    }
}
