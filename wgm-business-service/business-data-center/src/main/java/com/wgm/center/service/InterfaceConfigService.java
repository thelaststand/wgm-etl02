package com.wgm.center.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.common.utils.PageUtils;
import com.wgm.center.entity.InterfaceConfigEntity;

import java.util.List;
import java.util.Map;

/**
 * 接口配置
 *
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
public interface InterfaceConfigService extends IService<InterfaceConfigEntity> {

    /**
     * 接口配置列表
     * @param page 当前页
     * @param size 每页条数
     * @return
     */
    PageUtils getList(Integer page, Integer size);
    /**
     * 删除
     * @param id
     * @return
     */
    void dele(String id);
}

