package com.wgm.center.dao;

import com.wgm.center.entity.InterfaceRuleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.center.entity.InterfaceTypeEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 接口规则
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
@Mapper
public interface InterfaceRuleDao extends BaseMapper<InterfaceRuleEntity> {

    /**
     * 删除
     * @param id
     */
    void delet(String id);
    /**
     * 接口规则列表
     * @param page
     * @param size
     * @return
     */
    List<InterfaceRuleEntity> getList(@Param("page") Integer page,
                                      @Param("size") Integer size);

    /**
     * 查询总条数
     * @return
     */
    Integer getCount();

    /**
     * 修改状态
     * @param id
     * @param status
     */
    void updateStatus(@Param("id") String id,
                      @Param("status")String status);
}
