package com.wgm.center.vo;

import lombok.Data;

/**
*   class
*   @Author 小脸蛋
*   @Date 2021/1/20 14:15
*/
@Data
public class PageRequestParamVo {
    /**
     * api名称
     */
    private String name;
    /**
     * 审核状态（0审核失败，1成功 2.审核中）
     */
    private String status;
    /**
     * 当前登录的用户
     */
    private String loggedUser;
    /**
     * 当前页
     */
    private Integer page = 1;
    /**
     * 每页条数
     */
    private Integer size = 8;
}
