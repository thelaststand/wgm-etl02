package com.wgm.center.web;

import com.wgm.center.entity.ApiRequestEntity;
import com.wgm.center.vo.BlockRequestVo;
import com.wgm.common.constant.CommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wgm.center.service.BlockService;

/**
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 10:51:58
 */
@RestController
@RequestMapping("web/block")
public class BlockController {
    @Autowired
    private BlockService blockService;

    /**
     * 数据块列表
     * @param blockRequestVo 模糊字段 类型，名称
     * @param pageSize 每页记录数
     * @param currPage 当前页数
     * @return
     */
    @GetMapping("blockList")
    public CommonResponse getBlockList(BlockRequestVo blockRequestVo,
                                       @RequestParam(value = "pageSize",defaultValue = "20") Integer pageSize,
                                       @RequestParam(value = "currPage",defaultValue = "1") Integer currPage){
        return CommonResponse.ok(blockService.getBlockList(blockRequestVo,pageSize,currPage));
    }

    /**
     * 数据块 申请
     * @return CommonResponse
     */
    @PostMapping("/datareQuest")
    public CommonResponse datareQuest(@RequestBody ApiRequestEntity apiRequestEntity){
        blockService.datareQuest(apiRequestEntity);
        return CommonResponse.ok();
    }

}
