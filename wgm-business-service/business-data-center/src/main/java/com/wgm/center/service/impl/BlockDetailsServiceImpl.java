package com.wgm.center.service.impl;
import com.wgm.center.dao.ErrorCodeDao;
import com.wgm.center.entity.ErrorCodeEntity;
import com.wgm.center.vo.BlockDetailsResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.wgm.center.dao.BlockDetailsDao;
import com.wgm.center.entity.BlockDetailsEntity;
import com.wgm.center.service.BlockDetailsService;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author 小脸蛋
 */
@Service("blockDetailsService")
@Transactional(rollbackFor = Exception.class)
public class BlockDetailsServiceImpl extends ServiceImpl<BlockDetailsDao, BlockDetailsEntity> implements BlockDetailsService {

    @Autowired
    BlockDetailsDao blockDetailsDao;

    @Autowired
    ErrorCodeDao errorCodeDao;

    /**
     * 对应详情
     * @dataId
     * @return
     */
    @Override
    public Map getDetails(String dataId) {
        //获取对应数据块详情
        BlockDetailsEntity details = blockDetailsDao.getDetails(dataId);
        //获取数据块详情对应错误码
        ErrorCodeEntity errorCode = errorCodeDao.selectById(details.getErrorCodeId());

        //创建并放入ConcurrentHashMap中
        ConcurrentHashMap<Object, Object> detailsMap = new ConcurrentHashMap<>(16);
        detailsMap.put("details",details);
        detailsMap.put("errorCode",errorCode);
        return detailsMap;
    }
}