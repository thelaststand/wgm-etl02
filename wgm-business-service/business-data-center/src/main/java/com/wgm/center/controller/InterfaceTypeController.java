package com.wgm.center.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import com.alibaba.nacos.common.util.UuidUtils;
import com.wgm.center.entity.InterfaceRuleEntity;
import com.wgm.common.constant.CommonResponse;
import com.wgm.common.constant.CommonStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wgm.center.entity.InterfaceTypeEntity;
import com.wgm.center.service.InterfaceTypeService;




/**
 * 接口类型
 *
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
@RestController
@RequestMapping("center/interfacetype")
public class InterfaceTypeController {
    @Autowired
    private InterfaceTypeService interfaceTypeService;

    /**
     * 接口类型列表
     * @param page 当前页
     * @param size 每页条数
     * @return
     */
    @GetMapping("list")
    public CommonResponse getList(@RequestParam(value = "page",defaultValue = "1") Integer page,
                                  @RequestParam(value = "size",defaultValue = "10") Integer size){
        return CommonResponse.ok(interfaceTypeService.getList(page,size));
    }

    /**
     * 保存
     * @param interfaceType
     * @return
     */
    @RequestMapping("/save")
    public CommonResponse save(@RequestBody InterfaceTypeEntity interfaceType){
        interfaceType.setId(UuidUtils.generateUuid());
        interfaceType.setCreateDate(new Date());
		interfaceTypeService.save(interfaceType);
        return CommonResponse.ok();
    }

    /**
     * 修改
     * @param interfaceType
     * @return
     */
    @RequestMapping("/update")
    public CommonResponse update(@RequestBody InterfaceTypeEntity interfaceType){
		interfaceTypeService.updateById(interfaceType);
        return CommonResponse.ok();
    }

    /**
     * 根据ID查询
     * @param id
     * @return
     */
    @RequestMapping("/info/{id}")
    public CommonResponse info(@PathVariable("id") String id){
        InterfaceTypeEntity typeEntity = interfaceTypeService.getById(id);

        return CommonResponse.ok(typeEntity);
    }
    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/delete/{id}")
    public CommonResponse delete(@PathVariable("id") String id){
		interfaceTypeService.dele(id);
        return CommonResponse.ok();
    }

}
