package com.wgm.center.vo;

import lombok.Data;

/**
 * @author pangpang
 * @email 1963148908@qq.com
 * @date 2021/1/20 11:36
 */
@Data
public class BlockRequestVo {

    /**
     * 类型
     */
    String type;
    /**
     * 数据块名称
     */
    String name;

}
