package com.wgm.center.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 数据api测试需要的信息
 * 
 * @author kangbaby
 * @email 2859058385@qq.com
 * @date 2021-01-22 11:35:24
 */
@Data
@TableName("data_test")
public class DataTestEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String id;
	/**
	 * 测试的数据块
	 */
	private String blockId;
	/**
	 * 我的数据
	 */
	private String myData;
	/**
	 * 接口名称
	 */
	private String interfaceName;
	/**
	 * 接口地址
	 */
	private String interfaceAddress;
	/**
	 * 请求方式
	 */
	private String requestWay;
	/**
	 * 请求参数
	 */
	private String requestParam;

}
