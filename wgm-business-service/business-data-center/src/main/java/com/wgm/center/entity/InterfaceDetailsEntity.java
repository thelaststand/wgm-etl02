package com.wgm.center.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 接口详情
 * 
 * @author Ð¡Á³µ°
 * @email 2859058385@qq.com
 * @date 2021-01-21 15:02:18
 */
@Data
@TableName("interface_details")
public class InterfaceDetailsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 接口列表id
	 */
	private String interfaceId;
	/**
	 * 入参请求字段（json）
	 */
	private String inParamField;
	/**
	 * 入参请求格式 (json)
	 */
	private String inParamFormat;
	/**
	 * 出参请求字段（json）
	 */
	private String outParamField;

}
