package com.wgm.center.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.common.utils.PageUtils;
import com.wgm.center.entity.ApiRequestEntity;

import java.util.Map;

/**
 * 数据块申请
 *
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 18:52:59
 */
public interface ApiRequestService extends IService<ApiRequestEntity> {

}

