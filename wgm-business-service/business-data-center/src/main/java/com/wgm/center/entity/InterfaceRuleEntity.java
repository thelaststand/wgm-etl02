package com.wgm.center.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 接口规则
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
@Data
@TableName("interface_rule")
public class InterfaceRuleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String id;
	/**
	 * 规则名称
	 */
	private String ruleName;
	/**
	 * 规则代码
	 */
	private String ruleCode;
	/**
	 * 规则描述
	 */
	private String ruleDesc;
	/**
	 * 创建人
	 */
	private String createUser;
	/**
	 * 状态（1启用2禁用）
	 */
	private String status;
	/**
	 * 创建时间
	 */
	private Date createDate;
	/**
	 * 0展示 1不展示 (逻辑删除)
	 */
	private Integer showStatus;

}
