package com.wgm.center.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

import javax.validation.constraints.Null;

/**
 * 
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 10:51:57
 */
@Data
@TableName("data_block_details")
public class BlockDetailsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String id;
	/**
	 * 数据块id
	 */
	private String dataId;
	/**
	 * 接口地址
	 */
	private String interfaceAddress;
	/**
	 * 返回格式
	 */
	private String returnFormat;
	/**
	 * 请求方式
	 */
	private String requestMode;
	/**
	 * 请求示例
	 */
	private String requestExample;
	/**
	 * 简介
	 */
	private String intro;
	/**
	 * 请求参数说明
	 */
	private String requestParam;
	/**
	 * 返回参数说明
	 */
	private String returnParam;
	/**
	 * JSON返回示例
	 */
	private String jsonReturn;
	/**
	 * XML返回示例
	 */
	private String xmlReturn;
	/**
	 * 错误码id
	 */
	private String  errorCodeId;

}
