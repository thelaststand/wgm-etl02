package com.wgm.center.dao;

import com.wgm.center.entity.ApiMyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.center.vo.PageRequestParamVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

/**
 * 我的api
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 14:05:30
 */
@Mapper
public interface MyApiDao extends BaseMapper<ApiMyEntity> {
    /**
     * 查询 my API
     * @param page
     * @return
     */
    List<ConcurrentMap<String, Object>> search(PageRequestParamVo page);

    /**
     * 接口申请
     * @param id
     * @return
     */
    Map<String, Object> searchDetail(String id);
}
