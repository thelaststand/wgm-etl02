package com.wgm.center.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author pangpang
 * @email 1963148908@qq.com
 * @date 2021/1/20 14:13
 */
@Data
public class BlockResponseVo {

    private String id;
    /**
     * 接口名称
     */
    private String name;
    /**
     * 接口类型id
     */
    private Integer typeId;
    /**
     * 接口类型
     */
    private String type;
    /**
     * 费用（按次计费）
     */
    private Double money;
    /**
     * 描述
     */
    private String describe;
    /**
     * 接口图片
     */
    private String pic;
    /**
     * 接口来源
     */
    private String source;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新周期(单位：天)
     */
    private Integer updatePeriod;
    /**
     * 此接口使用的次数
     */
    private Integer useCount;

}
