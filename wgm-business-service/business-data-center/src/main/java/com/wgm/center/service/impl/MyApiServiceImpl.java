package com.wgm.center.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wgm.center.vo.PageRequestParamVo;
import com.wgm.common.constant.CommonConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


import com.wgm.center.dao.MyApiDao;
import com.wgm.center.entity.ApiMyEntity;
import com.wgm.center.service.MyApiService;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

/**
 * @author 小脸蛋
 */
@Service("myApiService")
@Transactional(rollbackFor = Exception.class)
public class MyApiServiceImpl extends ServiceImpl<MyApiDao, ApiMyEntity> implements MyApiService {

    @Autowired
    MyApiDao myApiDao;

    /**
     * 查询 my API
     * @param page
     * @return
     */
    @Override
    public PageInfo<ConcurrentMap<String, Object>> search(PageRequestParamVo page) {
        //分页参数
        PageHelper.startPage(page.getPage(),page.getSize());

        List<ConcurrentMap<String, Object>> search = myApiDao.search(page);
        PageInfo<ConcurrentMap<String, Object>> pageInfo = PageInfo.of(search);

        List<ConcurrentMap<String, Object>> list = pageInfo.getList();

        list.forEach(map->{
            map.forEach((key,value)->{
                //如果是 按 天  计费 进行 计算
                if (CommonConstants.Data.CHARGEMODE.equals(key)&& CommonConstants.OperationType.SELECT.equals(value)){
                    Date now = new Date();
                    Date createTime = (Date) map.get("createTime");
                    long l = (now.getTime() - createTime.getTime())/1000/12/30/24;
                    //剩余天数
                    Integer days = (Integer) map.get("days");
                    map.put("heaven", days-l);
                }
            });
        });
        pageInfo.setList(list);
        return pageInfo;
    }

    /**
     * 查询 my API 详情
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> searchDetail(String id) {
        Map<String, Object> map = myApiDao.searchDetail(id);
        return map;
    }
}