package com.wgm.center.dao;

import com.wgm.center.entity.BlockDetailsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.center.vo.BlockDetailsResponseVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 10:51:57
 */
@Mapper
public interface BlockDetailsDao extends BaseMapper<BlockDetailsEntity> {

    /**
     * 对应详情
     * @param dataId
     * @return
     */
    BlockDetailsEntity getDetails(String dataId);
}
