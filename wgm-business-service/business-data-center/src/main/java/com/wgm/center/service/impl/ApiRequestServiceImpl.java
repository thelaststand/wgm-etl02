package com.wgm.center.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.wgm.center.dao.ApiRequestDao;
import com.wgm.center.entity.ApiRequestEntity;
import com.wgm.center.service.ApiRequestService;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author 小脸蛋
 */
@Service("apiRequestService")
@Transactional(rollbackFor = Exception.class)
public class ApiRequestServiceImpl extends ServiceImpl<ApiRequestDao, ApiRequestEntity> implements ApiRequestService {

}