package com.wgm.center.dao;

import com.wgm.center.entity.BlockEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wgm.center.vo.BlockResponseVo;
import jdk.nashorn.internal.ir.Block;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 10:51:58
 */
@Mapper
public interface BlockDao extends BaseMapper<BlockEntity> {

    /**
     * 数据块列表
     * @param name
     * @param type
     * @param currPage
     * @param pageSize
     * @return
     */
    List<BlockResponseVo> getBlockList(@Param("name") String name,
                                       @Param("type") String type,
                                       @Param("currPage") Integer currPage,
                                       @Param("pageSize") Integer pageSize);

    /**
     * 总记录数
     * @return
     */
    int getCount();

    /**
     * 接口列表
     * @param name
     * @return
     */
    List<BlockEntity> interfaceList(String name);
}
