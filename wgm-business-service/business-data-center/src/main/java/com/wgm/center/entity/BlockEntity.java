package com.wgm.center.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

import javax.validation.constraints.Null;

/**
 * 
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 10:51:58
 */
@Data
@TableName("data_block")
public class BlockEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private String id;
	/**
	 * 接口名称
	 */
	@Null
	private String name;
	/**
	 * 接口类型id
	 */
	private Integer typeId;
	/**
	 * 费用（按次计费）
	 */
	private Double money;
	/**
	 * 描述
	 */
	private String describes;
	/**
	 * 接口图片
	 */
	private String pic;
	/**
	 * 接口来源
	 */
	private String source;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新周期(单位：天)
	 */
	private Integer updatePeriod;
	/**
	 * 此接口使用的次数
	 */
	private Integer useCount;
	/**
	 * 逻辑删除
	 */
	private Integer showStatus;
	/**
	 * 修改时间
	 */
	private Date updateTime;
}
