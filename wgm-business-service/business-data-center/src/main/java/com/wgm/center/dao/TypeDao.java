package com.wgm.center.dao;

import com.wgm.center.entity.DataTypeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 类型
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
@Mapper
public interface TypeDao extends BaseMapper<DataTypeEntity> {
	
}
