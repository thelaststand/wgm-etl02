package com.wgm.center.service.impl;

import com.wgm.center.entity.InterfaceConfigEntity;
import com.wgm.common.constant.CommonConstants;
import com.wgm.common.utils.PageUtils;
import com.wgm.common.utils.UuidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


import com.wgm.center.dao.InterfaceChargeModeDao;
import com.wgm.center.entity.InterfaceChargeModeEntity;
import com.wgm.center.service.InterfaceChargeModeService;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author 小脸蛋
 */
@Service("interfaceChargeModeService")
@Transactional(rollbackFor = Exception.class)
public class InterfaceChargeModeServiceImpl extends ServiceImpl<InterfaceChargeModeDao, InterfaceChargeModeEntity> implements InterfaceChargeModeService {

    @Autowired
    InterfaceChargeModeDao interfaceChargeModeDao;

    /**
     * 接口计费方式设置列表
     * @param page 当前页
     * @param size 每页条数
     * @return
     */
    @Override
    public PageUtils getList(Integer page, Integer size) {
        List<InterfaceChargeModeEntity> list = interfaceChargeModeDao.getList((page - 1) * size, size);
        Integer count = interfaceChargeModeDao.getCount();

        return new PageUtils(list,count,size,page);
    }

    /**
     * 删除
     * @param id
     */
    @Override
    public void dele(String id) {
        interfaceChargeModeDao.dele(id);
    }

    /**
     * 添加
     * @param interfaceChargeMode
     */
    @Override
    public void add(InterfaceChargeModeEntity interfaceChargeMode) {
        interfaceChargeMode.setId(UuidUtils.generateUuid());
        interfaceChargeMode.setCreateDate(new Date());
        //按次数收费
        if (CommonConstants.Data.NUMBER_CHARGE.equals(interfaceChargeMode.getCharheType())){
            interfaceChargeMode.setDayNum(null);
        //按天数收费
        }else if(CommonConstants.Data.DAY_CHARGE.equals(interfaceChargeMode.getCharheType())){
            interfaceChargeMode.setNumber(null);
        }
        interfaceChargeModeDao.insert(interfaceChargeMode);
    }
}