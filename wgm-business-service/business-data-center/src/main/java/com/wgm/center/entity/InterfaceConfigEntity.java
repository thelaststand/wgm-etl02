package com.wgm.center.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 接口配置
 * 
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
@Data
@TableName("interface_config")
public class InterfaceConfigEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String id;
	/**
	 * 配置名称
	 */
	private String configName;
	/**
	 * 配置描述
	 */
	private String configDesc;
	/**
	 * 接口规则
	 */
	private String interfaceRule;
	/**
	 * 接口规则ID
	 */
	private String ruleId;
	/**
	 * 主接口
	 */
	private String masterInterface;
	/**
	 * 从接口
	 */
	private String slaveInterface;
	/**
	 * 辅接口
	 */
	private String auxiliaryInterface;
	/**
	 * 创建人
	 */
	private String createUser;
	/**
	 * 创建时间
	 */
	private Date createDate;
	/**
	 * 0展示 1不展示 (逻辑删除)
	 */
	private Integer showStatus;

}
