package com.wgm.center.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
*   class
*   @Author 小脸蛋
*   @Date 2021/1/21 11:02
*/
@Data
public class InterfaceUsageRequestVo {

    /**
     *  接口id
     */
    private String id;
    /**
     * 模糊  公司名称
     */
    private String name;
    /**
     * 当前页
     */
    private Integer page = 1 ;
    /**
     * 每页的 条 数
     */
    private Integer size = 10;
    /**
     * 开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String startTime;
    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String endTime;
}
