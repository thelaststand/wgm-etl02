package com.wgm.center.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wgm.common.utils.PageUtils;
import com.wgm.center.entity.InterfaceChargeModeEntity;

import java.util.List;
import java.util.Map;

/**
 * 接口计费方式设置
 *
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-21 09:04:59
 */
public interface InterfaceChargeModeService extends IService<InterfaceChargeModeEntity> {

    /**
     * 接口计费方式设置列表
     * @param page 当前页
     * @param size 每页条数
     * @return
     */
    PageUtils getList(Integer page, Integer size);

    /**
     * 删除
     * @param id
     */
    void dele(String id);

    /**
     * 添加
     * @param interfaceChargeMode
     */
    void add(InterfaceChargeModeEntity interfaceChargeMode);
}

