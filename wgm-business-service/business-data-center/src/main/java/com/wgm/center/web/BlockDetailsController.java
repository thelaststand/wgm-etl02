package com.wgm.center.web;

import com.wgm.common.constant.CommonResponse;
import com.wgm.common.constant.CommonStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.wgm.center.service.BlockDetailsService;



/**
 * 
 *
 * @author 小脸蛋
 * @email 2859058385@qq.com
 * @date 2021-01-20 10:51:57
 */
@RestController
@RequestMapping("web/details")
public class BlockDetailsController {

    @Autowired
    private BlockDetailsService blockDetailsService;

    /**
     * 对应详情
     * @param dataId  数据块ID
     * @return
     */
    @GetMapping("/detailsList")
    public CommonResponse getDetails(@RequestParam("dataId")String dataId){
        return new CommonResponse(CommonStatus.VALID,blockDetailsService.getDetails(dataId));
    }


}
