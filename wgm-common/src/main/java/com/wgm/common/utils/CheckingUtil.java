package com.wgm.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @author ***
 * @date 2021/1/19 14:21
 */
public class CheckingUtil {

//    public static void main(String[] args) {
//        String str = "18524265283";
//
//        Boolean legal = isChinaPhoneLegal(str);
//        System.out.println(legal);
//
//
//        String password = "3a5;@8888888888089";
//        if(password.length()>=8){
//            if(password.length()<=20){
//                Integer integer = validatePasswdComplexity(password);
//                System.out.println(integer);
//            }else {
//                System.out.println("密码不能超过20位");
//            }
//        }else {
//            System.out.println("密位不足8位");
//        }
//    }


    public static Boolean isChinaPhoneLegal(String str) throws PatternSyntaxException{

        String regExp="^((13[0-9])|(14[5,7,9])|(15[0-3,5-9])|(166)|(17[3,5,6,7,8])|(18[0-9])|(19[8,9]))\\d{8}$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(str);
        return m.matches();
    }

}


