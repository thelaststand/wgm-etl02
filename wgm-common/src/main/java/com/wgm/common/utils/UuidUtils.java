package com.wgm.common.utils;

import java.util.UUID;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/20
 */
public class UuidUtils {

    public static String generateUuid(){
        String uuid = UUID.randomUUID().toString();
        uuid=uuid.replace("-","");
        uuid=uuid.toUpperCase();
        return uuid;
    }

}
