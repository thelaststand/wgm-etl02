package com.wgm.common.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author pangpang
 * @email 1963148908@qq.com
 * @date 2021/1/6 19:50
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EtlDatasourceInfoResponse {

    /**
     * 数据源名称
     */
    String dbName;
    /**
     * 备注/说明
     */
    String remarks;
    /**
     * 数据源类型
     */
    String type;
    /**
     * 状态
     */
    String state;
    /**
     * 描述
     */
    String description;
    /**
     * 最后更新时间
     */
    Date updateTime;
    /**
     * 数据源用户名
     */
    private String username;
}
