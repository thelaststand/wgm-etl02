package com.wgm.common.utils;
 
import com.wgm.common.constant.CommonConstants;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author lenovo
 */
public class Md5Utils {
	private static final int  NUM= 32 ;
	/**
	 * 使用md5的算法进行加密
	 */
	public String md5(String plainText) {
		byte[] secretBytes = null;
		try {
			secretBytes = MessageDigest.getInstance("md5").digest(
					plainText.getBytes());
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("没有md5这个算法！");
		}
		// 16进制数字
		String md5code = new BigInteger(1, secretBytes).toString(16);
		// 如果生成数字未满32位，需要前面补0
		for (int i = 0; i < NUM - md5code.length(); i++) {
			md5code = CommonConstants.Data.ZERO + md5code;
		}
		return md5code;
	}
 
//	public static void main(String[] args) {
//		System.out.println(md5("123"));
//	}
 
}