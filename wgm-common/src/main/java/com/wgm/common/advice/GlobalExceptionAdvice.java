package com.wgm.common.advice;

import com.wgm.common.constant.CommonStatus;
import com.wgm.common.exception.*;
import com.wgm.common.constant.CommonResponse;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * Demo 异常处理
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/6
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionAdvice {
    /**
     * 基础异常处理
     * @param baseException
     * @return
     */
    @ExceptionHandler(value = BaseException.class)
    public CommonResponse<String> baseExceptionHandler(BaseException baseException) {
        CommonResponse<String> commonResponse = new CommonResponse<String>();
        commonResponse.setCode(CommonStatus.INVALID.getCode());
        commonResponse.setMessage(baseException.getMessage());
        return commonResponse;
    }

    /**
     * adException
     * @param adException
     * @return
     */
    @ExceptionHandler(value = AdException.class)
    public CommonResponse<String> handlerAdException(AdException adException){
        CommonResponse<String> commonResponse = new CommonResponse<>(CommonStatus.INVALID);
        commonResponse.setData(adException.getMessage());
        return commonResponse;
    }

    /**
     * 任务异常处理
     * @param taskException
     * @return
     */
    @ExceptionHandler(value = TaskException.class)
    public CommonResponse<String> handlerAdException(TaskException taskException){
        CommonResponse<String> commonResponse = new CommonResponse<>(CommonStatus.INVALID);
        commonResponse.setMessage(taskException.getMessage());
        return commonResponse;
    }

    /**
     * 数据源异常处理
     * @param dataSourceException
     * @return
     */
    @ExceptionHandler(value = DataSourceException.class)
    public CommonResponse<String> handlerAdException(DataSourceException dataSourceException){
        CommonResponse<String> commonResponse = new CommonResponse<>(CommonStatus.INVALID);
        commonResponse.setMessage(dataSourceException.getMessage());
        return commonResponse;
    }

    /**
     * 数据源异常处理
     * @param engineException
     * @return
     */
    @ExceptionHandler(value = EngineException.class)
    public CommonResponse<String> engineException(EngineException engineException){
        CommonResponse<String> commonResponse = new CommonResponse<>(CommonStatus.INVALID);
        if (engineException.getMessage()!=null){
            commonResponse.setMessage(engineException.getMessage());
        }
        return commonResponse;
    }

    /**
     * 接口，数据块等  异常处理
     * @param interfaceException
     * @return
     */
    @ExceptionHandler(value = InterfaceException.class)
    public CommonResponse interfaceException(InterfaceException interfaceException){
        CommonResponse<String> commonResponse = new CommonResponse<>(CommonStatus.INVALID);
        if (interfaceException.getMessage()!=null){
            commonResponse.setMessage(interfaceException.getMessage());
        }
        return commonResponse;
    }
}