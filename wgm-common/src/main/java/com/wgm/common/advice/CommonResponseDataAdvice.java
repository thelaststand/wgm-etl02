package com.wgm.common.advice;

import com.wgm.common.annotation.IngoreResponseAdvice;
import com.wgm.common.constant.CommonStatus;
import com.wgm.common.constant.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * 统一结果集
 *
 * @author baimugudu
 * @email 2415621370@qq.com
 * @date 2020/12/7 15:09
 */
@RestControllerAdvice
@Component
@Slf4j
public class CommonResponseDataAdvice implements ResponseBodyAdvice<Object> {

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        //如果 我在类上 加了次注解 那么就不执行 增强
        if (returnType.getAnnotatedElement().isAnnotationPresent(IngoreResponseAdvice.class)){
            return  false;
        }
        //如果 我在方法上 加了次注解 那么就不执行 增强
        if (returnType.getMethod().isAnnotationPresent(IngoreResponseAdvice.class)){
            return  false;
        }
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        CommonResponse<Object> commonResponse = new CommonResponse<>(CommonStatus.VALID);
        commonResponse.setCode(CommonStatus.VALID.getCode());
        commonResponse.setMessage(CommonStatus.VALID.getDesc());
        if (body==null){
            //如果返回的是 空的 那么就直接返回
            return commonResponse;
        }else if (body instanceof CommonResponse){
            //如果不是空的 且属于结果集 那么就赋值
            commonResponse= (CommonResponse<Object>) body;
        }else {
            //直接赋值
            commonResponse.setData(body);
        }
        return commonResponse;
    }
}
