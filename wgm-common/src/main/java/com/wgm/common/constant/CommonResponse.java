package com.wgm.common.constant;

import lombok.Data;

import java.io.Serializable;

/**
*   class
*   @Author 小脸蛋
*   @CreateDate 2021/1/7
*/
@Data
public class CommonResponse<T> implements Serializable {

    /**
     * 业务状态码
     */
    private Integer code;
    /**
     * 业务描述信息
     */
    private String message;

    /**
     * 返回的数据
     */
    private T data;


    public CommonResponse() {

    }

    public static CommonResponse ok(){
        CommonResponse response = new CommonResponse(CommonStatus.VALID);
        return response;
    }

    public static CommonResponse ok(String msg){
        CommonResponse response = new CommonResponse(CommonStatus.VALID.getCode(),msg);
        return response;
    }

    public static CommonResponse ok(String msg,Object data){
        CommonResponse response = new CommonResponse(CommonStatus.VALID.getCode(),msg);
        if (data!=null){
            response.setData(data);
        }
        return response;
    }

    public static CommonResponse ok(Object data){
        CommonResponse response = new CommonResponse(CommonStatus.VALID);
        if (data!=null){
            response.setData(data);
        }
        return response;
    }

    public static CommonResponse error(){
        CommonResponse response = new CommonResponse(CommonStatus.INVALID);
        return response;
    }

    public static CommonResponse error(String msg){
        CommonResponse response = new CommonResponse(CommonStatus.INVALID.getCode(),msg);
        return response;
    }
    public static CommonResponse error(String msg,Object data){
        CommonResponse response = new CommonResponse(CommonStatus.INVALID.getCode(),msg,data);
        return response;
    }



    public CommonResponse(CommonStatus commonStatus) {
        this.code = commonStatus.getCode();
        this.message = commonStatus.getDesc();
    }

    public CommonResponse(CommonStatus commonStatus,T data) {
        this.code = commonStatus.getCode();
        this.message = commonStatus.getDesc();
        this.data = data;
    }

    public CommonResponse(Integer code, String message){
        this.code = code;
        this.message = message;
    }

    public CommonResponse(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

}
