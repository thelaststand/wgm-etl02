package com.wgm.common.constant;

import lombok.Getter;

/**
 * @author baimugudu
 * @email 2415621370@qq.com
 * @date 2020/12/9 16:46
 */
@Getter
public enum CommonStatus {
    //操作状态
    VALID(0,"操作成功"),
    INVALID(-1,"操作失败"),
    //数据源使用状态
    DISABLED_ENUM(1,"已禁用"),
    IS_USING_ENUM(0,"正在使用"),
    //数据显示状态
    DATA_DISPLAY(0,"数据展示"),
    DATA_IS_NOT_DISPLAY(1,"数据不展示"),
    //任务执行状态
    PROCESS_STATUS_NO(-1,"不可执行"),
    PROCESS_STATUS_CAN(0,"可执行"),
    PROCESS_STATUS_CONTINUE(1,"正在执行"),
    PROCESS_STATUS_SUCCESS(2,"执行完成"),
    //about file
    FILE_NOT_ENGINENAME_EXISTES(-1,"该名称引擎已存在"),
    FILE_NOT_EXISTES(-1,"类/函数名已存在"),

    SYSTEM_PROPERTIES(1,"系统属性"),
    CUSTOMIZE_PROPERTIES(0,"自定义属性"),

    //审核状态
    WAIT_CHECK(0,"待审核"),
    CHECK_PASSED(1,"审核通过"),
    CHECK_FAILURE(2,"审核未通过"),

    //地区级别
    PROVINCE(1,"省"),
    CITY(2,"市"),
    COUNTY(3,"县");





    private Integer code;
    private String desc;

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    CommonStatus(Integer code, String desc){
        this.code=code;
        this.desc=desc;
    }

}
