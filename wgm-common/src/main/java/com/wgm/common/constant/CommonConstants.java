package com.wgm.common.constant;

/**
 *   class
 *   @Author 小脸蛋
 *   @CreateDate 2021/1/7
 */
public class CommonConstants {

    public static class RegisteredAndLoginError{
        public final static String PHONE_NUMBER_ERROR ="手机号错误";
        public final static String PHONE_NUMBER_EXIST ="手机号已存在";
        public final static String PASSWORD_LENGTH_OVERFLOW_ERROR = "密码不能超过20位";
        public final static String PASSWORD_LENGTH_NOT_ENOUGH_ERROR="密码必须大于8位";
        public final static String INCORRECT_VERIFICATION_CODE = "验证码不正确";
        public final static String INCORRECT_PASSWORD_ERROR= "密码不正确";
        public final static String TWO_PASSWORDS_ARE_INCONSISTENT= "两次密码不一致";
        public final static String THE_USER_IS_NOT_REGISTERED = "该用户未注册";
        public final static String INCORRECT_PASSWORD_OR_USERNAME = "密码或用户名错误";
        public final static String USER_ALREADY_EXISTS = "用户已存在";


        public final static String NOT_LOGGED_ERROR = "用户未登录";
        public final static String PHONE_EXISTENCE_ERROR = "手机号已存在";
    }

    public static class ErrorMsg{
        public final static String REQUEST_PARAM_ERROR  ="请求参数错误";
        public final static String TOTAL_NO_MORE_THAN="每页不能超过50条";
        public final static String SUCCESS="成功";
        public final static String FAIL="失败";
        public final static String FIELD_SQL_ERROR="字段别名和字段名不对应";
        public final static String FILE_LAST_ERROR="上传文件格式不正确";
        public final static String FILE_TEMPLATE_ERROR="该文件模板不正确";
        public final static String TARGET_DOESNOTEXIST="该文件模板不正确";
        public final static String SYSTEM_PROPERTIES_DEL_ERROR="系统属性不能删除";
        public final static String CLASS_FUNCTION_NAME_NOT_EDITABLE="类/函数名称不可修改";
        public final static String RULE_ENGINE_NAME_REPEAT="规则引擎名称重复";
        public final static String FEIGN_HTSTRIX="断了断了";
        public final static String DOESNO_TEXIST="找的东西不存在";
        public static final String LOGIN_EXCEPTION_ERROR = "请先登录再进行操作";
    }
    public static class TaskError{
        public static final String TASK_NAME_EXISTS = "任务名已存在" ;
        public static final String TASK_WEIGHT_NULL = "任务权重为空";
        public final static String TASK_DOES_NOT_EXSIT_ERROR  ="任务不存在";
        public final static String TASK_IS_EXECUTING_ERROR  ="任务正在执行";
    }
    public static class DataError{

        public final static String DATA_ERROR  ="数据源不存在";
        public final static String DATA_ID_NULL  ="数据源id不能为空";
        public final static String TABLE_NAME_NULL  ="表名不能为空或与该数据库中的表名不一致";
        public final static String DATABASE_NULL  ="数据库名不能为空";
        public final static String CONNETTION_ERROR  ="连接不存在";
        public final static String DATA_CLOSE_ERROR  ="数据连接流关闭异常";
        public final static String CONNETTION_CLOSE_ERROR  ="连接关闭异常";
        public final static String DATA_CONNETTION_ERROR  ="数据源连接异常";
        public final static String DATA_SELECT_ERROR  ="数据查询异常";
        public final static String DATA_UPDATE_ERROR  ="数据操作异常";
        public final static String DATA_IS_EXECUTING_ERROR  ="数据源正在使用";
    }

    public static class Jdbc{
        public final static String MYSQLBASEQUERY="select @@version as version";
        public final static String SHOWVERSION="version";
        public final static String SHOWCOUNT="count";
        public final static String MYSQL_PRE = "jdbc:mysql://";
        public final static String MYSQL_DRIVER = "com.mysql.cj.jdbc.Driver";
        public final static String IS_WORKING = "0";
    }

    public static class Data{
        //50条
        public final static int MAXTOTAL=50;
        //100条
        public final static int HUNDRED=100;
        public final static String ZERO="0";

        public final static String SUCCESS="true";
        public final static String ERROR="false";
        public final static String GET="get";
        public final static String POST="post";
        public final static String MYSQL="mysql";
        public final static String REDIS="redis";
        public final static String TYPE="Type";
        public final static String VARCHAR="varchar";
        public final static String INT="int";
        public final static String NAME="name";
        public final static String PASSWORD="password";
        public final static String ADDRESS="address";
        public final static String CHARGEMODE="chargeMode";
        public final static String INTERFACEUSAGE = "接口使用情况";
        public final static String INTERFACEDETAILS = "接口详情";
        public final static String NUMBER_CHARGE = "按次数收费";
        public final static String DAY_CHARGE = "按天数收费";
    }
    public static class File{
        //文件 已存在
        public final static String FILE_NO_EXISTS="该java文件已存在";
    }

    public static class BiMassage{
        public final static String DATABASE_NOT_EXIST ="数据库不存在";
        public final static String TABLE_NOT_EXIST ="该库中没有该表";
    }

    public static class OperationType{
        public final static String INSTER ="1";
        public final static String SELECT ="2";
        public final static String UPDATE ="3";
        public final static String DELETE ="4";
        public final static String TEST ="5";
    }

    public static class Account{
        public final static String ACCOUNT_ID_NULL ="账户id不能为空";
        public final static String WARNING_THRESHOULD_NULL ="预警阈值不能为空";
        public final static String WARNING_PHONE_NULL ="预警电话不能为空";
    }

    public class IpWhiteListError {
        public static final String THE_COMPANY_IS_NOT_CERTIFIED = "公司未认证";
        public static final String THE_DATA_DOES_NOT_EXIST = "数据不存在";
    }

    public static class Recharge{
        public final static String USER_ID_NULL ="用户id不能为空";
        public final static String ACCOUNT_ID_NULL ="账户id为空";
        public final static String COMPANY_ID_NULL ="公司id为空";
        public final static String AMOUNT_ID_NULL ="充值为空";
    }

    public final static Integer TWELVE = 12;
    public final static Integer ONE = 1;
    public final static Integer SIX = 6;
    public final static Integer ZERO = 0;
}