package com.wgm.common.annotation;

import java.lang.annotation.*;

/**
 * @author baimugudu
 * @email 2415621370@qq.com
 * @date 2020/12/8 10:30
 */

@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IngoreResponseAdvice {
}
