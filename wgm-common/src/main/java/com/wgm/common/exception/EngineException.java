package com.wgm.common.exception;/**
*   class
*   @Author 小脸蛋
*   @CreateDate 2021/1/12
*/
public class EngineException extends BaseException{

   public EngineException(String message){
       super(message);
   }
}
