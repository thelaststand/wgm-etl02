package com.wgm.common.exception;/** 
*   class
*   @Author 小脸蛋
*   @Date 2021/1/21 21:46
*/
public class ExceptionCast {

    public static void interfaceException(String message){
        throw new InterfaceException(message);
    }
    public static void adException(String message){
        throw new AdException(message);
    }
    public static void engineException(String message){
        throw new EngineException(message);
    }
}
