package com.wgm.common.exception;

/**
*   class
*   @Author 小脸蛋
*   @Date 2021/1/21 21:33
*/
public class InterfaceException extends BaseException{
    public InterfaceException(String message){
        super(message);
    }
}
