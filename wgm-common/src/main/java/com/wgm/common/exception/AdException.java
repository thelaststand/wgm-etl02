package com.wgm.common.exception;

/**
 * @author 小脸蛋
 */
public class AdException extends BaseException{

    public AdException(String message){
        super(message);
    }
}