package com.wgm.common.exception;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/9
 */
public class DataSourceException extends BaseException{
    public DataSourceException(String message){
        super(message);
    }
}
