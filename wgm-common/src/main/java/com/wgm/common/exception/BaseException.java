package com.wgm.common.exception;

import com.wgm.common.constant.CommonStatus;

/**
 * Demo class
 *
 * @author wudongrun
 * @email 957099566@qq.com
 * @date 2021/1/9
 */
public class BaseException extends RuntimeException{
    private int status = CommonStatus.INVALID.getCode();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public BaseException() {
    }

    public BaseException(String message,int status) {
        super(message);
        this.status = status;
    }

    public BaseException(String message) {
        super(message);
    }

}
